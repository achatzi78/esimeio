@extends('layouts.app')

@section('meta-title', 'Μαθήματα::Προβολή')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Προβολή Μαθήματος
                </div>

                <div class="card-body">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Τίτλος</dt>
                                <dd>{{ $subject->title }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- faculties --}}
                        <div class="col-md-6 col-lg-4">
                            <dl>
                                <dt>Τμήματα</dt>
                                <dd>
                                    <ul class="list-unstyled">
                                        @foreach($subject->faculties as $faculty)
                                            <li>{{ $faculty->title }}</li>
                                        @endforeach
                                    </ul>
                                </dd>
                            </dl>
                        </div>

                        {{-- teachers --}}
                        <div class="col-md-6 col-lg-4">
                            <dl>
                                <dt>Καθηγητές</dt>
                                <dd>
                                    <ul class="list-unstyled">
                                        @foreach($subject->teachers as $teacher)
                                            <li>{{ $teacher->fullname }}</li>
                                        @endforeach
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        @can ('update', $subject)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="{{ route('subjects.edit', ['subject' => $subject]) }}" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endif

                        <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                            <a href="{{ route('subjects.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
