@extends('layouts.app')

@section('meta-title', 'Μαθήματα::Επεξεργασία')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:subjects.form :subject="$subject" />
        </div>
    </div>
@endsection
