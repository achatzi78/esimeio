@extends('layouts.app')

@section('meta-title', 'Μαθήματα')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:subjects.index />
        </div>
    </div>
@endsection
