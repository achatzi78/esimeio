@extends('layouts.app')

@section('meta-title', 'Μαθήματα::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:subjects.form />
        </div>
    </div>
@endsection

