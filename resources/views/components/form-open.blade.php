@props(['model' => null, 'route' => null, 'method' => 'GET'])

@if ($model)
    {!! Form::model($model, array_merge($attributes->getAttributes(), ['route' => $route, 'method' => $method])) !!}
@else
    {!! Form::open(array_merge($attributes->getAttributes(), ['route' => $route, 'method' => $method])) !!}
@endif
