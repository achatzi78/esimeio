@props(['phone' => null])

@if ($phone)
    {{--<a href="tel:{{ phone($phone)->formatForMobileDialingInCountry('GR') }}">{{ $slot }}</a>--}}
    <a href="tel:{{ $phone }}">{{ $slot }}</a>
@else
    {{ $slot }}
@endif
