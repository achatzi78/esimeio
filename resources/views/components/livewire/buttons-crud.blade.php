@props([
    'model' => null,
    'prefix' => '',
    'parameter' => '',
])

<div class="d-flex gap-2">
    @can('view', $model)
        <div class="flex-fill">
            <a href="{{ route("{$prefix}.show", [$parameter => $model]) }}" class="btn btn-primary btn-sm w-100" title="Προβολή" data-bs-toggle="tooltip"><i class="fas fa-search"></i></a>
        </div>
    @endcan

    @can('update', $model)
        <div class="flex-fill">
            <a href="{{ route("{$prefix}.edit", [$parameter => $model]) }}" class="btn btn-warning btn-sm w-100" title="Επεξεργασία" data-bs-toggle="tooltip"><i class="fas fa-pencil-alt"></i></a>
        </div>
    @endcan

    @can('delete', $model)
        <div class="flex-fill">
            <a href="javascript:;" class="btn btn-danger btn-sm w-100" onclick="confirm('Είστε σίγουρος/η;') || event.stopImmediatePropagation()" wire:click="remove({{ $model->id }})" title="Διαγραφή" data-bs-toggle="tooltip"><i class="fas fa-trash"></i></a>
        </div>
    @endcan
</div>
