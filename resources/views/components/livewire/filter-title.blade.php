<label class="form-label">{{ $label ?? 'Τίτλος' }}</label>
<input {{ $attributes->class(['form-control'])->merge(['wire:model.lazy' => 'filters.title', 'id' => 'title_filter', 'name' => 'filters[title]', 'type' => 'text']) }}>
