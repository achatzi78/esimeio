<th
    {{
        $attributes
            ->class([
                'text-capitalize',
                'sorting' => !empty($sortBy),
                'sorting_asc' => $currentSort == $sortBy && $currentDirection == 'asc',
                'sorting_desc' => $currentSort == $sortBy && $currentDirection == 'desc'
            ])
    }}
    @if ($sortBy) wire:click="sortBy('{{ $sortBy }}')" @endif
>
    {{ $slot }}
</th>
