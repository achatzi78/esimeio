<x-inputs.boolean-select
    wire:model.defer="filters.active"
    id="active_filter"
    name="filters[active]"
>
    <x-slot:for>active_filter</x-slot>
    <x-slot:label>{{ __('active') }}</x-slot>
</x-inputs.boolean-select>
