@props(['type' => null])

@if ($type == 'home')
    <abbr {{ $attributes->merge(['title' => 'Σπίτι']) }}><i class="fas fa-home"></i></abbr>
@elseif ($type == 'work')
    <abbr {{ $attributes->merge(['title' => 'Εργασία']) }}><i class="fas fa-briefcase"></i></abbr>
@elseif ($type == 'mobile')
    <abbr {{ $attributes->merge(['title' => 'Κινητό']) }}><i class="fas fa-mobile-alt"></i></abbr>
@else
    <abbr {{ $attributes->merge(['title' => 'Τηλέφωνο']) }}><i class="fas fa-phone"></i></abbr>
@endif
