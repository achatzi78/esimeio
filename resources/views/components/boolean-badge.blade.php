@props(['property' => true])

<h5>
    <span class="badge bg-{{ ($property) ? 'success' : 'danger' }}"><i class="fas fa-{{ ($property) ? 'check' : 'times' }}-circle"></i> {{ ($property) ? 'Ναι' : 'Όχι' }}</span>
</h5>
