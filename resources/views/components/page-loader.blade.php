<div class="d-flex justify-content-center align-items-center position-fixed w-100 h-100 bg-black bg-opacity-75" style="top: 0; left: 0; z-index: 99999;">
    <div class="bg-white rounded p-4 shadow-sm text-simeio">
        <h3 class="m-0"><i class="fa-solid fa-circle-notch fa-spin"></i> @if ($slot->isEmpty()) Φόρτωση... @else {{ $slot }} @endif</h3>
    </div>
</div>

