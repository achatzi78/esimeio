@props([
    'rows' => null,
])

{{ $rows->appends(request()->except(['page']))->onEachSide(1)->links() }}
