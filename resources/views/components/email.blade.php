@props(['email' => null])

@if ($email)
    <a href="mail:{{ $email }}">{{ $slot }}</a>
@else
    {{ $slot }}
@endif
