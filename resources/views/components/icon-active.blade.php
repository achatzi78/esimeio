@props(['active' => null])

<i class="fas fa-{{ ($active) ? 'check' : 'times' }} fa-lg text-{{ ($active) ? 'success' : 'danger' }}"></i>
