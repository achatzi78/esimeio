@props([
    'top' => null,
    'after_view' => null,
    'after_update' => null,
    'model' => null,
    'prefix' => '',
    'parameter' => '',
    'livewire' => false
])

<a href="javascript:;" class="btn btn-sm col-12 btn-outline-secondary make-popover" data-placement="auto" wire:ignore.self><i class="fas fa-cogs"></i></a>
<div class="webui-popover-content" wire:ignore.self>
    <div class="dropdown-menu">

        {{ $top }}

        @if ($model)
            @can('view', $model)
                <a class="dropdown-item" href="{{ route("{$prefix}.show", [$parameter => $model]) }}"><i class="fas fa-search"></i> Προβολή</a>
            @endcan

            {{ $after_view }}

            @can('update', $model)
                <a class="dropdown-item" href="{{ route("{$prefix}.edit", [$parameter => $model]) }}"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
            @endcan

            {{ $after_update }}

            @can('delete', $model)
                <div class="dropdown-divider"></div>
                <a href="javascript:;" class="dropdown-item" onclick="confirm('Είστε σίγουρος;') || event.stopImmediatePropagation()" wire:click="remove({{ $model->id }})">
                    <i class="fas fa-trash"></i> Διαγραφή
                </a>
            @endcan
        @endif

        {{ $slot }}
    </div>
</div>
