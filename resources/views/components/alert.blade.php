<div
    {{
        $attributes
            ->class([
                'alert',
                "alert-{$type}",
                'd-flex',
                'align-items-center',
                'alert-dismissible' => $dismiss,
                'fade',
                'show',
            ])
    }}

    role="alert"
>
    <i class="fas fa-fw {{ $icon }} flex-shrink-0 me-2" role="img"></i>
    <div>
        @if ($message)
            {{ $message }}
        @else
            {{ $slot}}
        @endif
    </div>

    @if ($dismiss ?? true)
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Κλείσιμο"></button>
    @endif
</div>
