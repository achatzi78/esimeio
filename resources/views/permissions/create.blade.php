@extends('layouts.app')

@section('meta-title', 'Δικαιώματα::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:permissions.form />
        </div>
    </div>
@endsection
