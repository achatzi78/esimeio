@extends('layouts.app')

@section('meta-title', 'Δικαιώματα')

@section ('content')
    <div class="row">
        <div class="col-12">
            <livewire:permissions.index />
        </div>
    </div>
@endsection

