@extends('layouts.app')

@section('meta-title', 'Δικαιώματα::Προβολή')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Προβολή Δικαιώματος
                </div>

                <div class="card-body">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Τίτλος</dt>
                                <dd>{{ $permission->title }}</dd>
                            </dl>
                        </div>

                        {{-- action --}}
                        <div class="col-md-6 mt-3 mt-md-0">
                            <dl>
                                <dt>Ενέργεια</dt>
                                <dd>{{ $permission->action }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row">
                        {{-- description --}}
                        <div class="col-12 mt-3">
                            <dl>
                                <dt>Περιγραφή</dt>
                                <dd>{{ $permission->description }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        @can ('update', $permission)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="{{ route('permissions.edit', ['permission' => $permission]) }}" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endif

                        <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                            <a href="{{ route('permissions.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
