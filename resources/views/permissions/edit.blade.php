@extends('layouts.app')

@section('meta-title', 'Δικαιώματα::Επεξεργασία')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:permissions.form :permission="$permission" />
        </div>
    </div>
@endsection
