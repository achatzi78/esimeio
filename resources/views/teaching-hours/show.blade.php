@extends('layouts.app')

@section('meta-title', 'Διδακτικές Ώρες::Προβολή')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Προβολή Διδακτικής Ώρας
                </div>

                <div class="card-body">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Τίτλος</dt>
                                <dd>{{ $teaching_hour->title }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- start time --}}
                        <div class="col-md-6 col-lg-3 mt-3 mt-md-0">
                            <dl>
                                <dt>Χρόνος Έναρξης</dt>
                                <dd>{{ $teaching_hour->starts_at }}</dd>
                            </dl>
                        </div>

                        {{-- end time --}}
                        <div class="col-md-6 col-lg-3 mt-3 mt-md-0">
                            <dl>
                                <dt>Χρόνος Λήξης</dt>
                                <dd>{{ $teaching_hour->ends_at }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- academic year --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Ακαδημαϊκό Έτος</dt>
                                <dd>{{ $teaching_hour->academic_year->title }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        @can ('update', $teaching_hour)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="{{ route('teaching-hours.edit', ['teaching_hour' => $teaching_hour]) }}" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endif

                        <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                            <a href="{{ route('teaching-hours.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
