@extends('layouts.app')

@section('meta-title', 'Διδακτικές Ώρες::Επεξεργασία')

@section('form-title', 'Επεξεργασία Διδακτικής Ώρας')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:teaching-hours.form :teaching_hour_id="$teaching_hour->id" />
        </div>
    </div>
@endsection
