@extends('layouts.app')

@section('meta-title', 'Διδακτικές Ώρες')

@section ('content')
    <div class="row">
        <div class="col-12">
            <livewire:teaching-hours.index />
        </div>
    </div>
@endsection

