@extends('layouts.app')

@section('meta-title', 'Διδακτικές Ώρες::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:teaching-hours.form />
        </div>
    </div>
@endsection
