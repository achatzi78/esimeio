<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('meta-title') | {{ config('app.name') }} {{ app_version() }}</title>

        {{-- css --}}
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        @livewireStyles

        {{-- favicon --}}
        <link id="favicon_192" rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon-light-192x192.png') }}">
        <link id="favicon_96" rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon-light-96x96.png') }}">
        <link id="favicon_32" rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-light-32x32.png') }}">
        <link id="favicon_16" rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-light-16x16.png') }}">

        {{-- page css --}}
        @stack('page-css')

        {{-- dark mode --}}
        <script>
            const usesDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches || false;
            const favicon_16 = document.getElementById('favicon_16');
            const favicon_32 = document.getElementById('favicon_32');
            const favicon_96 = document.getElementById('favicon_96');
            const favicon_192 = document.getElementById('favicon_192');

            function switchIcon(usesDarkMode) {
                if (usesDarkMode) {
                    favicon_16.href = '{{ asset('favicon-light-16x16.png') }}';
                    favicon_32.href = '{{ asset('favicon-light-32x32.png') }}';
                    favicon_96.href = '{{ asset('favicon-light-96x96.png') }}';
                    favicon_192.href = '{{ asset('favicon-light-192x192.png') }}';
                }
                else {
                    favicon_16.href = '{{ asset('favicon-dark-16x16.png') }}';
                    favicon_32.href = '{{ asset('favicon-dark-32x32.png') }}';
                    favicon_96.href = '{{ asset('favicon-dark-96x96.png') }}';
                    favicon_192.href = '{{ asset('favicon-dark-192x192.png') }}';
                }
            }

            window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', (e) => switchIcon(e.matches));
            switchIcon(usesDarkMode);
        </script>
    </head>

    <body class="d-flex flex-column h-100">

        {{-- header --}}
        <header id="header">
            @include('partials.navbar')
        </header>

        {{-- content --}}
        <main role="main" class="flex-shrink-0 mb-3">
            <div class="container-fluid">
                {{ Breadcrumbs::render() }}

                @include('partials.errors')

                @include('partials.messages')

                @yield('content')
            </div>
        </main>

        {{-- footer --}}
        <footer class="footer mt-auto py-3 bg-gray-200">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center" style="margin-bottom: 0;">
                            <small>{{ config('app.name') }} {{ app_version() }} | Σημείο &copy; {{ date('Y') }}</small>
                        </p>
                    </div>
                </div>
            </div>
        </footer>

        {{-- app js --}}
        @livewireScripts
        <script src="{{ mix('js/app.js') }}"></script>
        <x-livewire-alert::scripts />
        @stack('page-js')
    </body>
</html>
