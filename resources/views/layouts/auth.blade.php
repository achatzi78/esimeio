<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('meta-title') | {{ config('app.name') }} {{ app_version() }}</title>

        {{-- css --}}
        <link href="{{ mix('css/auth.css') }}" rel="stylesheet">
        @livewireStyles
    </head>

    <body>

        <div class="w-100 vh-100 d-flex align-items-center bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">

                        <h3 class="mb-3">@yield('page-title')</h3>

                        <div class="bg-white shadow rounded">

                            <div class="row">
                                <div class="col-md-7 pe-0">
                                    <div class="form-left h-100 py-5 px-5">
                                        @include('partials.errors', ['full_width' => true])

                                        @yield('form')
                                    </div>
                                </div>

                                <div class="col-md-5 ps-0 d-none d-md-block">
                                    <div class="form-right h-100 bg-simeio text-white text-center p-5">
                                        <img src="{{ asset('storage/images/logo_white.png') }}" class="img-fluid">

                                        <h2 class="mt-3">{{ config('app.name') }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="text-end text-secondary mt-3">Εφαρμογή Διαχείρισης Φροντιστηρίου | Σημείο &copy; {{ date('Y') }}</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- app js --}}
        @livewireScripts
        <script src="{{ mix('js/app.js') }}"></script>
        <x-livewire-alert::scripts />
        @stack('page-js')
    </body>
</html>
