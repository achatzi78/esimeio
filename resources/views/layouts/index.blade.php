@extends('layouts.app')

@section('content')

    @section('filters-card')
        {!! Form::open(['url' => url()->current(), 'method' => 'get', 'role' => 'form']) !!}

        <div class="row">
            <div class="col-12">
                <div class="card border-main">
                    <div class="card-header text-white bg-simeio">
                        <a href="#" class="text-white {{ (request()->filled('filters')) ? '' : 'collapsed' }}" data-bs-toggle="collapse" data-bs-target=".filters_collapse" role="button" aria-expanded="{{ (request()->filled('filters')) ? 'true' : 'false' }}" aria-controls="filters_body filters_buttons">
                            <i class="fas fa-filter"></i> Φίλτρα
                        </a>
                    </div>
                    <div id="filters_body" class="card-body collapse {{ (request()->filled('filters')) ? 'show' : '' }} filters_collapse">
                        @yield('filters-form-inputs')
                    </div>

                    @section('filters-form-buttons')
                    <div id="filters_buttons" class="card-footer collapse {{ (request()->filled('filters')) ? 'show' : '' }} filters_collapse">
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <button type="submit" class="btn btn-primary col-12">
                                    <i class="fas fa-check"></i> Εφαρμογή
                                </button>
                            </div>

                            <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                                <a href="{!! url()->current() !!}" class="btn btn-outline-secondary col-12">
                                    <i class="fas fa-times"></i> Καθαρισμός
                                </a>
                            </div>
                        </div>
                    </div>
                    @show
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    @show

    @section('buttons')
        <div class="row mt-5">
            @section('add-button')
                <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 offset-md-4 offset-lg-9 offset-xl-10">
                    <a href="@yield('add-button-url')" class="btn btn-success col-12">
                        <i class="fas fa-plus"></i> Προσθήκη
                    </a>
                </div>
            @show
        </div>
    @show

    <hr>

    @section('top-pagination')
        @if ($rows instanceof \Illuminate\Pagination\AbstractPaginator)
            <div class="row mt-3">
                <div class="col-12 col-md-6">
                    {{ $rows->appends(request()->except(['page']))->links('partials.pagination.index') }}
                </div>

                <div class="col-12 col-md-6 text-center text-md-end">
                    Εμφανίζονται {{ $rows->firstItem() }} έως {{ $rows->lastItem() }} από {{ $rows->total() }} καταχωρήσεις
                </div>
            </div>
        @endif
    @show

    <div class="row mt-2">
        <div class="col-12">
            @yield('table')
        </div>
    </div>

    @section('bottom-pagination')
        @if ($rows instanceof \Illuminate\Pagination\AbstractPaginator)
            <div class="row mt-3">
                <div class="col-12 col-md-6">
                    {{ $rows->appends(request()->except(['page']))->links('partials.pagination.index') }}
                </div>

                <div class="col-12 col-md-6 text-center text-md-end">
                    Εμφανίζονται {{ $rows->firstItem() }} έως {{ $rows->lastItem() }} από {{ $rows->total() }} καταχωρήσεις
                </div>
            </div>
        @endif
    @show
@endsection
