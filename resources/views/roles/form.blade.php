<x-form-open :model="$model ?? null" :route="$route ?? null" :method="$method ?? 'GET'"></x-form-open>

<div class="card">
    <div class="card-header">
        @yield('form-title', 'Προσθήκη Ρόλου')
    </div>

    <div class="card-body">
        <div class="row">
            {{-- title --}}
            <div class="col-12">
                {!! Form::label('title', 'Τίτλος', ['class' => 'form-label']) !!}
                {!! Form::text('title', null, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'required' => 'required']) !!}
            </div>
        </div>

        <div class="row mt-3">
            {{-- hierarchy --}}
            <div class="col-md-6 col-lg-4 col-xl-2">
                {!! Form::label('hierarchy', 'Ιεραρχία', ['class' => 'form-label']) !!}
                {!! Form::number('hierarchy', null, ['class' => 'form-control' . ($errors->has('hierarchy') ? ' is-invalid' : ''), 'required' => 'required']) !!}
            </div>
        </div>

        @yield('sync-users')

        <p class="mt-3">Δικαιώματα</p>
        <div class="row">
            @foreach($permissions->split(2) as $permissions_group)
            <div class="col-md-6">

                @foreach($permissions_group as $permission)
                    <div class="form-check">
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['id' => 'role_permission_' . $permission->id, 'class' => 'form-check-input', (auth()->user()->is_allowed_to('change-user-role')) ? '' : 'disabled']) !!}
                        {!! Form::label('role_permission_' . $permission->id, $permission->title, ['class' => 'form-check-label', 'data-bs-toggle' => 'tooltip', 'data-bs-trigger' => 'hover', 'data-bs-placement' => 'right', 'title' => $permission->description]) !!}
                    </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <button type="submit" class="btn btn-primary col-12"><i class="fas fa-check"></i> Καταχώρηση</button>
            </div>

            <div class="col-sm-6 col-md-4 mt-2 mt-sm-0">
                <a href="{{ route('roles.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
