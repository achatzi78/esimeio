@extends('roles.index')

@section('form-title', 'Επεξεργασία Ρόλου')

@section('sync-users')
    <p class="mt-3">Εφαρμογή αλλαγών δικαιωμάτων στους χρήστες</p>
    <div class="row">
        <div class="col-12">
            <div class="form-check form-check-inline">
                {!! Form::radio('sync_users', 'N', null, ['id' => 'sync_users_N', 'class' => 'form-check-input']) !!}
                {!! Form::label('sync_users_N', 'Όχι', ['class' => 'form-label']) !!}
            </div>
            <div class="form-check form-check-inline">
                {!! Form::radio('sync_users', 'S', null, ['id' => 'sync_users_S', 'class' => 'form-check-input']) !!}
                {!! Form::label('sync_users_S', 'Συγχρονισμός', ['class' => 'form-label']) !!}
            </div>
            <div class="form-check form-check-inline">
                {!! Form::radio('sync_users', 'U', null, ['id' => 'sync_users_U', 'class' => 'form-check-input']) !!}
                {!! Form::label('sync_users_U', 'Ενημέρωση', ['class' => 'form-label']) !!}
            </div>

            <div class="form-text">
                <strong class="text-danger">Όχι:</strong> οι χρήστες <strong class="text-danger">ΔΕΝ</strong> θα ενημερωθούν με τις αλλαγές<br>
                <strong class="text-danger">Συγχρονισμός:</strong> οι χρήστες με τον ρόλο <span class="text-primary">{{ $selected_role->title }}</span> θα έχουν <strong class="text-danger">ΑΚΡΙΒΩΣ</strong> τα ίδια δικαιώματα με τον ρόλο<br>
                <strong class="text-danger">Ενημέρωση:</strong> οι χρήστες με τον ρόλο <span class="text-primary">{{ $selected_role->title }}</span> θα ενημερωθούν με τις προσθήκες και τις αφαιρέσεις που έχουν επιλεχθεί, <strong class="text-danger">ΧΩΡΙΣ</strong> να χάσουν άλλα δικαιώματα
            </div>
        </div>
    </div>
@endsection


