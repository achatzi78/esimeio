@extends('layouts.app')

@section('meta-title', 'Ρόλοι')

@section('content')

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="list-group">
                @foreach($roles as $role)
                    <a
                        href="{{ route('roles.edit', ['role' => $role]) }}"
                        @class([
                            'list-group-item',
                            'list-group-item-action',
                            'active' => $role->id == $selected_role->id
                        ])
                    >{{ $role->title }}</a>
                @endforeach
            </div>
        </div>

        <div class="col-sm-6 col-md-8 mt-5 mt-sm-0">

            <livewire:roles.form :role="$selected_role" />

        </div>
    </div>

@endsection
