@extends('layouts.app')

@section('meta-title', 'Τμήματα::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:faculties.form />
        </div>
    </div>
@endsection
