@extends('layouts.app')

@section('meta-title', 'Τμήματα::Προβολή')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Προβολή Τμήματος
                </div>

                <div class="card-body">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Τίτλος</dt>
                                <dd>{{ $faculty->title }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- subjects --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Μαθήματα</dt>
                                <dd>
                                    <ul class="list-unstyled">
                                        @foreach($faculty->subjects as $subject)
                                            <li>{{ $subject->title }}</li>
                                        @endforeach
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        @can ('update', $faculty)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="{{ route('faculties.edit', ['faculty' => $faculty]) }}" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endif

                        <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                            <a href="{{ route('faculties.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
