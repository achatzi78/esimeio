@extends('layouts.app')

@section('meta-title', 'Τμήματα')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:faculties.index />
        </div>
    </div>
@endsection

