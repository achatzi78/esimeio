@extends('layouts.app')

@section('meta-title', 'Τμήματα::Επεξεργασία')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:faculties.form :faculty="$faculty" />
        </div>
    </div>
@endsection
