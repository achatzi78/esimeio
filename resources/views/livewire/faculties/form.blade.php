<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->is_new)
                Προσθήκη Τμήματος
            @else
                Επεξεργασία Τμήματος
            @endif
        </div>

        <div class="card-body">
            <fieldset wire:loading.attr="disabled">

                {{-- title --}}
                <div class="row">
                    <div class="col-md-6">
                        <label for="title" class="form-label required">
                            <span wire:loading wire:target="faculty.title" class="spinner-border spinner-border-sm"></span> Τίτλος
                        </label>
                        <input
                            wire:model.lazy="faculty.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('faculty.title')
                            ])
                        >

                        @error('faculty.title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>

                {{-- subjects --}}
                <div class="row mt-4">
                    <label class="form-label required">
                        <span wire:loading wire:target="subjects" class="spinner-border spinner-border-sm"></span> Μαθήματα
                    </label>

                    @error('subjects')
                        <div class="text-danger">
                            <small>{!! $message !!}</small>
                        </div>
                    @enderror

                    @foreach ($this->subjects_list->split(3) as $subject_group)
                        <div class="col-md-4">
                            @foreach ($subject_group as $subject)
                                <div class="form-check">
                                    <input
                                        wire:model="subjects"
                                        wire:loading.attr="disabled"
                                        id="subject_{{ $subject->id }}"
                                        name="subjects[]"
                                        type="checkbox"
                                        value="{{ $subject->id }}"
                                        @class([
                                            'form-check-input',
                                            'is-invalid' => $errors->has('subjects')
                                        ])
                                    >
                                    <label class="form-check-label" for="subject_{{ $subject->id }}">
                                        {{ $subject->title }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>

            </fieldset>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
