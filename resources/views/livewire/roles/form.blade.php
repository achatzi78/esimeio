<form wire:submit.prevent="save">
    <div class="card">
        <div class="card-header">
            @if ($this->is_new)
                Προσθήκη Ρόλου
            @else
                Επεξεργασία Ρόλου
            @endif
        </div>

        <div class="card-body">
            <fieldset wire:loading.attr="disabled">
                <div class="row">
                    {{-- title --}}
                    <div class="col-sm-9 col-xl-5 col-xxl-6 mt-4 mt-sm-0">
                        <label for="title" class="form-label required">
                            <span wire:loading wire:target="role.title" class="spinner-border spinner-border-sm"></span> Τίτλος
                        </label>
                        <input
                            wire:model.lazy="role.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('role.title')
                            ])
                        >
                        @error('role.title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    {{-- hierarchy --}}
                    <div class="col-sm-9 col-xl-5 col-xxl-6 mt-4 mt-sm-0">
                        <label for="hierarchy" class="form-label required">
                            <span wire:loading wire:target="role.hierarchy" class="spinner-border spinner-border-sm"></span> Ιεραρχία
                        </label>
                        <input
                            wire:model.lazy="role.hierarchy"
                            name="hierarchy"
                            type="number"
                            min="0"
                            step="1"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('role.hierarchy')
                            ])
                        >
                        @error('role.hierarchy')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                @if (!$this->is_new)
                    <div class="row mt-4">
                        <div class="col-12">
                            <label class="form-label">
                                <span wire:loading wire:target="sync_users" class="spinner-border spinner-border-sm"></span> Εφαρμογή αλλαγών δικαιωμάτων στους χρήστες
                            </label>

                            @error('sync_users')
                            <div class="text-danger">
                                <small>{{ $message }}</small>
                            </div>
                            @enderror

                            <div class="space-x-2">
                                <div class="form-check form-check-inline">
                                    <input
                                        wire:model="sync_users"
                                        id="sync_users_N"
                                        name="sync_users"
                                        type="radio"
                                        value="N"
                                        @class([
                                            'form-check-input',
                                            'is-invalid' => $errors->has('sync_users')
                                        ])
                                    >
                                    <label class="form-check-label" for="sync_users_N">Όχι</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input
                                        wire:model="sync_users"
                                        id="sync_users_S"
                                        name="sync_users"
                                        type="radio"
                                        value="S"
                                        @class([
                                            'form-check-input',
                                            'is-invalid' => $errors->has('sync_users')
                                        ])
                                    >
                                    <label class="form-check-label" for="sync_users_S">Συγχρονισμός</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input
                                        wire:model="sync_users"
                                        id="sync_users_U"
                                        name="sync_users"
                                        type="radio"
                                        value="U"
                                        @class([
                                            'form-check-input',
                                            'is-invalid' => $errors->has('sync_users')
                                        ])
                                    >
                                    <label class="form-check-label" for="sync_users_U">Ενημέρωση</label>
                                </div>
                            </div>

                            <div class="form-text">
                                <strong class="text-danger">Όχι:</strong> οι χρήστες <strong class="text-danger">ΔΕΝ</strong> θα ενημερωθούν με τις αλλαγές<br>
                                <strong class="text-danger">Συγχρονισμός:</strong> οι χρήστες με τον ρόλο <span class="text-primary">{{ $role->title }}</span> θα έχουν <strong class="text-danger">ΑΚΡΙΒΩΣ</strong> τα ίδια δικαιώματα με τον ρόλο<br>
                                <strong class="text-danger">Ενημέρωση:</strong> οι χρήστες με τον ρόλο <span class="text-primary">{{ $role->title }}</span> θα ενημερωθούν με τις προσθήκες και τις αφαιρέσεις που έχουν επιλεχθεί, <strong class="text-danger">ΧΩΡΙΣ</strong> να χάσουν άλλα δικαιώματα
                            </div>
                        </div>
                    </div>
                @endif

                {{-- permissions --}}
                <div class="row mt-4">
                    <label class="form-label required">
                        <span wire:loading wire:target="role_permissions" class="spinner-border spinner-border-sm"></span> Δικαιώματα
                    </label>

                    @error('role_permissions')
                    <div class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror

                    @foreach ($this->permissions_list->split(2) as $permissions_list_group)
                        <div
                            @class([
                                'col-md-6',
                                'mt-4 mt-md-0' => ($loop->iteration != 1)
                            ])
                        >
                            @foreach ($permissions_list_group as $permissions_list_key => $permissions_list)
                                <label class="mt-3 fw-bold">{{ $permissions_list_key }}</label>
                                <div class="space-y-2">
                                    @foreach($permissions_list as $permission)
                                        <div class="form-check">
                                            <input
                                                wire:model="role_permissions"
                                                wire:key="role_permission_{{ $permission->id }}"
                                                wire:loading.attr="disabled"
                                                id="role_permission_{{ $permission->id }}"
                                                name="permissions[]"
                                                type="checkbox"
                                                value="{{ $permission->id }}"
                                                @class([
                                                    'form-check-input',
                                                    'is-invalid' => $errors->has('role_permissions')
                                                ])
                                            >
                                            <label
                                                class="form-check-label"
                                                for="role_permission_{{ $permission->id }}"
                                                title="{{ $permission->description }}"
                                                data-bs-toggle="tooltip"
                                            >{{ $permission->title }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>

                <div wire:loading.remove class="col-12">
                    <a class="btn btn-outline-secondary col-12" href="{{ route('roles.index') }}">
                        <i class="fas fa-times me-2"></i> Ακύρωση
                    </a>
                </div>
            </div>
        </div>

    </div>
</form>
