@extends('livewire.index')

@section('filters')
    <div class="row">
        {{-- title --}}
        <div class="col-md-6 col-lg-3">
            <x-livewire.filter-title></x-livewire.filter-title>
        </div>
    </div>
@endsection

@section('buttons')
    @can('create', \App\Models\Classroom::class)
        <div class="row mt-4 mb-4 justify-content-center justify-content-lg-end">
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <a href="{{ route('classrooms.create') }}" class="btn btn-success col-12 text-capitalize">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        </div>

        <hr>
    @endcan
@endsection

@section('table')
    <div class="table-responsive-lg">
        <table class="table table-bordered align-middle m-0">
            <thead class="table-light">
            <tr>
                <th class="text-center" style="width: 5%">#</th>
                <th class="text-center" style="width: 10%"></th>
                <x-livewire.table-head sort-by="title" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                    Τίτλος
                </x-livewire.table-head>
            </tr>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td class="text-end">{{ $loop->iteration }}</td>
                    <td>
                        <x-livewire.buttons-crud :model="$row" :prefix="'classrooms'" :parameter="'classroom'"></x-livewire.buttons-crud>
                    </td>
                    <td>{{ $row->title }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
