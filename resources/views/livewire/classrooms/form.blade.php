<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->is_new)
                Προσθήκη Αίθουσας
            @else
                Επεξεργασία Αίθουσας
            @endif
        </div>

        <div class="card-body">
            <fieldset wire:loading.attr="disabled">

                {{-- title --}}
                <div class="row">
                    <div class="col-md-6">
                        <label for="title" class="form-label required">
                            <span wire:loading wire:target="classroom.title" class="spinner-border spinner-border-sm"></span> Τίτλος
                        </label>
                        <input
                            wire:model.lazy="classroom.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('classroom.title')
                            ])
                        >

                        @error('classroom.title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

            </fieldset>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
