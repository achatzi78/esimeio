<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->creates)
                Προσθήκη Μαθήματος
            @else
                Επεξεργασία Μαθήματος
            @endif
        </div>

        <div class="card-body">

            {{-- title --}}
            <div class="row">
                <div class="col-md-6">
                    <label for="title" class="form-label">
                        <span wire:loading wire:target="subject.title" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Τίτλος
                    </label>
                    <input
                        wire:model.lazy="subject.title"
                        id="title"
                        name="title"
                        type="text"
                        maxlength="255"
                        aria-describedby="validation_title_feedback"
                        @class([
                            'form-control',
                            'is-invalid' => $errors->has('subject.title'),
                        ])
                    >
                    @error('subject.title')
                    <div id="validation_title_feedback" class="invalid-feedback">
                        {!! $message !!}
                    </div>
                    @enderror
                </div>
            </div>

            {{-- faculties --}}
            <div class="row mt-3">
                <label class="form-label">
                    <span wire:loading wire:target="faculties" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Τμήματα
                </label>

                @error('faculties')
                    <div class="form-text text-danger">
                        {!! $message !!}
                    </div>
                @enderror

                @foreach ($this->faculties_list->split(3) as $faculty_group)
                    <div class="col-md-4">
                        @foreach ($faculty_group as $faculty)
                            <div class="form-check">
                                <input
                                    wire:model="faculties"
                                    wire:loading.attr="disabled"
                                    id="faculty_{{ $faculty->id }}"
                                    name="faculties[]"
                                    type="checkbox"
                                    value="{{ $faculty->id }}"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('faculties')
                                    ])
                                >
                                <label class="form-check-label" for="faculty_{{ $faculty->id }}">
                                    {{ $faculty->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>

            {{-- teachers --}}
            <div class="row mt-3">
                <label class="form-label">
                    <span wire:loading wire:target="teachers" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Καθηγητές
                </label>

                @error('teachers')
                <div class="form-text text-danger">
                    {!! $message !!}
                </div>
                @enderror

                @foreach ($this->teachers_list->split(3) as $teacher_group)
                    <div class="col-md-4">
                        @foreach ($teacher_group as $teacher)
                            <div class="form-check">
                                <input
                                    wire:model="teachers"
                                    wire:loading.attr="disabled"
                                    id="teacher_{{ $teacher->id }}"
                                    name="teachers[]"
                                    type="checkbox"
                                    value="{{ $teacher->id }}"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('teachers')
                                    ])
                                >
                                <label class="form-check-label" for="teacher_{{ $teacher->id }}">
                                    {{ $teacher->official_name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>

        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
