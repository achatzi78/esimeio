<div>
    {{-- filters --}}
    <div class="row">
        <div class="col-12">
            <div class="card border-main">
                <div class="card-header text-white bg-simeio">
                    <a href="#" class="text-white {{ (count($filters)) ? 'collapsed' : '' }}" data-bs-toggle="collapse" data-bs-target=".filters_collapse" role="button" aria-expanded="{{ (request()->filled('filters')) ? 'true' : 'false' }}" aria-controls="filters_body filters_buttons">
                        <i class="fas fa-filter"></i> Φίλτρα
                    </a>
                </div>

                <div id="filters_body" class="card-body collapse {{ (count($filters)) ? 'show' : '' }} filters_collapse">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6 col-lg-4">
                            <label for="title_filter" class="form-label">Τίτλος</label>
                            <input
                                wire:model.debounce.500ms="filters.title"
                                id="title_filter"
                                name="filters[title]"
                                type="text"
                                class="form-control"
                            >
                        </div>

                        {{-- teacher --}}
                        <div class="col-md-6 col-lg-4">
                            <label for="teacher_filter" class="form-label">Καθηγητής</label>
                            <select
                                wire:model="filters.teacher"
                                id="teacher_filter"
                                name="filters[teacher]"
                                type="text"
                                class="form-select"
                            >
                                <option></option>
                                @if ($this->filtered_teacher)
                                    <option value="{{ $this->filtered_teacher->id }}" selected>{{ $this->filtered_teacher->fullname }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div id="filters_buttons" class="card-footer collapse {{ (count($filters)) ? 'show' : '' }} filters_collapse">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <a href="{{ route('subjects.index') }}" class="btn btn-outline-secondary col-12">
                                <i class="fas fa-times"></i> Καθαρισμός
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- buttons --}}
    <div class="row mt-5">
        @can('create', \App\Models\Faculty::class)
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 offset-md-4 offset-lg-9 offset-xl-10">
                <a href="{{ route('subjects.create') }}" class="btn btn-success col-12">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        @endcan
    </div>

    {{-- top pagination --}}
    <div class="row mt-4">
        <div class="col-12 col-md-6">
            {{ $rows->appends(request()->except(['page']))->links() }}
        </div>

        <div class="col-12 col-md-6 text-center text-md-end">
            Εμφανίζονται {{ $rows->firstItem() }} έως {{ $rows->lastItem() }} από {{ $rows->total() }} καταχωρήσεις
        </div>
    </div>

    {{-- loader --}}
    <div  class="row" style="height: 8px;">
        <div wire:loading class="col-12">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>

    {{-- table --}}
    <div class="row">
        <div class="col-12">
            <div class="table-responsive-lg">
                <table class="table table-bordered align-middle">
                    <thead class="thead-light">
                    <tr>
                        <th class="text-center" style="width: 5%">#</th>
                        <th class="text-center" style="width: 10%"></th>
                        <x-table-head sort-by="title" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}" style="width: 20%;">
                            Τίτλος
                        </x-table-head>
                        <th style="width: 30%">Τμήματα</th>
                        <th style="width: 35%">Καθηγητές</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rows as $subject)
                        <tr>
                            <td class="text-end">{{ $loop->iteration }}</td>
                            <td>
                                <x-crud :model="$subject" :prefix="'subjects'" :parameter="'subject'" :livewire="true"></x-crud>
                            </td>
                            <td>{{ $subject->title }}</td>
                            <td>
                                {{ $subject->faculties->implode('title', ', ') }}
                            </td>
                            <td>
                                {{ $subject->teachers->implode('fullname', ', ') }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@push('page-css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-5-theme/1.2.0/select2-bootstrap-5-theme.min.css" rel="stylesheet" />
@endpush

@push('page-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/el.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#teacher_filter').select2({
                language: 'el',
                theme: 'bootstrap-5',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('api.users.index') }}',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filters: {
                                'role': {{ ROLE_TEACHER }}
                            },
                            search: {
                                'name': params.term,
                                'email': params.term,
                            },
                            page: params.page
                        }
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: $.map(data.users, function (obj) {
                                return {
                                    id: obj.id,
                                    text: obj.fullname
                                };
                            }),
                            pagination: {
                                more: (params.page * data.meta.per_page) < data.meta.total
                            },
                        }
                    },
                    cache: true
                }
            });

            $('#teacher_filter').on('change', function (e) {
                @this.set('filters.teacher', $(this).select2('val'));
            });
        });

    </script>
@endpush
