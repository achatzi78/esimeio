<form wire:submit.prevent="login">
    <fieldset wire:loading.attr="disabled">
        <div class="d-flex flex-column">
            {{-- email --}}
            <div class="mb-4">
                <label for="email" class="form-label required">Email</label>
                <input
                    wire:model.defer="email"
                    id="email"
                    name="email"
                    type="email"
                    @class([
                        'form-control',
                        'is-invalid' => $errors->has('email')
                    ])
                >
                @error('email')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
                @enderror
            </div>

            {{-- password --}}
            <div class="mb-4">
                <label for="password" class="form-label required">Κωδικός</label>
                <input
                    wire:model.defer="password"
                    id="password"
                    name="password"
                    type="password"
                    @class([
                        'form-control',
                        'is-invalid' => $errors->has('password')
                    ])
                >
                @error('password')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
                @enderror
            </div>

            {{-- rememeber me --}}
            <div class="mb-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="remember_me" name="remember">
                    <label class="form-check-label" for="remember_me">Να με θυμάσαι</label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="d-flex justify-content-between">
        @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}" class="text-primary text-decoration-none">Ξεχάσατε τον κωδικό σας;</a>
        @endif

        <button type="submit" class="btn px-4 btn-primary">
            <i wire:loading.class="fa-circle-notch fa-spin" class="fa-solid fa-check me-1"></i> Σύνδεση
        </button>
    </div>
</form>
