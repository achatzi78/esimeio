<form wire:submit.prevent="save">
    <fieldset wire:loading.attr="disabled">
        <div class="d-flex flex-column">
            {{-- email --}}
            <div class="mb-4">
                <label for="email" class="form-label required">Email</label>
                <input
                    wire:model.defer="email"
                    id="email"
                    name="email"
                    type="email"
                    @class([
                        'form-control',
                        'is-invalid' => $errors->has('email')
                    ])
                >
                @error('email')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
                @enderror
            </div>

            {{-- password --}}
            <div class="mb-4">
                <label for="password" class="form-label required">Κωδικός</label>
                <input
                    wire:model.defer="password"
                    id="password"
                    name="password"
                    type="password"
                    @class([
                        'form-control',
                        'is-invalid' => $errors->has('password')
                    ])
                >
                @error('password')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
                @enderror
            </div>

            {{-- password confirmation --}}
            <div class="mb-4">
                <label for="password_confirmation" class="form-label required">Επιβεβαίωση Κωδικού</label>
                <input
                    wire:model.defer="password_confirmation"
                    id="password_confirmation"
                    name="password_confirmation"
                    type="password"
                    @class([
                        'form-control',
                        'is-invalid' => $errors->has('password_confirmation')
                    ])
                >
                @error('password_confirmation')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </fieldset>

    <div class="d-flex justify-content-end">
        <button type="submit" class="btn px-4 btn-primary">
            <i wire:loading.class="fa-circle-notch fa-spin" class="fa-solid fa-check me-1"></i> Αποθήκευση
        </button>
    </div>
</form>
