<form wire:submit.prevent="send">
    <p>Εισάγετε το email που χρησιμοποιείτε για να συνδεθείτε στην εφαρμογή και θα σας σταλεί ένας σύνδεσμος επαναφοράς κωδικού.</p>

    <fieldset wire:loading.attr="disabled">
        <div class="mb-4">
            <label for="email" class="form-label text-capitalize required">{{ __('email') }}</label>
            <input
                wire:model.defer="email"
                id="email"
                name="email"
                type="email"
                @class([
                    'form-control',
                    'is-invalid' => $errors->has('email')
                ])
            >
            @error('email')
            <div class="invalid-feedback" role="alert">{{ $message }}</div>
            @enderror
        </div>
    </fieldset>

    <div class="d-flex justify-content-between">
        <a href="{{ route('login') }}" class="text-primary text-decoration-none">Σύνδεση</a>

        <button type="submit" class="btn px-4 btn-primary">
            <i wire:loading.class="fa-circle-notch fa-spin" class="fa-solid fa-check me-1"></i> Αποστολή Συνδέσμου
        </button>
    </div>
</form>
