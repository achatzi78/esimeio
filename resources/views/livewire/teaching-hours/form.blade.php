<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->creates)
                Προσθήκη Διδακτικής Ώρας
            @else
                Επεξεργασία Διδακτικής Ώρας
            @endif
        </div>

        <div class="card-body">

            <div class="row mt-4">
                {{-- start time --}}
                <div class="col-md-3">
                    <label for="start_time" class="form-label">
                        <span wire:loading wire:target="start_time" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Έναρξη
                    </label>
                    <input
                        wire:model.lazy="start_time"
                        id="start_time"
                        name="start_time"
                        type="text"
                        maxlength="255"
                        list="teaching_hours_start_times"
                        aria-describedby="validation_start_time_feedback"
                        @class([
                            'form-control',
                            'is-invalid' => $errors->has('start_time'),
                        ])
                    >
                    @error('start_time')
                        <div id="validation_start_time_feedback" class="invalid-feedback">
                            {!! $message !!}
                        </div>
                    @enderror

                    <datalist id="teaching_hours_start_times">
                        @foreach(teaching_hours_times() as $teaching_hour_time)
                            <option value="{{ $teaching_hour_time }}">
                        @endforeach
                    </datalist>
                </div>

                {{-- end time --}}
                <div class="col-md-3 mt-4 mt-md-0">
                    <label for="end_time" class="form-label">
                        <span wire:loading wire:target="end_time" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Λήξη
                    </label>
                    <input
                        wire:model.lazy="end_time"
                        id="end_time"
                        name="end_time"
                        type="text"
                        maxlength="255"
                        list="teaching_hours_end_times"
                        aria-describedby="validation_end_time_feedback"
                        step="300"
                        @class([
                            'form-control',
                            'is-invalid' => $errors->has('end_time'),
                        ])
                    >
                    @error('end_time')
                        <div id="validation_end_time_feedback" class="invalid-feedback">
                            {!! $message !!}
                        </div>
                    @enderror

                    <datalist id="teaching_hours_end_times">
                        @foreach(teaching_hours_times() as $teaching_hour_time)
                            <option value="{{ $teaching_hour_time }}">
                        @endforeach
                    </datalist>
                </div>
            </div>

            {{-- academic year --}}
            <div class="row mt-4">
                <div class="col-md-3">
                    <label for="academic_year_id" class="form-label">
                        <span wire:loading wire:target="academic_year_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Ακαδημαϊκό Έτος
                    </label>
                    <select
                        wire:model="academic_year_id"
                        id="academic_year_id"
                        name="academic_year_id"
                        aria-describedby="validation_academic_year_id_feedback"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('academic_year_id'),
                        ])
                    >
                        <option></option>
                        @foreach($academic_years as $academic_year)
                            <option value="{{ $academic_year->id }}">{{ $academic_year->title }}</option>
                        @endforeach
                    </select>
                    @error('academic_year_id')
                        <div id="validation_academic_year_id_feedback" class="invalid-feedback">
                            {!! $message !!}
                        </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
