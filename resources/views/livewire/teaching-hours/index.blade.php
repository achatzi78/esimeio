@extends('livewire.index')

@section('filters')
    <div class="row">
        {{-- title --}}
        <div class="col-md-6">
            <x-livewire.filter-title></x-livewire.filter-title>
        </div>

        {{-- academic years --}}
        <div class="col-md-6 col-lg-3">
            <label for="academic_year_filter" class="form-label mt-3 mt-md-0">Ακαδημαϊκό Έτος</label>
            <select
                wire:model="filters.academic_year"
                id="academic_year_filter"
                name="filters[academic_year]"
                class="form-select"
            >
                <option></option>
                @foreach($academic_years as $academic_year)
                    <option value="{{ $academic_year->id }}">{{ $academic_year->title }}</option>
                @endforeach
            </select>
        </div>
    </div>
@endsection

@section('buttons')
    @can('create', \App\Models\TeachingHour::class)
        <div class="row mt-4 mb-4 justify-content-center justify-content-lg-end">
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <a href="{{ route('teaching-hours.create') }}" class="btn btn-success col-12">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        </div>

        <hr>
    @endcan
@endsection

@section('table')
    <div class="table-responsive-lg">
        <table class="table table-bordered align-middle m-0">
            <thead class="table-light">
                <tr>
                    <th class="text-center" style="width: 5%">#</th>
                    <th class="text-center" style="width: 10%"></th>
                    <x-livewire.table-head sort-by="academic_year_id" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}" style="width: 20%;">
                        Ακαδημαϊκό Έτος
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="title" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        Τίτλος
                    </x-livewire.table-head>
                </tr>
            </thead>
            <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td class="text-end">{{ $loop->iteration }}</td>
                        <td>
                            <x-livewire.buttons-crud :model="$row" :prefix="'teaching-hours'" :parameter="'teaching_hour'"></x-livewire.buttons-crud>
                        </td>
                        <td>{{ $row->academic_year->title }}</td>
                        <td>{{ $row->title }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
