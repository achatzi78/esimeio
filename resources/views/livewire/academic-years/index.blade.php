@extends('livewire.index')

@section('filters')
    <div class="row">
        {{-- title --}}
        <div class="col-md-6 col-lg-3">
            <x-livewire.filter-title></x-livewire.filter-title>
        </div>

        {{-- start year --}}
        <div class="col-md-6 col-lg-3">
            <label for="start_year_filter" class="form-label mt-3 mt-md-0">Έτος Έναρξης</label>
            <input
                wire:model.lazy="filters.start_year"
                id="start_year_filter"
                name="filters[start_year]"
                type="number"
                class="form-control"
                min="2020"
                step="1"
            >
        </div>

        {{-- end year --}}
        <div class="col-md-6 col-lg-3">
            <label for="end_year_filter" class="form-label mt-3 mt-md-0">Έτος Λήξης</label>
            <input
                wire:model.lazy="filters.end_year"
                id="end_year_filter"
                name="filters[end_year]"
                type="number"
                class="form-control"
                min="2020"
                step="1"
            >
        </div>

        {{-- current --}}
        <div class="col-md-6 col-lg-3">
            <label for="current_filter" class="form-label mt-3 mt-md-0">Τρέχων</label>
            <select
                wire:model="filters.current"
                id="current_filter"
                name="filters[current]"
                class="form-select"
            >
                <option></option>
                <option value="1">Ναι</option>
                <option value="0">Όχι</option>
            </select>
        </div>
    </div>
@endsection

@section('buttons')
    @can('create', \App\Models\AcademicYear::class)
        <div class="row mt-4 mb-4 justify-content-center justify-content-lg-end">
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <a href="{{ route('academic-years.create') }}" class="btn btn-success col-12 text-capitalize">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        </div>

        <hr>
    @endcan
@endsection

@section('table')
    <div class="table-responsive-lg">
        <table class="table table-bordered align-middle m-0">
            <thead class="table-light">
                <tr>
                    <th class="text-center" style="width: 10%"></th>
                    <x-livewire.table-head sort-by="title" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        Τίτλος
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="start_year" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}" style="width: 15%;">
                        Έτος Έναρξης
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="end_year" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}" style="width: 15%;">
                        Έτος Λήξης
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="is_current" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}" style="width: 10%;">
                        Τρέχων
                    </x-livewire.table-head>
                </tr>
            </thead>
            <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td>
                            <x-livewire.buttons-crud :model="$row" :prefix="'academic-years'" :parameter="'academic_year'"></x-livewire.buttons-crud>
                        </td>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->start_year }}</td>
                        <td>{{ $row->end_year }}</td>
                        <td class="text-center">
                            <x-icon-active :active="$row->is_current"></x-icon-active>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
