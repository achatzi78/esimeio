<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->is_new)
                Προσθήκη Ακαδημαϊκού Έτους
            @else
                Επεξεργασία Ακαδημαϊκού Έτους
            @endif
        </div>

        <div class="card-body">
            <fieldset wire:loading.attr="disabled">

                {{-- title --}}
                <div class="row">
                    <div class="col-md-6">
                        <label for="title" class="form-label required">
                            <span wire:loading wire:target="academic_year.title" class="spinner-border spinner-border-sm"></span> Τίτλος
                        </label>
                        <input
                            wire:model.lazy="academic_year.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('academic_year.title')
                            ])
                        >

                        @error('academic_year.title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="row mt-3">
                    {{-- start year --}}
                    <div class="col-md-3">
                        <label for="start_year" class="form-label required">
                            <span wire:loading wire:target="academic_year.start_year" class="spinner-border spinner-border-sm"></span> Έτος Έναρξης
                        </label>
                        <input
                            wire:model.lazy="academic_year.start_year"
                            name="start_year"
                            type="number"
                            min="2021"
                            step="1"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('academic_year.start_year')
                            ])
                        >

                        @error('academic_year.start_year')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    {{-- end year --}}
                    <div class="col-md-3 mt-3 mt-md-0">
                        <label for="end_year" class="form-label required">
                            <span wire:loading wire:target="academic_year.end_year" class="spinner-border spinner-border-sm"></span> Έτος Λήξης
                        </label>
                        <input
                            wire:model.lazy="academic_year.end_year"
                            name="end_year"
                            type="number"
                            min="2021"
                            step="1"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('academic_year.end_year')
                            ])
                        >

                        @error('academic_year.end_year')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                {{-- current --}}
                <div class="row mt-3">
                    <div class="col-md-6">
                        <label class="form-label required">
                            <span wire:loading wire:target="academic_year.is_current" class="spinner-border spinner-border-sm"></span> Τρέχων
                        </label>

                        <div>
                            <div class="form-check form-check-inline">
                                <input
                                    wire:model="academic_year.is_current"
                                    id="academic_year_is_current_1"
                                    name="is_current"
                                    type="radio"
                                    value="1"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('academic_year.is_current')
                                    ])
                                >
                                <label class="form-check-label text-capitalize" for="academic_year_is_current_1">Ναι</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    wire:model="academic_year.is_current"
                                    id="academic_year_is_current_0"
                                    name="is_current"
                                    type="radio"
                                    value="0"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('academic_year.is_current')
                                    ])
                                >
                                <label class="form-check-label text-capitalize" for="academic_year_is_current_0">Όχι</label>
                            </div>
                        </div>

                        @error('academic_year.is_current')
                            <div class="text-danger">
                                <small>{{ $message }}</small>
                            </div>
                        @enderror

                        <div
                            @class([
                                'text-orange-500',
                                'd-none' => !$current_year_conficts
                            ])
                        >
                            <small class="fw-bold"><i class="fas fa-triangle-exclamation me-1"></i> ΠΡΟΣΟΧΗ: Υπάρχει ήδη άλλο τρέχων ακαδημαϊκό έτος!</small>
                        </div>

                    </div>
                </div>

                {{-- months --}}
                <div class="row mt-3">
                    <div class="col-12">
                        <label class="form-label required">
                            <span wire:loading wire:target="months" class="spinner-border spinner-border-sm"></span> Μήνες
                        </label>

                        @error('months')
                            <div class="text-danger">
                                <small>{{ $message }}</small>
                            </div>
                        @enderror

                        @foreach (\App\Enums\Months::toArray() as $month_number => $month_name)
                            <div class="form-check">
                                <input
                                    wire:model="months"
                                    wire:loading.attr="disabled"
                                    wire:loading.target="months"
                                    id="months_{{ $month_number }}"
                                    name="months[]"
                                    type="checkbox"
                                    value="{{ $month_number }}"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('months'),
                                    ])
                                >
                                <label for="months_{{ $month_number }}" class="form-check-label">{{ $month_name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
