<div>
    <div wire:loading>
        <x-page-loader />
    </div>

    @include('partials.messages')

    <div>
        <div class="card">
            <div class="card-header">
                @yield('form-title')
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- title --}}
                    <div class="col-md-6">
                        <label for="title" class="form-label">Τίτλος</label>
                        <input
                            wire:model="schedule.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('schedule.title'),
                            ])
                        >
                    </div>

                    {{-- academic year --}}
                    <div class="col-md-6 col-lg-3 mt-4 mt-md-0">
                        <label for="academic_year_id" class="form-label">Ακαδημαϊκό Έτος</label>
                        <select
                            wire:model="schedule.academic_year_id"
                            name="academic_year_id"
                            @class([
                                'form-select',
                                'is-invalid' => $errors->has('academic_year_id')
                            ])
                        >
                            <option></option>
                            @foreach ($academic_years as $academic_year)
                                <option value="{{ $academic_year->id }}">{{ $academic_year->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if ($schedule->academic_year_id)
                    <hr>

                    <livewire:schedules.schedule-line-form :academic_year_id="$schedule->academic_year_id" />

                    <hr>

                    <livewire:schedules.calendar :academic_year_id="$schedule->academic_year_id" :lines="$this->lines" />
                @endif

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <button wire:click="save" type="button" class="btn btn-primary col-12"><i class="fas fa-check"></i> Υποβολή</button>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <a href="@yield('cancel-route')" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
