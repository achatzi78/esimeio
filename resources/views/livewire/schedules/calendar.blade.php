<div>
    <div wire:loading>
        <x-page-loader></x-page-loader>
    </div>

    <h3 class="text-primary text-center">Ημερολόγιο</h3>

    @foreach(weekdays() as $day_num => $day_name)
        <div class="row mt-3">
            <div class="col-12">
                <h4>{{ $day_name }}</h4>
                <table id="schedule_day_{{ $loop->iteration }}" class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            @foreach($teaching_hours as $teaching_hour)
                                <th>{{ $teaching_hour->title }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($teachers as $teacher)
                            <tr>
                                <th>{{ $teacher->official_name }}</th>
                                @foreach($teaching_hours as $teaching_hour)
                                    <td>
                                        @foreach($faculties as $faculty)
                                            @php($key = "{$day_num}-{$teacher->id}-{$faculty->id}-{$teaching_hour->id}")
                                            @if ($lines->get($key))
                                                <span class="badge rounded-pill bg-info text-dark">{{ $lines->get($key)->faculty->title }} - {{ $lines->get($key)->subject->title }} - {{ $lines->get($key)->classroom->title }}</span>
                                            @endif
                                        @endforeach
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
</div>
