<div>
    <div wire:loading wire:target="addLine">
        <x-page-loader>Προσθήκη Μαθήματος...</x-page-loader>
    </div>

    <fieldset wire:loading.attr="disabled">
        <div class="row mb-4">
            {{-- department --}}
            <div class="col-md-6 col-lg-4">
                <label for="faculty_id" class="form-label">
                    <span wire:loading wire:target="schedule_line.faculty_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Τμήμα
                </label>
                <select
                    wire:model="schedule_line.faculty_id"
                    name="faculty_id"
                    @class([
                        'form-select',
                        'is-invalid' => $errors->has('faculty_id')
                    ])
                >
                    <option></option>
                    @foreach ($faculties as $faculty)
                        <option value="{{ $faculty->id }}">{{ $faculty->title }}</option>
                    @endforeach
                </select>
            </div>

            {{-- subject --}}
            @if (!empty($schedule_line->faculty_id))
                <div class="col-md-6 col-lg-4 mt-4 mt-md-0">
                    <label for="subject_id" class="form-label">
                        <span wire:loading wire:target="schedule_line.subject_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Μάθημα
                    </label>
                    <select
                        wire:model="schedule_line.subject_id"
                        name="subject_id"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('subject_id')
                        ])
                    >
                        <option></option>
                        @foreach ($this->subjects as $subject)
                            <option value="{{ $subject->id }}">{{ $subject->title }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            {{-- teacher --}}
            @if (!empty($schedule_line->subject_id))
                <div class="col-md-6 col-lg-4 mt-4 mt-md-0">
                    <label for="subject_id" class="form-label">
                        <span wire:loading wire:target="schedule_line.teacher_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Καθηγητής
                    </label>
                    <select
                        wire:model="schedule_line.teacher_id"
                        name="teacher_id"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('teacher_id')
                        ])
                    >
                        <option></option>
                        @foreach ($this->teachers as $teacher)
                            <option value="{{ $teacher->id }}">{{ $teacher->official_name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
        </div>

        <div
            @class([
                'row',
                'mb-4' => !empty($schedule_line->teacher_id),
            ])
        >
            {{-- week day --}}
            @if (!empty($schedule_line->teacher_id))
                <div class="col-md-6 col-lg-4">
                    <label for="weekday" class="form-label">
                        <span wire:loading wire:target="schedule_line.weekday" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Μέρα
                    </label>
                    <select
                        wire:model="schedule_line.weekday"
                        name="weekday"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('weekday')
                        ])
                    >
                        <option></option>
                        @foreach ($this->weekdays as $day_num => $day_name)
                            <option value="{{ $day_num }}">{{ $day_name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            {{-- teaching hour --}}
            @if (!empty($schedule_line->weekday))
                <div class="col-md-6 col-lg-4 mt-4 mt-md-0">
                    <label for="teaching_hour_id" class="form-label">
                        <span wire:loading wire:target="schedule_line.teaching_hour_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Ώρα Διδασκαλίας
                    </label>
                    <select
                        wire:model="schedule_line.teaching_hour_id"
                        name="teaching_hour_id"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('teaching_hour_id')
                        ])
                    >
                        <option></option>
                        @foreach ($this->teaching_hours as $teaching_hour)
                            <option value="{{ $teaching_hour->id }}">{{ $teaching_hour->title }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            {{-- classroom --}}
            @if (!empty($schedule_line->teaching_hour_id))
                <div class="col-md-6 col-lg-4 mt-4 mt-md-0">
                    <label for="classroom_id" class="form-label">
                        <span wire:loading wire:target="schedule_line.classroom_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Αίθουσα
                    </label>
                    <select
                        wire:model="schedule_line.classroom_id"
                        name="classroom_id"
                        @class([
                            'form-select',
                            'is-invalid' => $errors->has('classroom_id')
                        ])
                    >
                        <option></option>
                        @foreach ($classrooms as $classroom)
                            <option value="{{ $classroom->id }}">{{ $classroom->title }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
        </div>
    </fieldset>

    @if ($academic_year_id)
        <div class="row">
            <div class="col-12 col-md-3">
                <button
                    wire:click="addLine"
                    type="button"
                    class="btn btn-success col-12"
                >
                    Προσθήκη Μαθήματος
                </button>
            </div>
        </div>
    @endif
</div>
