<div>
    <div class="row">
        <div class="col-12">
            <div class="card border-main">
                <div class="card-header text-white bg-simeio">
                    <a href="#" class="text-white {{ (count($filters)) ? 'collapsed' : '' }}" data-bs-toggle="collapse" data-bs-target=".filters_collapse" role="button" aria-expanded="{{ (request()->filled('filters')) ? 'true' : 'false' }}" aria-controls="filters_body filters_buttons">
                        <i class="fas fa-filter"></i> Φίλτρα
                    </a>
                </div>

                <div id="filters_body" class="card-body collapse {{ (count($filters)) ? 'show' : '' }} filters_collapse">
                    <div class="row">
                        {{-- name --}}
                        <div class="col-md-6">
                            <label for="name_filter" class="form-label">Όνομα</label>
                            <input
                                wire:model.debounce.500ms="filters.name"
                                id="name_filter"
                                name="filters[name]"
                                class="form-control"
                            >
                        </div>

                        {{-- email --}}
                        <div class="col-md-6">
                            <label for="email_filter" class="form-label mt-3 mt-md-0">Email</label>
                            <input
                                wire:model.debounce.500ms="filters.email"
                                id="email_filter"
                                name="filters[email]"
                                class="form-control"
                            >
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- role --}}
                        <div class="col-sm-6 col-md-4">
                            <label for="role_filter" class="form-label">Ρόλος</label>
                            <select
                                wire:model.debounce.500ms="filters.role"
                                id="role_filter"
                                name="filters[role]"
                                class="form-select"
                            >
                                <option></option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- tel --}}
                        <div class="col-sm-4 col-md-4">
                            <label for="phone_filter" class="form-label mt-3 mt-lg-0">Τηλέφωνο</label>
                            <input
                                wire:model.debounce.500ms="filters.phone"
                                id="phone_filter"
                                name="filters[phone]"
                                class="form-control"
                            >
                        </div>

                        {{-- can login --}}
                        <div class="col-sm-4 col-md-4">
                            <label for="login_filter" class="form-label mt-3 mt-lg-0">Ενεργός</label>
                            <x-boolean-select
                                wire:model="filters.login"
                                id="login_filter"
                                name="filters[login]"
                            ></x-boolean-select>
                        </div>
                    </div>
                </div>

                <div id="filters_buttons" class="card-footer collapse {{ (count($filters)) ? 'show' : '' }} filters_collapse">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <a href="{{ route('users.index') }}" class="btn btn-outline-secondary col-12">
                                <i class="fas fa-times"></i> Καθαρισμός
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        @can('create', \App\Models\User::class)
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 offset-md-4 offset-lg-9 offset-xl-10">
                <a href="{{ route('users.create') }}" class="btn btn-success col-12">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        @endcan
    </div>

    <div class="row mt-4">
        <div class="col-12 col-md-6">
            {{ $rows->appends(request()->except(['page']))->links() }}
        </div>

        <div class="col-12 col-md-6 text-center text-md-end">
            Εμφανίζονται {{ $rows->firstItem() }} έως {{ $rows->lastItem() }} από {{ $rows->total() }} καταχωρήσεις
        </div>
    </div>

    <div  class="row" style="height: 8px;">
        <div wire:loading class="col-12">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>

    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
        @foreach ($rows as $user)

            <div class="col">
                <div class="card h-100">
                    <div class="card-body">
                        <h5 class="card-title">
                            <i
                                title="@if (!$user->can_login) Μη Ενεργός @else Ενεργός @endif"
                                @class([
                                    'fas',
                                    'fa-sm',
                                    'fa-fw',
                                    'text-danger' => !$user->can_login,
                                    'text-success' => $user->can_login,
                                    'fa-times-circle' => !$user->can_login,
                                    'fa-check-circle' => $user->can_login,
                                ])
                            ></i>
                            {{ $user->official_name }}
                        </h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $user->email }}</h6>
                        <div class="card-text">
                            {{ $user->role->title }}
                            @if ($user->home_phone)
                                <br><x-icon-phone :type="'home'"></x-icon-phone> <x-phone :phone="$user->home_phone">{{ phone($user->home_phone)->formatForCountry('GR') }}</x-phone>
                            @endif
                            @if ($user->work_phone)
                                <br><x-icon-phone :type="'work'"></x-icon-phone> <x-phone :phone="$user->work_phone">{{ phone($user->work_phone)->formatForCountry('GR') }}</x-phone>
                            @endif
                            @if ($user->mobile)
                                <br><x-icon-phone :type="'mobile'"></x-icon-phone> <x-phone :phone="$user->mobile">{{ phone($user->mobile)->formatForCountry('GR') }}</x-phone>
                            @endif
                            <br><strong>Ακαδημαϊκά Έτη:</strong> {{ $user->academic_years->implode('title', ', ') }}
                            <br><strong>Μαθήματα:</strong> {{ $user->subjects->implode('title', ', ') }}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-grid gap-2 d-md-block d-md-flex justify-content-md-center">
                            @can('view', $user)
                                <a href="{{ route('users.show', ['user' => $user]) }}" class="btn btn-primary flex-md-fill"><i class="fas fa-search"></i></a>
                            @endcan

                            @can('update', $user)
                                <a href="{{ route('users.edit', ['user' => $user]) }}" class="btn btn-warning flex-md-fill"><i class="fas fa-pencil-alt"></i></a>
                            @endcan

                            @can('delete', $user)
                                <a href="javascript:;" class="btn btn-danger flex-md-fill" onclick="confirm('Είστε σίγουρος;') || event.stopImmediatePropagation()" wire:click="remove({{ $user->id }})">
                                    <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-trash" wire:target="remove({{ $user->id }})" class="fas fa-trash fa-fw"></i>
                                </a>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>
