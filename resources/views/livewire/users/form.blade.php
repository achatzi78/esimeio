<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->creates)
                Προσθήκη Χρήστη
            @else
                Επεξεργασία Χρήστη
            @endif
        </div>

        <div class="card-body">

            {{-- tabs list --}}
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item" wire:ignore>
                    <a href="#general" class="nav-link active" data-bs-toggle="tab">Γενικά Στοιχεία</a>
                </li>
                <li class="nav-item" wire:ignore>
                    <a href="#permissions" class="nav-link" data-bs-toggle="tab">Δικαιώματα</a>
                </li>
                <li class="nav-item" wire:ignore>
                    <a href="#academics" class="nav-link" data-bs-toggle="tab">Ακαδημαϊκά</a>
                </li>
            </ul>

            {{-- tabs content --}}
            <div class="tab-content">
                {{-- general --}}
                <div class="tab-pane fade show active" id="general" wire:ignore.self>
                    <div class="row mt-4">
                        {{-- name --}}
                        <div class="col-md-6 col-xl-3">
                            <label for="name" class="form-label">
                                <span wire:loading wire:target="name" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Όνομα
                            </label>
                            <input
                                wire:model.lazy="name"
                                id="name"
                                name="name"
                                type="text"
                                aria-describedby="validation_name_feedback"
                                @class([
                                    'form-control',
                                    'is-invalid' => $errors->has('name'),
                                ])
                            >
                            @error('name')
                            <div id="validation_name_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>

                        {{-- surname --}}
                        <div class="col-md-6 col-xl-3 mt-4 mt-xl-0">
                            <label for="surname" class="form-label">
                                <span wire:loading wire:target="surname" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Επώνυμο
                            </label>
                            <input
                                wire:model.lazy="surname"
                                id="surname"
                                name="surname"
                                type="text"
                                aria-describedby="validation_surname_feedback"
                                @class([
                                    'form-control',
                                    'is-invalid' => $errors->has('surname'),
                                ])
                            >
                            @error('surname')
                            <div id="validation_surname_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="row mt-4">
                        {{-- email --}}
                        <div class="col-md-6 col-xl-3">
                            <label for="email" class="form-label">
                                <span wire:loading wire:target="email" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Email
                            </label>
                            <input
                                wire:model.lazy="email"
                                id="email"
                                name="email"
                                type="email"
                                aria-describedby="validation_email_feedback"
                                @class([
                                    'form-control',
                                    'is-invalid' => $errors->has('email'),
                                ])
                            >
                            @error('email')
                            <div id="validation_email_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>

                        {{-- password --}}
                        <div class="col-md-6 col-xl-3 mt-4 mt-xl-0">
                            <label for="password" class="form-label">
                                <span wire:loading wire:target="password" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Κωδικός
                            </label>
                            <input
                                wire:model.lazy="password"
                                id="password"
                                name="password"
                                type="password"
                                aria-describedby="validation_password_feedback"
                                @class([
                                    'form-control',
                                    'is-invalid' => $errors->has('password'),
                                ])
                            >
                            @error('password')
                            <div id="validation_password_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>

                        {{-- password confirm --}}
                        <div class="col-md-6 col-xl-3 mt-4 mt-xl-0">
                            <label for="password_confirmation" class="form-label">
                                <span wire:loading wire:target="password_confirmation" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Επιβεβαίωση Κωδικού
                            </label>
                            <input
                                wire:model.lazy="password_confirmation"
                                id="password_confirmation"
                                name="password_confirmation"
                                type="password"
                                aria-describedby="validation_password_confirmation_feedback"
                                @class([
                                    'form-control',
                                    'is-invalid' => $errors->has('password_confirmation'),
                                ])
                            >
                            @error('password_confirmation')
                            <div id="validation_password_confirmation_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>
                    </div>

                    {{-- can login --}}
                    <div class="row mt-4">
                        <div class="col-md-4 col-lg-3">
                            <label class="form-label">
                                <span wire:loading wire:target="can_login" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Σύνδεση
                            </label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input
                                    wire:model="can_login"
                                    id="can_login_1"
                                    name="can_login"
                                    type="radio"
                                    value="1"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('can_login'),
                                    ])
                                >
                                <label class="form-check-label" for="can_login_1">Ναι</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    wire:model="can_login"
                                    id="can_login_0"
                                    name="can_login"
                                    type="radio"
                                    value="0"
                                    @class([
                                        'form-check-input',
                                        'is-invalid' => $errors->has('can_login'),
                                    ])
                                >
                                <label class="form-check-label" for="can_login_0">Όχι</label>
                            </div>
                            @error('can_login')
                            <div class="form-text text-danger">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>
                    </div>

                    @if (!empty($this->role_id) && !$this->is_developer && !$this->is_admin)
                        {{-- phone numbers --}}
                        <div class="row mt-4">
                            <label class="form-label">Τηλέφωνα <small class="text-muted">(Τα τηλέφωνα πρέπει να είναι της μορφής +30xxxxxxxxxx)</small></label>
                            {{-- phone home --}}
                            <div class="col-md-4">
                                <div class="input-group mb-3">
                            <span class="input-group-text" id="data_phones_home_addon" title="Οικίας">
                                <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-home" wire:target="additional_data.phones.home" class="fas fa-home fa-fw"></i>
                            </span>
                                    <input
                                        wire:model.lazy="additional_data.phones.home"
                                        id="data_phones_home"
                                        name="additional_data[phones][home]"
                                        type="tel"
                                        aria-describedby="data_phones_home_addon"
                                        @class([
                                            'form-control',
                                            'is-invalid' => $errors->has('additional_data.phones.home'),
                                        ])
                                    >
                                    @error('additional_data.phones.home')
                                    <div class="invalid-feedback">
                                        {!! $message !!}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            {{-- phone work --}}
                            <div class="col-md-4">
                                <div class="input-group mb-3">
                            <span class="input-group-text" id="data_phones_work_addon" title="Εργασίας">
                                <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-briefcase" wire:target="additional_data.phones.work" class="fas fa-briefcase fa-fw"></i>
                            </span>
                                    <input
                                        wire:model.lazy="additional_data.phones.work"
                                        id="data_phones_work"
                                        name="additional_data[phones][work]"
                                        type="tel"
                                        aria-describedby="data_phones_work_addon"
                                        @class([
                                            'form-control',
                                            'is-invalid' => $errors->has('additional_data.phones.work'),
                                        ])
                                    >
                                    @error('additional_data.phones.work')
                                    <div class="invalid-feedback">
                                        {!! $message !!}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            {{-- phone mobile --}}
                            <div class="col-md-4">
                                <div class="input-group mb-3">
                            <span class="input-group-text" id="data_phones_mobile_addon" title="Κινητό">
                                <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-mobile-alt" wire:target="additional_data.phones.mobile" class="fas fa-mobile-alt fa-fw"></i>
                            </span>
                                    <input
                                        wire:model.lazy="additional_data.phones.mobile"
                                        id="data_phones_mobile"
                                        name="additional_data[phones][mobile]"
                                        type="tel"
                                        aria-describedby="data_phones_mobile_addon"
                                        @class([
                                            'form-control',
                                            'is-invalid' => $errors->has('additional_data.phones.mobile'),
                                        ])
                                    >
                                    @error('additional_data.phones.mobile')
                                    <div class="invalid-feedback">
                                        {!! $message !!}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                {{-- permissions --}}
                <div class="tab-pane fade" id="permissions" wire:ignore.self>
                    {{-- role --}}
                    <div class="row mt-4">
                        <div class="col-md-4 col-lg-3">
                            <label class="form-label">
                                <span wire:loading wire:target="role_id" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Ρόλος
                            </label>
                            <select
                                wire:model="role_id"
                                id="role_id"
                                name="role_id"
                                aria-describedby="validation_role_id_feedback"
                                @class([
                                    'form-select',
                                    'is-invalid' => $errors->has('role_id'),
                                ])
                            >
                                <option></option>
                                @foreach($this->roles_list as $role)
                                    <option value="{{ $role->id }}">{{ $role->title }}</option>
                                @endforeach
                            </select>
                            @error('role_id')
                            <div id="validation_role_id_feedback" class="invalid-feedback">
                                {!! $message !!}
                            </div>
                            @enderror
                        </div>
                    </div>

                    {{-- permissions --}}
                    @if ($this->role_id)
                        <div class="row mt-4">
                            <div class="col-12">
                                <label class="form-label">
                                    <span wire:loading wire:target="permissions" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Δικαιώματα
                                </label>

                                @error('permissions')
                                <div class="form-text text-danger">
                                    {!! $message !!}
                                </div>
                                @enderror

                                <div class="row">
                                    @foreach($this->permissions_list->split(3) as $permissions_group)
                                        <div class="col-md-4">
                                            @foreach($permissions_group as $permission)
                                                <div class="form-check">
                                                    <input
                                                        wire:model="permissions"
                                                        wire:loading.attr="disabled"
                                                        id="permission_{{ $permission->id }}"
                                                        name="permissions[]"
                                                        type="checkbox"
                                                        value="{{ $permission->id }}"
                                                        @class([
                                                            'form-check-input',
                                                            'is-invalid' => $errors->has('permissions')
                                                        ])
                                                    >
                                                    <label class="form-check-label" for="permission_{{ $permission->id }}">
                                                        {{ $permission->title }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                {{-- academics --}}
                <div class="tab-pane fade" id="academics" wire:ignore.self>
                    @if (!empty($this->role_id) && !$this->is_developer && !$this->is_admin)
                        {{-- availability --}}
                        <div class="row mt-4">

                            <h3>Διαθεσιμότητα</h3>

                            @if ($this->is_teacher)
                                <div class="col-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                            @foreach ($this->academic_years_list as $academic_year)
                                                <tr>
                                                    <th colspan="7">{{ $academic_year->title }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" style="width: 16%">
                                                        <div wire:loading wire:loading.target="availabilities" class="spinner-border spinner-border-sm" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div>
                                                    </th>
                                                    @foreach (weekdays() as $day_num => $day_name)
                                                        <th class="text-center" style="width: 14%">{{ $day_name }}</th>
                                                    @endforeach
                                                </tr>
                                                @foreach ($academic_year->teaching_hours->sortBy('start_time') as $teaching_hour)
                                                    <tr>
                                                        <th>{{ $teaching_hour->title }}</th>
                                                        @foreach(data_get($this->availabilities_list, "{$academic_year->id}.{$teaching_hour->id}") as $availability)
                                                            <td class="text-center">
                                                                <input
                                                                    wire:model="availabilities"
                                                                    wire:loading.attr="disabled"
                                                                    id="availabilities_{{ $availability->id }}"
                                                                    name="availabilities[]"
                                                                    type="checkbox"
                                                                    class="form-check-input"
                                                                    value="{{ $availability->id }}"
                                                                >
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="col-md-6 col-lg-3">
                                    <label class="form-label">
                                        <span wire:loading wire:target="academic_years" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Ακαδημαϊκά Έτη
                                    </label>

                                    @error('academic_years')
                                        <div class="form-text text-danger">
                                            {!! $message !!}
                                        </div>
                                    @enderror

                                    @foreach ($this->academic_years_list as $academic_year)
                                        <div class="form-check">
                                            <input
                                                wire:model="academic_years"
                                                wire:loading.attr="disabled"
                                                id="academic_year_{{ $academic_year->id }}"
                                                name="academic_years[]"
                                                type="checkbox"
                                                value="{{ $academic_year->id }}"
                                                @class([
                                                    'form-check-input',
                                                    'is-invalid' => $errors->has('academic_years')
                                                ])
                                            >
                                            <label for="academic_year_{{ $academic_year->id }}" class="form-check-label">
                                                {{ $academic_year->title }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                        </div>

                        @if ($this->is_teacher)
                            {{-- subjects --}}
                            <div class="row mt-4">
                                <label class="form-label">
                                    <span wire:loading wire:target="subjects" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Μαθήματα
                                </label>

                                @error('subjects')
                                <div class="form-text text-danger">
                                    {!! $message !!}
                                </div>
                                @enderror

                                @foreach ($this->subjects_list->split(3) as $subject_group)
                                    <div class="col-md-4">
                                        @foreach ($subject_group as $subject)
                                            <div class="form-check">
                                                <input
                                                    wire:model="subjects"
                                                    wire:loading.attr="disabled"
                                                    id="subject_{{ $subject->id }}"
                                                    name="subjects[]"
                                                    type="checkbox"
                                                    value="{{ $subject->id }}"
                                                    @class([
                                                        'form-check-input',
                                                        'is-invalid' => $errors->has('subjects')
                                                    ])
                                                >
                                                <label class="form-check-label" for="subject_{{ $subject->id }}">
                                                    {{ $subject->title }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @endif
                </div>
            </div>

        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
