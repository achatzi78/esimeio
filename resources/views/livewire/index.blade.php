<div>
    {{-- filters --}}
    <div class="row">
        <div class="col-12">

            <div class="card border-simeio">
                <div class="card-header text-white bg-simeio">
                    <a wire:ignore.self href="#filters_card_body" class="text-white" data-bs-toggle="collapse" aria-expanded="true">
                        <i class="fas fa-filter"></i> Φίλτρα
                    </a>
                </div>

                <div wire:ignore.self id="filters_card_body" class="collapse show">
                    <div class="card-body">
                        <fieldset wire:loading.attr="disabled">
                            @yield('filters')

                            <hr>

                            <div class="row row-cols-sm-2 row-cols-xxl-6">
                                <div class="col">
                                    <button wire:click="clearFilters()" type="button" class="btn btn-outline-secondary w-100"><i class="fas fa-times me-1"></i> Εκκαθάριση</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{-- buttons --}}
    @yield('buttons')

    {{-- table --}}
    <div class="row">
        <div class="col-12">

            {{-- top pagination --}}
            <div class="mb-3">
                <x-pagination :rows="$rows"></x-pagination>
            </div>

            {{-- loader --}}
            <div class="row" style="height: 4px;">
                <div wire:loading class="col-12">
                    <div class="progress-bar-loader bg-red-100">
                        <div class="progress-bar-value bg-danger"></div>
                    </div>
                </div>
            </div>

            @yield('table')

            {{-- loader --}}
            <div class="row" style="height: 4px;">
                <div wire:loading class="col-12">
                    <div class="progress-bar-loader bg-red-100">
                        <div class="progress-bar-value bg-danger"></div>
                    </div>
                </div>
            </div>

            {{-- bottom pagination --}}
            <div class="mt-3">
                <x-pagination :rows="$rows"></x-pagination>
            </div>

        </div>
    </div>
</div>
