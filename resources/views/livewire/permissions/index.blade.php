@extends('livewire.index')

@section('filters')
    <div class="row mb-4">
        {{-- title --}}
        <div class="col-md-6 col-lg-3">
            <x-livewire.filter-title></x-livewire.filter-title>
        </div>

        {{-- category --}}
        <div class="col-md-6 col-lg-3 mt-3 mt-md-0">
            <label for="category_filter" class="form-label">Κατηγορία</label>
            <select
                wire:model.lazy="filters.category"
                id="category_filter"
                name="filters[category]"
                class="form-select"
            >
                <option></option>
                @foreach($this->categories as $category)
                    <option value="{{ $category }}">{{ Str::headline($category) }}</option>
                @endforeach
            </select>
        </div>

        {{-- description --}}
        <div class="col-md-6 col-lg-3 mt-3 mt-lg-0">
            <label for="description_filter" class="form-label">Περιγραφή</label>
            <input
                wire:model.lazy="filters.description"
                id="description_filter"
                name="filters[description]"
                type="text"
                class="form-control"
            >
        </div>

        {{-- action --}}
        <div class="col-md-6 col-lg-3 mt-3 mt-lg-0">
            <label for="action_filter" class="form-label">Ενέργεια</label>
            <input
                wire:model.lazy="filters.action"
                id="action_filter"
                name="filters[action]"
                type="text"
                class="form-control"
            >
        </div>
    </div>
@endsection

@section('buttons')
    @can('create', \App\Models\Permission::class)
        <div class="row mt-4 mb-4 justify-content-center justify-content-lg-end">
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <a href="{{ route('permissions.create') }}" class="btn btn-success col-12 text-capitalize">
                    <i class="fas fa-plus"></i> Προσθήκη
                </a>
            </div>
        </div>

        <hr>
    @endcan
@endsection

@section('table')
    <div class="table-responsive-lg">
        <table class="table table-bordered align-middle m-0">
            <thead class="table-light">
                <tr>
                    <th class="text-center" style="width: 10%"></th>
                    <x-livewire.table-head sort-by="category" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        {{ __('category') }}
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="title" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        {{ __('title') }}
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="description" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        {{ __('description') }}
                    </x-livewire.table-head>
                    <x-livewire.table-head sort-by="action" current-sort="{{ $sort_by }}" current-direction="{{ $sort_direction }}">
                        {{ __('action') }}
                    </x-livewire.table-head>
                </tr>
            </thead>
            <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td>
                            <x-livewire.buttons-crud :model="$row" :prefix="'permissions'" :parameter="'permission'"></x-livewire.buttons-crud>
                        </td>
                        <td>{{ $row->category }}</td>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->description }}</td>
                        <td>{{ $row->action }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
