<form wire:submit.prevent="save">

    <div class="card">
        <div class="card-header">
            @if ($this->is_new)
                Προσθήκη Δικαιώματος
            @else
                Επεξεργασία Δικαιώματος
            @endif
        </div>

        <div class="card-body">
            <fieldset wire:loading.attr="disabled">
                <div class="row">
                    <div class="col-md-4">
                        <label for="title" class="form-label required">
                            <span wire:loading wire:target="permission.title" class="spinner-border spinner-border-sm"></span> Τίτλος
                        </label>
                        <input
                            wire:model.lazy="permission.title"
                            name="title"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('permission.title')
                            ])
                        >

                        @error('permission.title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="col-md-4 mt-4 mt-md-0">
                        <label for="category" class="form-label required">
                            <span wire:loading wire:target="permission.category" class="spinner-border spinner-border-sm"></span> Κατηγορία
                        </label>
                        <input
                            wire:model.lazy="permission.category"
                            name="category"
                            type="text"
                            list="permission-categories"
                            autocomplete="off"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('permission.title')
                            ])
                        >
                        @error('permission.category')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror

                        <datalist id="permission-categories">
                            @foreach($this->categories as $category)
                                <option value="{{ $category }}">
                            @endforeach
                        </datalist>
                    </div>

                    <div class="col-md-4 mt-4 mt-md-0">
                        <label for="action" class="form-label required">
                            <span wire:loading wire:target="permission.action" class="spinner-border spinner-border-sm"></span> Ενέργεια
                        </label>
                        <input
                            wire:model.lazy="permission.action"
                            name="action"
                            type="text"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('permission.action')
                            ])
                        >
                        @error('permission.action')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-12">
                        <label for="description" class="form-label">
                            <span wire:loading wire:target="permission.description" class="spinner-border spinner-border-sm"></span> Περιγραφή
                        </label>
                        <textarea
                            wire:model.lazy="permission.description"
                            name="description"
                            rows="5"
                            @class([
                                'form-control',
                                'is-invalid' => $errors->has('permission.description')
                            ])
                        ></textarea>
                        @error('permission.description')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="card-footer">
            <div class="row row-cols-sm-6 row-cols-md-3 row-cols-lg-4">
                <div class="col-12">
                    <button wire:loading.attr="disabled" wire:target="save" type="submit" class="btn btn-primary w-100">
                        <i wire:loading.class="fa-circle-notch fa-spin" wire:loading.class.remove="fa-check" wire:target="save" class="fas fa-check me-1"></i> Καταχώρηση
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
