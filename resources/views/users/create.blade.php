@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <x-alert-info :dismiss="false">
                <h4 class="alert-heading">Οδηγίες Κωδικού Πρόσβασης</h4>

                <p>Ο κωδικός πρόσβασης πρέπει να αποτελείται από τουλάχιστον 6 χαρακτήρες και πρέπει να περιέχει τουλάχιστον έναν χαρακτήρα από τις ακόλουθες κατηγορίες:</p>

                <ul>
                    <li>Λατινικοί κεφαλαίοι χαρακτήρες (A – Z)</li>
                    <li>Λατινικοί πεζοί χαρακτήρες (a – z)</li>
                    <li>Βασικοί αριθμοί (0 – 9)</li>
                    <li>Μη αλφαριθμητικό (πχ: !, $, #, ή %)</li>
                </ul>
            </x-alert-info>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12">
            <livewire:users.form />
        </div>
    </div>
@endsection
