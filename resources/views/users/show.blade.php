@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @yield('form-title')
                </div>
                <div class="card-body">
                    <div class="row">
                        {{-- name --}}
                        <div class="col-md-6 col-xl-3">
                            <dl>
                                <dt>Όνομα</dt>
                                <dd>{{ $user->official_name }}</dd>
                            </dl>
                        </div>

                        {{-- email --}}
                        <div class="col-md-6 col-xl-3">
                            <dl>
                                <dt>Email</dt>
                                <dd>{{ $user->email }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        {{-- active --}}
                        <div class="col-md-4 col-lg-3">
                            <dl>
                                <dt>Σύνδεση</dt>
                                <dd>
                                    <h5>
                                        <span class="badge bg-{{ ($user->can_login) ? 'success' : 'danger' }}"><i class="fas fa-{{ ($user->can_login) ? 'check' : 'times' }}-circle"></i> {{ ($user->can_login) ? 'Ναι' : 'Όχι' }}</span>
                                    </h5>
                                </dd>
                            </dl>
                        </div>

                        {{-- role --}}
                        <div class="col-md-4 col-lg-3">
                            <dl>
                                <dt>Ρόλος</dt>
                                <dd>{{ $user->role->title }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <dl>
                            <dt>Τηλέφωνα</dt>
                            <dd>
                                <ul class="list-unstyled">
                                    @if ($user->home_phone)
                                        <li>
                                            <x-icon-phone :type="'home'"></x-icon-phone> <x-phone :phone="$user->home_phone">{{ phone($user->home_phone)->formatForCountry('GR') }}</x-phone>
                                        </li>
                                    @endif

                                    @if ($user->work_phone)
                                        <li>
                                            <x-icon-phone :type="'work'"></x-icon-phone> <x-phone :phone="$user->work_phone">{{ phone($user->work_phone)->formatForCountry('GR') }}</x-phone>
                                        </li>
                                    @endif

                                    @if ($user->mobile)
                                        <li>
                                            <x-icon-phone :type="'mobile'"></x-icon-phone> <x-phone :phone="$user->mobile">{{ phone($user->mobile)->formatForCountry('GR') }}</x-phone>
                                        </li>
                                    @endif
                                </ul>
                            </dd>
                        </dl>
                    </div>

                    @yield('additional-inputs')
                </div>
                <div class="card-footer">
                    <div class="row">
                        @can('update', $user)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="@yield('edit-route')" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endcan

                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <a href="@yield('back-route')" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
