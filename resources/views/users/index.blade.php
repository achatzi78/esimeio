@extends('layouts.app')

@section('meta-title', 'Χρήστες')

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <livewire:users.index />
        </div>
    </div>
@endsection
