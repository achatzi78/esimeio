@extends('layouts.app')

@section('meta-title', 'Αίθουσες')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:classrooms.index />
        </div>
    </div>
@endsection
