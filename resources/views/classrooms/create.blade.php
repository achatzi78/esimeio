@extends('layouts.app')

@section('meta-title', 'Αίθουσες::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:classrooms.form />
        </div>
    </div>
@endsection
