@extends('layouts.app')

@section('meta-title', 'Αίθουσες::Επεξεργασία')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:classrooms.form :classroom="$classroom" />
        </div>
    </div>
@endsection
