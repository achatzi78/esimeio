@extends('layouts.app')

@section('meta-title', 'Αίθουσες::Προβολή')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Προβολή Αίθουσας
                </div>

                <div class="card-body">
                    <div class="row">
                        {{-- title --}}
                        <div class="col-md-6">
                            <dl>
                                <dt>Τίτλος</dt>
                                <dd>{{ $classroom->title }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        @can ('update', $classroom)
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <a href="{{ route('classrooms.edit', ['classroom' => $classroom]) }}" class="btn btn-primary col-12"><i class="fas fa-pencil-alt"></i> Επεξεργασία</a>
                            </div>
                        @endif

                        <div class="col-sm-6 col-md-4 col-lg-3 mt-3 mt-sm-0">
                            <a href="{{ route('classrooms.index') }}" class="btn btn-outline-secondary col-12"><i class="fas fa-times"></i> Ακύρωση</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
