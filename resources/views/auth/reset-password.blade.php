@extends('layouts.auth')

@section('meta-title', 'Επαναφορά Κωδικού Πρόσβασης')

@section('page-title', 'Επαναφορά Κωδικού Πρόσβασης')

@section('form')
    <livewire:auth.reset-password-form :token="request()->route('token')" :email="request('email')" />
@endsection
