@extends('layouts.auth')

@section('meta-title', 'Σύνδεση')

@section('page-title', 'Σύνδεση')

@section('form')
    <livewire:auth.login-form />
@endsection



