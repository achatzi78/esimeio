@extends('layouts.auth')

@section('meta-title', 'Αίτηση Επαναφοράς Κωδικού Πρόσβασης')

@section('page-title', 'Αίτηση Επαναφοράς Κωδικού Πρόσβασης')

@section('form')
    <livewire:auth.forgot-password-form />
@endsection
