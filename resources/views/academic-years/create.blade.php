@extends('layouts.app')

@section('meta-title', 'Ακαδημαϊκά Έτη::Προσθήκη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:academic-years.form />
        </div>
    </div>
@endsection
