@extends('layouts.app')

@section('meta-title', 'Ακαδημαϊκά Έτη::Επεξεργασία')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:academic-years.form :academic_year="$academic_year" />
        </div>
    </div>
@endsection
