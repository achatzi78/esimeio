@extends('layouts.app')

@section('meta-title', 'Ακαδημαϊκά Έτη')

@section('content')
    <div class="row">
        <div class="col-12">
            <livewire:academic-years.index />
        </div>
    </div>
@endsection
