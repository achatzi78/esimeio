@livewireScripts

{{-- icons --}}
<script src="https://kit.fontawesome.com/13a9b34187.js" crossorigin="anonymous"></script>

{{-- app js --}}
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/vendor/blockui/blockui.js') }}"></script>

{{-- toastr --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" crossorigin="anonymous"></script>

@stack('page-js')
