@if ($errors->any())
    <div class="row">
        <div class="@if ($full_width ?? false) col-12 @else col-xl-8 offset-xl-2 @endif">

            <x-alert-error>
                @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                @endforeach
            </x-alert-error>

        </div>
    </div>
@endif
