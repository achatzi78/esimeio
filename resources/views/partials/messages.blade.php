@if ($messages = session('messages'))
    <div class="row">
        <div class="@if ($full_width ?? false) col-12 @else col-xl-6 offset-xl-3 @endif">

            @foreach($messages as $type => $message)
                <x-alert :type="$type">
                    @if (is_array($message))
                        @foreach($message as $msg)
                            {!! $msg !!}<br>
                        @endforeach
                    @else
                        {!! $message !!}
                    @endif
                </x-alert>
            @endforeach

        </div>
    </div>
@endif
