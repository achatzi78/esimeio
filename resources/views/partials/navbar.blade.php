<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top shadow-sm fh-fixedHeader">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('storage/images/logo_black.png') }}" class="img-fluid" alt="{{ __(config('app.name', 'LMS')) }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#top_menu" aria-controls="top_menu" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div id="top_menu" class="collapse navbar-collapse">
            {{-- left menu --}}
            {!! Menu::navbar()->render() !!}

            {{-- right menu --}}
            <ul class="navbar-nav">

                {{-- academic year --}}
                @if ($navbar_academic_years->count() > 0)
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle"  role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="text-danger">{{ $navbar_current_academic_year->title }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="auth_dropdown">
                            @foreach ($navbar_academic_years as $navbar_academic_year)
                                <li>
                                    <a class="dropdown-item" href="{{ route('academic-years.set-current', ['academic_year' => $navbar_academic_year]) }}" onclick="event.preventDefault(); document.getElementById('frm_academic_year').submit();">
                                        {{ $navbar_academic_year->title }}
                                    </a>

                                    <form id="frm_academic_year" action="{{ route('academic-years.set-current', ['academic_year' => $navbar_academic_year]) }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @else
                    <span class="navbar-text">
                        Ακαδημαϊκό Έτος: <span class="text-danger">{{ $navbar_current_academic_year->title }}</span> &nbsp;|
                    </span>
                @endif

                {{-- auth dropdown --}}
                <li class="nav-item dropdown">
                    <a href="#" id="auth_dropdown" class="nav-link dropdown-toggle"  role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ auth()->user()->fullname }}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="auth_dropdown">

                        <li><hr class="dropdown-divider"></li>

                        @if (session()->get('fake_user'))
                            <li>
                                <a class="dropdown-item" href="{{ url()->current() }}?fake_logout">
                                    <i class="fas fa-lock-open"></i> {{ __('Fake Logout') }}
                                </a>
                            </li>
                        @else
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm_logout').submit();">
                                    <i class="fas fa-lock-open"></i> {{ __('Logout') }}
                                </a>

                                <form id="frm_logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
