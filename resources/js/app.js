require('./bootstrap');

Alpine.start();

document.addEventListener('DOMContentLoaded', () => {
    //enable tooltips
    [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]')).map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
    });

    //enable popovers
    [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]')).map(function (popoverTriggerEl) {
        let options = {};//content
        let sibling = popoverTriggerEl.nextElementSibling;

        while (sibling) {
            if (sibling.classList.contains('popover-content')) {
                options.html = true;
                options.content = sibling;
                break;
            }

            sibling = sibling.nextElementSibling;
        }

        return new bootstrap.Popover(popoverTriggerEl, options);
    });

    Livewire.hook('message.sent', (message, component) => {
        //dispose tooltips
        [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]')).map(function (tooltipTriggerEl) {
            let tooltip = bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl);

            tooltip.dispose();
        });

        //dispose popovers
        [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]')).map(function (popoverTriggerEl) {
            let popover = bootstrap.Popover.getOrCreateInstance(popoverTriggerEl);

            popover.dispose();
        });
    });

    Livewire.hook('message.processed', (message, component) => {
        //enable tooltips
        [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]')).map(function (tooltipTriggerEl) {
            //return new bootstrap.Tooltip(tooltipTriggerEl);
            return bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl);
        });

        //enable popovers
        [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]')).map(function (popoverTriggerEl) {
            let options = {};
            let sibling = popoverTriggerEl.nextElementSibling;

            while (sibling) {
                if (sibling.classList.contains('popover-content')) {
                    options.html = true;
                    options.content = sibling;
                    break;
                }

                sibling = sibling.nextElementSibling;
            }

            return new bootstrap.Popover(popoverTriggerEl, options);
        });
    });
});
