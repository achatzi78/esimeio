<?php

use App\Http\Controllers\AcademicYearController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\DeveloperController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\TeachingHourController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware(['auth'])
    ->group(function () {
        /* HOME */
        Route::get('/', [HomeController::class, 'index'])->name('home');

        /* ROLES */
        Route::resource('roles', RoleController::class)->only('index', 'edit');

        /* PERMISSIONS */
        Route::resource('permissions', PermissionController::class)->only('index', 'show', 'create', 'edit');

        /* ACADEMIC YEARS */
        Route::post('academic-years/{academic_year}/set-current', [AcademicYearController::class, 'change'])->name('academic-years.set-current');
        Route::resource('academic-years', AcademicYearController::class)->only('index', 'show', 'create', 'edit');

        /* CLASSROOMS */
        Route::resource('classrooms', ClassroomController::class)->only('index', 'show', 'create', 'edit');

        /* FACULTIES */
        Route::resource('faculties', FacultyController::class)->only('index', 'show', 'create', 'edit');

        /* TEACHING HOURS */
        Route::resource('teaching-hours', TeachingHourController::class)->only('index', 'show', 'create', 'edit');

        /* SUBJECTS */
        Route::resource('subjects', SubjectController::class)->only('index', 'show', 'create', 'edit');

        /* USERS */
        Route::resource('users', UserController::class)->only('index', 'show', 'create', 'edit');

        /* SCHEDULES */
        Route::get('/schedules', [ScheduleController::class, 'index'])->name('schedules.index');
        Route::post('/schedules', [ScheduleController::class, 'store'])->name('schedules.store');
        Route::get('/schedules/create', \App\Http\Livewire\Schedules\Create::class)->name('schedules.create');
        Route::get('/schedules/{schedule}', [ScheduleController::class, 'show'])->name('schedules.show');
        Route::get('/schedules/{schedule}/edit', [ScheduleController::class, 'edit'])->name('schedules.edit');
        Route::patch('/schedules/{schedule}/edit', [ScheduleController::class, 'update'])->name('schedules.update');
    });


