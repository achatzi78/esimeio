<?php

// Home > Users > Subjects
Breadcrumbs::for('subjects.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Μαθήματα', route('subjects.index'));
});


// Home > Users > Subjects > Create
Breadcrumbs::for('subjects.create', function ($trail) {
    $trail->parent('subjects.index');
    $trail->push('Προσθήκη', route('subjects.create'));
});

// Home > Users > Subjects > View
Breadcrumbs::for('subjects.show', function ($trail, $subject) {
    $trail->parent('subjects.index');
    $trail->push($subject->title, route('subjects.show', ['subject' => $subject]));
});

// Home > Users > Subjects > Edit
Breadcrumbs::for('subjects.edit', function ($trail, $subject) {
    $trail->parent('subjects.show', $subject);
    $trail->push('Επεξεργασία', route('subjects.edit', ['subject' => $subject]));
});
