<?php

// Home > Classrooms
Breadcrumbs::for('classrooms.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Αίθουσες', route('classrooms.index'));
});

// Home > Classrooms > Create
Breadcrumbs::for('classrooms.create', function ($trail) {
    $trail->parent('classrooms.index');
    $trail->push('Προσθήκη', route('classrooms.create'));
});

// Home > Classrooms > View
Breadcrumbs::for('classrooms.show', function ($trail, $classroom) {
    $trail->parent('classrooms.index');
    $trail->push($classroom->title, route('classrooms.show', ['classroom' => $classroom]));
});

// Home > Classrooms > Edit
Breadcrumbs::for('classrooms.edit', function ($trail, $classroom) {
    $trail->parent('classrooms.show', $classroom);
    $trail->push('Επεξεργασία', route('classrooms.edit', ['classroom' => $classroom]));
});
