<?php

// Home > Teaching Hours
Breadcrumbs::for('teaching-hours.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Διδακτικές Ώρες', route('teaching-hours.index'));
});

// Home > Teaching Hours > Create
Breadcrumbs::for('teaching-hours.create', function ($trail) {
    $trail->parent('teaching-hours.index');
    $trail->push('Προσθήκη', route('teaching-hours.create'));
});

// Home > Teaching Hours > View
Breadcrumbs::for('teaching-hours.show', function ($trail, $teaching_hour) {
    $trail->parent('teaching-hours.index');
    $trail->push($teaching_hour->title, route('teaching-hours.show', ['teaching_hour' => $teaching_hour]));
});

// Home > Teaching Hours > Edit
Breadcrumbs::for('teaching-hours.edit', function ($trail, $teaching_hour) {
    $trail->parent('teaching-hours.show', $teaching_hour);
    $trail->push('Επεξεργασία', route('teaching-hours.edit', ['teaching_hour' => $teaching_hour]));
});
