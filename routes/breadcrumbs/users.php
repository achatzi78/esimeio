<?php

// Home > Users
Breadcrumbs::for('users.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Χρήστες', route('users.index'));
});


// Home > Users > Create
Breadcrumbs::for('users.create', function ($trail) {
    $trail->parent('users.index');
    $trail->push('Προσθήκη', route('users.create'));
});

// Home > Users > View
Breadcrumbs::for('users.show', function ($trail, $user) {
    $trail->parent('users.index');
    $trail->push($user->fullname, route('users.show', ['user' => $user]));
});

// Home > Users > Edit
Breadcrumbs::for('users.edit', function ($trail, $user) {
    $trail->parent('users.show', $user);
    $trail->push('Επεξεργασία', route('users.edit', ['user' => $user]));
});
