<?php

// Home > Academic Years
Breadcrumbs::for('academic-years.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Ακαδημαϊκά Έτη', route('academic-years.index'));
});

// Home > Academic Years > Create
Breadcrumbs::for('academic-years.create', function ($trail) {
    $trail->parent('academic-years.index');
    $trail->push('Προσθήκη', route('academic-years.create'));
});

// Home > Academic Years > View
Breadcrumbs::for('academic-years.show', function ($trail, $academic_year) {
    $trail->parent('academic-years.index');
    $trail->push($academic_year->title, route('academic-years.show', ['academic_year' => $academic_year]));
});

// Home > Academic Years > Edit
Breadcrumbs::for('academic-years.edit', function ($trail, $academic_year) {
    $trail->parent('academic-years.show', $academic_year);
    $trail->push('Επεξεργασία', route('academic-years.edit', ['academic_year' => $academic_year]));
});
