<?php

// Home > Permissions
Breadcrumbs::for('permissions.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Δικαιώματα', route('permissions.index'));
});

// Home > Permissions > Create
Breadcrumbs::for('permissions.create', function ($trail) {
    $trail->parent('permissions.index');
    $trail->push('Προσθήκη Δικαιώματος', route('permissions.create'));
});

// Home > Permissions > View
Breadcrumbs::for('permissions.show', function ($trail, $permission) {
    $trail->parent('permissions.index');
    $trail->push($permission->title, route('permissions.show', ['permission' => $permission]));
});

// Home > Permissions > Edit
Breadcrumbs::for('permissions.edit', function ($trail, $permission) {
    $trail->parent('permissions.show', $permission);
    $trail->push('Επεξεργασία', route('permissions.edit', ['permission' => $permission]));
});
