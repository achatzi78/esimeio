<?php

// Home > Schedules
Breadcrumbs::for('schedules.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Προγράμματα', route('schedules.index'));
});


// Home > Schedules > Create
Breadcrumbs::for('schedules.create', function ($trail) {
    $trail->parent('schedules.index');
    $trail->push('Προσθήκη', route('schedules.create'));
});

// Home > Schedules > View
Breadcrumbs::for('schedules.show', function ($trail, $schedule) {
    $trail->parent('schedules.index');
    $trail->push($schedule->title, route('schedules.show', ['admin' => $schedule]));
});

// Home > Schedules > Edit
Breadcrumbs::for('schedules.edit', function ($trail, $schedule) {
    $trail->parent('schedules.show', $schedule);
    $trail->push('Επεξεργασία', route('schedules.edit', ['admin' => $schedule]));
});
