<?php

// Home > Faculties
Breadcrumbs::for('faculties.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Τμήματα', route('faculties.index'));
});

// Home > Faculties > Create
Breadcrumbs::for('faculties.create', function ($trail) {
    $trail->parent('faculties.index');
    $trail->push('Προσθήκη', route('faculties.create'));
});

// Home > Faculties > View
Breadcrumbs::for('faculties.show', function ($trail, $faculty) {
    $trail->parent('faculties.index');
    $trail->push($faculty->title, route('faculties.show', ['faculty' => $faculty]));
});

// Home > Faculties > Edit
Breadcrumbs::for('faculties.edit', function ($trail, $faculty) {
    $trail->parent('faculties.show', $faculty);
    $trail->push('Επεξεργασία', route('faculties.edit', ['faculty' => $faculty]));
});
