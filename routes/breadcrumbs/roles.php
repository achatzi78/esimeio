<?php

// Home > Roles
Breadcrumbs::for('roles.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Ρόλοι', route('roles.index'));
});

// Home > Roles > Edit
Breadcrumbs::for('roles.edit', function ($trail, $role) {
    $trail->parent('roles.index');
    $trail->push($role->title, route('roles.edit', ['role' => $role]));
});
