<?php

// Note: Laravel will automatically resolve `Breadcrumbs::` without
// this import. This is nice for IDE syntax and refactoring.
use Diglactic\Breadcrumbs\Breadcrumbs;

// This import is also not required, and you could replace `BreadcrumbTrail $trail`
//  with `$trail`. This is nice for IDE type checking and completion.
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

// Home
Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Αρχική', route('home'));
});

// Users
include_once 'breadcrumbs/users.php';

// Roles
include_once 'breadcrumbs/roles.php';

// Permissions
include_once 'breadcrumbs/permissions.php';

// Academic Years
include_once 'breadcrumbs/academic-years.php';

// Classrooms
include_once 'breadcrumbs/classrooms.php';

// Faculties
include_once 'breadcrumbs/faculties.php';

// Teaching Hours
include_once 'breadcrumbs/teaching-hours.php';

// Subjects
include_once 'breadcrumbs/subjects.php';

// Schedules
include_once 'breadcrumbs/schedules.php';
