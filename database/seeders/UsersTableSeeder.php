<?php

namespace Database\Seeders;

use App\Models\Subject;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* developers */
        $user = User::firstOrNew([
            'email' => 'achatzi@yahoo.com'
        ]);
        $user->role_id = ROLE_DEVELOPER;
        $user->name = 'SYSTEM';
        $user->surname = '';
        $user->password = bcrypt(time());
        $user->timezone = 'UTC';
        $user->can_login = false;
        $user->save();

        $user = User::firstOrNew([
            'email' => 'achatzi@gmail.com'
        ]);
        $user->role_id = ROLE_DEVELOPER;
        $user->name = 'Αντώνης';
        $user->surname = 'Χατζηκωνσταντής';
        $user->password = bcrypt('wzav0Pff');
        $user->timezone = 'Europe/Athens';
        $user->can_login = true;
        $user->save();

        /* teachers */
        $user = User::firstOrNew([
            'email' => 'elli_kar@hotmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Έλλη';
        $user->surname = 'Καραγιάννη';
        $user->password = bcrypt('elli_kar@hotmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306937003221'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Έκθεση Α Γυμνασίου',
            'Έκθεση Β Γυμνασίου',
            'Αρχαία ΒΘΕΩΡ',
            'Ιστορία ΒΘΕΩΡ',
            'Αρχαία ΓΘΕΩΡ',
            'Ιστορία ΓΘΕΩΡ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'nikikofina10@yahoo.gr'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Νίκη';
        $user->surname = 'Κοφινά';
        $user->password = bcrypt('nikikofina10@yahoo.gr');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306973884306'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'ΑΟΘ ΒΟΙΚ',
            'ΑΟΘ ΓΟΙΚ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'georgemandas@hotmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Γιώργος';
        $user->surname = 'Μαντάς';
        $user->password = bcrypt('georgemandas@hotmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306934838606'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Φυσική Α Λυκείου',
            'Φυσική ΒΘΕΤ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'astropelekion@yahoo.gr'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Μανώλης';
        $user->surname = 'Μεράκης';
        $user->password = bcrypt('astropelekion@yahoo.gr');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306948623323'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Έκθεση Α Γυμνασίου',
            'Λατινικά ΒΘΕΩΡ',
            'Έκθεση ΓΟΙΚ',
            'Λατινικά ΓΘΕΩΡ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'evangelos.fotopoulos@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Ευάγγελος';
        $user->surname = 'Φωτόπουλος';
        $user->password = bcrypt('evangelos.fotopoulos@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306948267559'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'ΑΕΠΠ ΒΟΙΚ',
            'ΑΕΠΠ ΓΟΙΚ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'giorgosanaz@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Γιώργος';
        $user->surname = 'Χατζηκωνσταντής';
        $user->password = bcrypt('giorgosanaz@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306945336615'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'ΑΟΘ ΓΟΙΚ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'verlism@hotmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Μιχάλης';
        $user->surname = 'Βερλής';
        $user->password = bcrypt('verlism@hotmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306942826489'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Φυσική ΓΘΕΤ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'popisarakinou@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Πόπη';
        $user->surname = 'Σαρακηνού';
        $user->password = bcrypt('popisarakinou@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306970482602'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Αρχαία Α Γυμνασίου',
            'Έκθεση Γ Γυμνασίου',
            'Αρχαία Γ Γυμνασίου',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'mariachountasi@hotmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Μαρία';
        $user->surname = 'Χουντασση';
        $user->password = bcrypt('mariachountasi@hotmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306972245737'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Έκθεση ΓΟΙΚ',
            'Έκθεση ΓΘΕΩΡ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'kaberidou@hotmail.gr'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Κωνσταντίνα';
        $user->surname = 'Καμπερίδου';
        $user->password = bcrypt('kaberidou@hotmail.gr');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306930402954'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Αρχαία Α Γυμνασίου',
            'Έκθεση Β Γυμνασίου',
            'Αρχαία Β Γυμνασίου',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'dinanikelli@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Ντίνα';
        $user->surname = 'Νικέλλη';
        $user->password = bcrypt('dinanikelli@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306955315703'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Β Γυμνασίου',
            'Μαθηματικά Γ Γυμνασίου',
            'Μαθηματικά Α Λυκείου',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'elenagiannoulake@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Ελεάννα';
        $user->surname = 'Γιαννουλάκη';
        $user->password = bcrypt('elenagiannoulake@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306989887823'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Αρχαία Γ Γυμνασίου',
            'Έκθεση Α Λυκείου',
            'Αρχαία Α Λυκείου',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'papastamatiou.charis@yahoo.gr'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Χάρης';
        $user->surname = 'Παπασταματίου';
        $user->password = bcrypt('papastamatiou.charis@yahoo.gr');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306948238946'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Φυσική Α Γυμνασίου',
            'Φυσική Β Γυμνασίου',
            'Χημεία Β Γυμνασίου',
            'Φυσική Γ Γυμνασίου',
            'Χημεία Γ Γυμνασίου',
            'Χημεία Α Λυκείου',
            'Χημεία ΒΘΕΤ',
            'Χημεία ΓΘΕΤ',
        ])->get());

        $user = User::firstOrNew([
            'email' => 'samis.salim.m@gmail.com'
        ]);
        $user->role_id = ROLE_TEACHER;
        $user->name = 'Σάμι';
        $user->surname = 'Σαλίμ';
        $user->password = bcrypt('samis.salim.m@gmail.com');
        $user->timezone = 'Athens/Europe';
        $user->can_login = true;
        $user->additional_data = [
            'phones' => [
                'mobile' => '+306946905137'
            ]
        ];
        $user->save();
        $user->academic_years()->sync([1]);
        $user->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Α Γυμνασίου',
            'Μαθηματικά Β Γυμνασίου',
            'Μαθηματικά Γ Γυμνασίου',
            'Μαθηματικά Α Λυκείου',
            'Μαθηματικά ΒΘΕΤ+ΒΟΙΚ',
            'Μαθηματικά ΓΟΙΚ',
        ])->get());
    }
}
