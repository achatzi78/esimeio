<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::updateOrCreate([
            'title' => 'Developer',
        ], [
            'hierarchy' => 0
        ]);

        Role::updateOrCreate([
            'title' => 'Admin',
        ], [
            'hierarchy' => 10
        ]);

        Role::updateOrCreate([
            'title' => 'Γραμματεία',
        ], [
            'hierarchy' => 20
        ]);

        Role::updateOrCreate([
            'title' => 'Ρεσεψιόν',
        ], [
            'hierarchy' => 30
        ]);

        Role::updateOrCreate([
            'title' => 'Καθηγητής',
        ], [
            'hierarchy' => 40
        ]);

        Role::updateOrCreate([
            'title' => 'Μαθητής',
        ], [
            'hierarchy' => 50
        ]);

        Role::updateOrCreate([
            'title' => 'Γονέας',
        ], [
            'hierarchy' => 60
        ]);
    }
}
