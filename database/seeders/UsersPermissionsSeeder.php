<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::with('role.permissions')
            ->get()
            ->each(function ($user) {
                $user->permissions()->sync($user->role->permissions->pluck('id'));
            });
    }
}
