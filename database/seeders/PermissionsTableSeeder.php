<?php

namespace Database\Seeders;

use Database\Seeders\Permissions\AcademicYears;
use Database\Seeders\Permissions\Classrooms;
use Database\Seeders\Permissions\Faculties;
use Database\Seeders\Permissions\Generic;
use Database\Seeders\Permissions\Permissions;
use Database\Seeders\Permissions\Roles;
use Database\Seeders\Permissions\Subjects;
use Database\Seeders\Permissions\TeachingHours;
use Database\Seeders\Permissions\Users;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AcademicYears::class);
        $this->call(Classrooms::class);
        $this->call(Faculties::class);
        $this->call(Generic::class);
        $this->call(Permissions::class);
        $this->call(Roles::class);
        $this->call(Subjects::class);
        $this->call(TeachingHours::class);
        $this->call(Users::class);

    }
}
