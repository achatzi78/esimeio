<?php

namespace Database\Seeders;

use App\Models\Classroom;
use Illuminate\Database\Seeder;

class ClassroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classroom::create([
            'title' => 'Αίθουσα 1'
        ]);

        Classroom::create([
            'title' => 'Αίθουσα 2'
        ]);

        Classroom::create([
            'title' => 'Αίθουσα 3'
        ]);

        Classroom::create([
            'title' => 'Αίθουσα 4'
        ]);

        Classroom::create([
            'title' => 'Αίθουσα 5'
        ]);
    }
}
