<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'title' => 'Μαθηματικά Α Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Φυσική Α Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Έκθεση Α Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Αρχαία Α Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Μαθηματικά Β Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Φυσική Β Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Χημεία Β Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Έκθεση Β Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Αρχαία Β Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Μαθηματικά Γ Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Φυσική Γ Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Χημεία Γ Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Έκθεση Γ Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Αρχαία Γ Γυμνασίου'
        ]);

        Subject::create([
            'title' => 'Μαθηματικά Α Λυκείου'
        ]);

        Subject::create([
            'title' => 'Φυσική Α Λυκείου'
        ]);

        Subject::create([
            'title' => 'Χημεία Α Λυκείου'
        ]);

        Subject::create([
            'title' => 'Έκθεση Α Λυκείου'
        ]);

        Subject::create([
            'title' => 'Αρχαία Α Λυκείου'
        ]);

        Subject::create([
            'title' => 'Μαθηματικά ΒΘΕΤ+ΒΟΙΚ'
        ]);

        Subject::create([
            'title' => 'Φυσική ΒΘΕΤ'
        ]);

        Subject::create([
            'title' => 'Χημεία ΒΘΕΤ'
        ]);

        Subject::create([
            'title' => 'Έκθεση ΒΘΕΤ+ΒΟΙΚ'
        ]);

        Subject::create([
            'title' => 'ΑΕΠΠ ΒΟΙΚ'
        ]);

        Subject::create([
            'title' => 'ΑΟΘ ΒΟΙΚ'
        ]);

        Subject::create([
            'title' => 'Έκθεση ΒΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Αρχαία ΒΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Ιστορία ΒΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Λατινικά ΒΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Έκθεση ΓΟΙΚ'
        ]);

        Subject::create([
            'title' => 'Μαθηματικά ΓΟΙΚ'
        ]);

        Subject::create([
            'title' => 'Φυσική ΓΘΕΤ'
        ]);

        Subject::create([
            'title' => 'Χημεία ΓΘΕΤ'
        ]);

        Subject::create([
            'title' => 'ΑΟΘ ΓΟΙΚ'
        ]);

        Subject::create([
            'title' => 'ΑΕΠΠ ΓΟΙΚ'
        ]);

        Subject::create([
            'title' => 'Έκθεση ΓΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Αρχαία ΓΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Ιστορία ΓΘΕΩΡ'
        ]);

        Subject::create([
            'title' => 'Λατινικά ΓΘΕΩΡ'
        ]);
    }
}
