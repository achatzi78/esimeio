<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(AcademicYearsTableSeeder::class);
        //$this->call(SubjectsTableSeeder::class);
        //$this->call(ClassroomsTableSeeder::class);
        //$this->call(TeachingHoursTableSeeder::class);
        //$this->call(FacultiesTableSeeder::class);
        //$this->call(RolesTableSeeder::class);
        //$this->call(UsersTableSeeder::class);
        //$this->call(PermissionsTableSeeder::class);
        $this->call(RolesPermissionsSeeder::class);
        $this->call(UsersPermissionsSeeder::class);

        //\App\Models\User::factory(40)->create();
    }
}
