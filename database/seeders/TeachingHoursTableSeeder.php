<?php

namespace Database\Seeders;

use App\Models\TeachingHour;
use Illuminate\Database\Seeder;

class TeachingHoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '14:45',
            'end_time' => '15:30'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '15:40',
            'end_time' => '16:25'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '16:35',
            'end_time' => '17:20'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '17:30',
            'end_time' => '18:15'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '18:25',
            'end_time' => '19:10'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '19:20',
            'end_time' => '20:05'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '20:15',
            'end_time' => '21:00'
        ]);

        TeachingHour::create([
            'academic_year_id' => 1,
            'start_time' => '21:10',
            'end_time' => '21:55'
        ]);
    }
}
