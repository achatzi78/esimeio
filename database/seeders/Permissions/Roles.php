<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-roles',
        ], [
            'title' => 'Προβολή Ρόλων',
            'category' => 'Ρόλοι',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα των ρόλων της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-role',
        ], [
            'title' => 'Δημιουργία Ρόλου',
            'category' => 'Ρόλοι',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα νέο ρόλο'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-role',
        ], [
            'title' => 'Επεξεργασία Ρόλου',
            'category' => 'Ρόλοι',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα ρόλο'
        ]);
    }
}
