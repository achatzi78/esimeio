<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class AcademicYears extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-academic-years',
        ], [
            'title' => 'Προβολή Ακαδημαϊκών Ετών',
            'category' => 'Ακαδημαϊκά Έτη',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τα ακαδημαϊκά έτη της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-academic-year',
        ], [
            'title' => 'Προβολή Ακαδημαϊκού Έτους',
            'category' => 'Ακαδημαϊκά Έτη',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα ενός ακαδημαϊκού έτους'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-academic-year',
        ], [
            'title' => 'Δημιουργία Ακαδημαϊκού Έτους',
            'category' => 'Ακαδημαϊκά Έτη',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα νέο ακαδημαϊκό έτος'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-academic-year',
        ], [
            'title' => 'Επεξεργασία Ακαδημαϊκού Έτους',
            'category' => 'Ακαδημαϊκά Έτη',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα ακαδημαϊκό έτος'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-academic-year',
        ], [
            'title' => 'Διαγραφή Ακαδημαϊκού Έτους',
            'category' => 'Ακαδημαϊκά Έτη',
            'description' => 'Ο χρήστης μπορεί να διαγράψει ένα ακαδημαϊκό έτος'
        ]);
    }
}
