<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Generic extends Seeder
{
    public function run()
    {
        //CACHE
        Permission::firstOrCreate([
            'action' => 'clear-cache',
        ], [
            'title' => 'Καθαρισμός Προσωρινής Μνήμης',
            'category' => 'Γενικά',
            'description' => 'Ο χρήστης μπορεί να καθαρίσει την προωρινή μνήμη του συστήματος'
        ]);
    }
}
