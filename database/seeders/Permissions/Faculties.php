<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Faculties extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-faculties',
        ], [
            'title' => 'Προβολή Τμημάτων',
            'category' => 'Τμήματα',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τα τμήματα της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-faculty',
        ], [
            'title' => 'Προβολή Τμήματος',
            'category' => 'Τμήματα',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα ενός τμήματος'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-faculty',
        ], [
            'title' => 'Δημιουργία Τμήματος',
            'category' => 'Τμήματα',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα νέο τμήμα'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-faculty',
        ], [
            'title' => 'Επεξεργασία Τμήματος',
            'category' => 'Τμήματα',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα τμήμα'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-faculty',
        ], [
            'title' => 'Διαγραφή Τμήματος',
            'category' => 'Τμήματα',
            'description' => 'Ο χρήστης μπορεί να διαγράψει ένα τμήμα'
        ]);
    }
}
