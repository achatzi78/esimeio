<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Subjects extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-subjects',
        ], [
            'title' => 'Προβολή Μαθημάτων',
            'category' => 'Μαθήματα',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τα μαθήματα της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-subject',
        ], [
            'title' => 'Προβολή Μαθήματος',
            'category' => 'Μαθήματα',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα ενός μαθήματος'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-subject',
        ], [
            'title' => 'Δημιουργία Μαθήματος',
            'category' => 'Μαθήματα',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα νέο μάθημα'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-subject',
        ], [
            'title' => 'Επεξεργασία Μαθήματος',
            'category' => 'Μαθήματα',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα μάθημα'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-subject',
        ], [
            'title' => 'Διαγραφή Μαθήματος',
            'category' => 'Μαθήματα',
            'description' => 'Ο χρήστης μπορεί να διαγράψει ένα μάθημα'
        ]);
    }
}
