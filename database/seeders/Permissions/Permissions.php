<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Permissions extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-permissions',
        ], [
            'title' => 'Προβολή Δικαιωμάτων',
            'category' => 'Δικαιώματα',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τα δικαιώματα της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-permission',
        ], [
            'title' => 'Προβολή Δικαιώματος',
            'category' => 'Δικαιώματα',
            'description' => 'Ο χρήστης μπορεί να δει ένα δικαίωμα'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-permission',
        ], [
            'title' => 'Δημιουργία Δικαιώματος',
            'category' => 'Δικαιώματα',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα δικαίωμα'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-permission',
        ], [
            'title' => 'Επεξεργασία Δικαιώματος',
            'category' => 'Δικαιώματα',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα δικαίωμα'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-permission',
        ], [
            'title' => 'Διαγραφή Δικαιώματος',
            'category' => 'Δικαιώματα',
            'description' => 'Ο χρήστης μπορεί να διαγράψει ένα δικαίωμα'
        ]);
    }
}
