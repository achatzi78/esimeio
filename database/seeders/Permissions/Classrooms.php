<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Classrooms extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-classrooms',
        ], [
            'title' => 'Προβολή Αιθουσών',
            'category' => 'Αίθουσες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τις αίθουσες της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-classroom',
        ], [
            'title' => 'Προβολή Αίθουσας',
            'category' => 'Αίθουσες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα μιας αίθουσας'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-classroom',
        ], [
            'title' => 'Δημιουργία Αίθουσας',
            'category' => 'Αίθουσες',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει μία νέα αίθουσα'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-classroom',
        ], [
            'title' => 'Επεξεργασία Αίθουσας',
            'category' => 'Αίθουσες',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί μία αίθουσα'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-classroom',
        ], [
            'title' => 'Διαγραφή Αίθουσας',
            'category' => 'Αίθουσες',
            'description' => 'Ο χρήστης μπορεί να διαγράψει μία αίθουσα'
        ]);
    }
}
