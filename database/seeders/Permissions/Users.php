<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-users',
        ], [
            'title' => 'Προβολή Χρηστών',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τους χρήστες της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-user',
        ], [
            'title' => 'Προβολή Χρήστη',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα ενός χρήστη'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-user',
        ], [
            'title' => 'Δημιουργία Χρήστη',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει ένα νέο χρήστη'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-user',
        ], [
            'title' => 'Επεξεργασία Χρήστη',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί ένα χρήστη'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-user',
        ], [
            'title' => 'Διαγραφή Χρήστη',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης μπορεί να διαγράψει ένα χρήστη'
        ]);

        Permission::firstOrCreate([
            'action' => 'edit-user-permissions',
        ], [
            'title' => 'Επεξεργασία Δικαιωμάτων Χρήστη',
            'category' => 'Χρήστες',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί τα δικαιώματα ενός χρήστη'
        ]);
    }
}
