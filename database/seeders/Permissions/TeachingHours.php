<?php

namespace Database\Seeders\Permissions;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class TeachingHours extends Seeder
{
    public function run()
    {
        Permission::firstOrCreate([
            'action' => 'list-teaching-hours',
        ], [
            'title' => 'Προβολή Διδακτικών Ωρών',
            'category' => 'Διδακτικές Ώρες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην λίστα με τα τμήματα της εφαρμογής'
        ]);

        Permission::firstOrCreate([
            'action' => 'view-teaching-hour',
        ], [
            'title' => 'Προβολή Διδακτικής Ώρας',
            'category' => 'Διδακτικές Ώρες',
            'description' => 'Ο χρήστης έχει πρόσβαση στην καρτέλα μιας διδακτικής ώρας'
        ]);

        Permission::firstOrCreate([
            'action' => 'create-teaching-hour',
        ], [
            'title' => 'Δημιουργία Διδακτικής Ώρας',
            'category' => 'Διδακτικές Ώρες',
            'description' => 'Ο χρήστης μπορεί να δημιουργήσει μία νέα διδακτική ώρα'
        ]);

        Permission::firstOrCreate([
            'action' => 'update-teaching-hour',
        ], [
            'title' => 'Επεξεργασία Διδακτικής Ώρας',
            'category' => 'Διδακτικές Ώρες',
            'description' => 'Ο χρήστης μπορεί να επεξεργαστεί μία διδακτική ώρα'
        ]);

        Permission::firstOrCreate([
            'action' => 'delete-teaching-hour',
        ], [
            'title' => 'Διαγραφή Διδακτικής Ώρας',
            'category' => 'Διδακτικές Ώρες',
            'description' => 'Ο χρήστης μπορεί να διαγράψει μία διδακτική ώρα'
        ]);
    }
}
