<?php

namespace Database\Seeders;

use App\Models\Faculty;
use App\Models\Subject;
use Illuminate\Database\Seeder;

class FacultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faculty = Faculty::create([
            'title' => 'Α1 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Α Γυμνασίου',
            'Φυσική Α Γυμνασίου',
            'Έκθεση Α Γυμνασίου',
            'Αρχαία Α Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Α2 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Α Γυμνασίου',
            'Φυσική Α Γυμνασίου',
            'Έκθεση Α Γυμνασίου',
            'Αρχαία Α Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Β1 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Β Γυμνασίου',
            'Φυσική Β Γυμνασίου',
            'Χημεία Β Γυμνασίου',
            'Έκθεση Β Γυμνασίου',
            'Αρχαία Β Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Β2 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Β Γυμνασίου',
            'Φυσική Β Γυμνασίου',
            'Χημεία Β Γυμνασίου',
            'Έκθεση Β Γυμνασίου',
            'Αρχαία Β Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ1 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Γ Γυμνασίου',
            'Φυσική Γ Γυμνασίου',
            'Χημεία Γ Γυμνασίου',
            'Έκθεση Γ Γυμνασίου',
            'Αρχαία Γ Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ2 Γυμνασίου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Γ Γυμνασίου',
            'Φυσική Γ Γυμνασίου',
            'Χημεία Γ Γυμνασίου',
            'Έκθεση Γ Γυμνασίου',
            'Αρχαία Γ Γυμνασίου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Α1 Λυκείου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Α Λυκείου',
            'Φυσική Α Λυκείου',
            'Χημεία Α Λυκείου',
            'Έκθεση Α Λυκείου',
            'Αρχαία Α Λυκείου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Α2 Λυκείου'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Μαθηματικά Α Λυκείου',
            'Φυσική Α Λυκείου',
            'Χημεία Α Λυκείου',
            'Έκθεση Α Λυκείου',
            'Αρχαία Α Λυκείου',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Β-ΘΕΤ'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Έκθεση ΒΘΕΤ+ΒΟΙΚ',
            'Μαθηματικά ΒΘΕΤ+ΒΟΙΚ',
            'Φυσική ΒΘΕΤ',
            'Χημεία ΒΘΕΤ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Β-ΟΙΚ'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'ΑΕΠΠ ΒΟΙΚ',
            'ΑΟΘ ΒΟΙΚ',
            'Έκθεση ΒΘΕΤ+ΒΟΙΚ',
            'Μαθηματικά ΒΘΕΤ+ΒΟΙΚ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Β-ΘΕΩΡ'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Αρχαία ΒΘΕΩΡ',
            'Έκθεση ΒΘΕΩΡ',
            'Ιστορία ΒΘΕΩΡ',
            'Λατινικά ΒΘΕΩΡ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ-ΘΕΤ'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Φυσική ΓΘΕΤ',
            'Χημεία ΓΘΕΤ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ-ΟΙΚ1'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'ΑΕΠΠ ΓΟΙΚ',
            'ΑΟΘ ΓΟΙΚ',
            'Έκθεση ΓΟΙΚ',
            'Μαθηματικά ΓΟΙΚ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ-ΟΙΚ2'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'ΑΕΠΠ ΓΟΙΚ',
            'ΑΟΘ ΓΟΙΚ',
            'Έκθεση ΓΟΙΚ',
            'Μαθηματικά ΓΟΙΚ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ-ΟΙΚ3'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'ΑΕΠΠ ΓΟΙΚ',
            'ΑΟΘ ΓΟΙΚ',
            'Έκθεση ΓΟΙΚ',
            'Μαθηματικά ΓΟΙΚ',
        ])->get());

        $faculty = Faculty::create([
            'title' => 'Γ-ΘΕΩΡ'
        ]);
        $faculty->subjects()->sync(Subject::whereIn('title', [
            'Αρχαία ΓΘΕΩΡ',
            'Έκθεση ΓΘΕΩΡ',
            'Ιστορία ΓΘΕΩΡ',
            'Λατινικά ΓΘΕΩΡ',
        ])->get());
    }
}
