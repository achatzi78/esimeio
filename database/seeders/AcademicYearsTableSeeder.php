<?php

namespace Database\Seeders;

use App\Models\AcademicYear;
use Illuminate\Database\Seeder;

class AcademicYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicYear::create([
            'title' => '2021 - 2022',
            'start_year' => 2021,
            'end_year' => 2022,
            'is_current' => true,
            'months' => [
                9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7
            ],
        ]);
    }
}
