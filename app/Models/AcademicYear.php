<?php

namespace App\Models;

use App\Concerns\Filters\WithBooleanFilter;
use App\Concerns\Filters\WithTitleFilter;
use Arr;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\AcademicYear
 *
 * @property int $id
 * @property string $title
 * @property string $start_year
 * @property string $end_year
 * @property bool $is_current
 * @property array $months
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|TeachingHour[] $teaching_hours
 * @property-read int|null $teaching_hours_count
 * @method static Builder|AcademicYear booleanFilter(string $column, string $filter_key, array $filters = [])
 * @method static Builder|AcademicYear filter(array $filters = [])
 * @method static Builder|AcademicYear newModelQuery()
 * @method static Builder|AcademicYear newQuery()
 * @method static Builder|AcademicYear query()
 * @method static Builder|AcademicYear sortBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|AcademicYear titleFilter(array $filters = [])
 * @method static Builder|AcademicYear whereCreatedAt($value)
 * @method static Builder|AcademicYear whereEndYear($value)
 * @method static Builder|AcademicYear whereId($value)
 * @method static Builder|AcademicYear whereIsCurrent($value)
 * @method static Builder|AcademicYear whereMonths($value)
 * @method static Builder|AcademicYear whereStartYear($value)
 * @method static Builder|AcademicYear whereTitle($value)
 * @method static Builder|AcademicYear whereUpdatedAt($value)
 * @mixin Eloquent
 */
class AcademicYear extends BaseModel
{
    use HasFactory, WithTitleFilter, WithBooleanFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'start_year',
        'end_year',
        'is_current',
        'months',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_current' => 'boolean',
        'months' => 'array',
    ];

    public function teaching_hours(): HasMany
    {
        return $this->hasMany(TeachingHour::class);
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->titleFilter($filters)
            ->booleanFilter('is_current', 'current', $filters)
            ->when(Arr::get($filters, 'start_year'), fn($query) => $query->whereStartYear($filters['start_year']))
            ->when(Arr::get($filters, 'end_year'), fn($query) => $query->whereEndYear($filters['end_year']))
            ->sortBy($filters, 'is_current', 'desc');
    }

    public static function months($key = null, $default = null)
    {
        $months = [];
        $start = Carbon::parse('2021-08-01');

        for ($i = 1; $i <= 12; $i++) {
            $month = $start->addMonth();

            $months["{$month->month}"] = $month->monthName;
        }

        if (!empty($key)) {
            return Arr::get($months, $key, $default) ?? $default;
        }

        return $months;
    }
}
