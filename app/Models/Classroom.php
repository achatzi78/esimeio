<?php

namespace App\Models;

use App\Concerns\Filters\WithTitleFilter;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;


/**
 * App\Models\Classroom
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Classroom filter(array $filters = [])
 * @method static Builder|Classroom newModelQuery()
 * @method static Builder|Classroom newQuery()
 * @method static Builder|Classroom query()
 * @method static Builder|Classroom sortBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|Classroom titleFilter(array $filters = [])
 * @method static Builder|Classroom whereCreatedAt($value)
 * @method static Builder|Classroom whereId($value)
 * @method static Builder|Classroom whereTitle($value)
 * @method static Builder|Classroom whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Classroom extends BaseModel
{
    use HasFactory, WithTitleFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->titleFilter($filters)
            ->sortBy($filters);
    }
}
