<?php

namespace App\Models;

use App\Concerns\ExposesSchema;
use App\Concerns\Filters\WithTitleFilter;
use App\Concerns\IsOrdered;
use Arr;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection as SupportCollection;

/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string|null $category
 * @property string $action
 * @property string $title
 * @property string|null $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read EloquentCollection|Role[] $roles
 * @property-read int|null $roles_count
 * @property-read EloquentCollection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Permission filter(array $filters = [])
 * @method static Builder|Permission newModelQuery()
 * @method static Builder|Permission newQuery()
 * @method static Builder|Permission query()
 * @method static Builder|Permission sortBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|Permission titleFilter(array $filters = [])
 * @method static Builder|Permission whereAction($value)
 * @method static Builder|Permission whereCategory($value)
 * @method static Builder|Permission whereCreatedAt($value)
 * @method static Builder|Permission whereDescription($value)
 * @method static Builder|Permission whereId($value)
 * @method static Builder|Permission whereTitle($value)
 * @method static Builder|Permission whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Permission extends Model
{
    use HasFactory, ExposesSchema, IsOrdered, WithTitleFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category',
        'title',
        'action',
        'description'
    ];

    public function roles(): MorphToMany
    {
        return $this->morphedByMany(Role::class, 'permissionable');
    }

    public function users(): MorphToMany
    {
        return $this->morphedByMany(User::class, 'permissionable');
    }

    public static function categories(): SupportCollection
    {
        return self::select('category')
            ->distinct()
            ->orderBy('category')
            ->pluck('category');
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->titleFilter($filters)
            ->when(Arr::get($filters, 'category'), fn($query) => $query->whereCategory($filters['category']))
            ->when(Arr::get($filters, 'description'), fn($query) => $query->where('description', 'like', '%' . $filters['description'] . '%'))
            ->when(Arr::get($filters, 'action'), fn($query) => $query->where('action', 'like', '%' . $filters['action'] . '%'))
            ->sortBy($filters, 'category');
    }
}
