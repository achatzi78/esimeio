<?php

namespace App\Models;

use App\Concerns\HasPermissions;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Role
 *
 * @property int $id
 * @property int $hierarchy
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Role lower_equal(?int $hierarchy = null)
 * @method static Builder|Role newModelQuery()
 * @method static Builder|Role newQuery()
 * @method static Builder|Role query()
 * @method static Builder|Role whereCreatedAt($value)
 * @method static Builder|Role whereDeletedAt($value)
 * @method static Builder|Role whereHierarchy($value)
 * @method static Builder|Role whereId($value)
 * @method static Builder|Role whereTitle($value)
 * @method static Builder|Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends Model
{
    use HasFactory, HasPermissions;

    protected $fillable = [
        'title',
        'hierarchy'
    ];

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * @param Builder $builder
     * @param int|null $hierarchy
     *
     * @return Builder
     */
    public function scopeLower_equal(Builder $builder, int $hierarchy = null): Builder
    {
        $hierarchy ??= auth()->user()->role->hierarchy;

        return $builder->where('hierarchy', '>=', $hierarchy);
    }
}
