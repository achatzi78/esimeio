<?php

namespace App\Models;

use App\Concerns\ExposesSchema;
use App\Concerns\IsOrdered;
use App\Concerns\WithDefaultCasting;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use ExposesSchema, IsOrdered, WithDefaultCasting;

    protected $perPage = 25;

    /**
     * Set the value of an enum castable attribute.
     *
     * @param  string  $key
     * @param  BackedEnum  $value
     * @return void
     */
    protected function setEnumCastableAttribute($key, $value)
    {
        $enumClass = $this->getCasts()[$key];

        if (!isset($value)) {
            $this->attributes[$key] = null;
        }
        elseif ($value instanceof $enumClass) {
            $this->attributes[$key] = $value->value;
        }
        else {
            $this->attributes[$key] = optional($enumClass::tryFrom($value))->value ?? null;
        }
    }
}
