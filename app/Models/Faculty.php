<?php

namespace App\Models;

use App\Concerns\Filters\WithTitleFilter;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Faculty
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Subject[] $subjects
 * @property-read int|null $subjects_count
 * @method static Builder|Faculty filter(array $filters = [])
 * @method static Builder|Faculty newModelQuery()
 * @method static Builder|Faculty newQuery()
 * @method static Builder|Faculty query()
 * @method static Builder|Faculty sortBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|Faculty titleFilter(array $filters = [])
 * @method static Builder|Faculty whereCreatedAt($value)
 * @method static Builder|Faculty whereId($value)
 * @method static Builder|Faculty whereTitle($value)
 * @method static Builder|Faculty whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Faculty extends BaseModel
{
    use HasFactory, WithTitleFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @return BelongsToMany
     */
    public function subjects(): BelongsToMany
    {
        return $this->belongsToMany(Subject::class, 'faculties_subjects');
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->titleFilter($filters)
            ->sortBy($filters, 'title');
    }
}
