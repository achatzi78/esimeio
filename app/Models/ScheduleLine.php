<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ScheduleLine
 *
 * @property int $id
 * @property int $schedule_id
 * @property int $faculty_id
 * @property int $subject_id
 * @property int $teacher_id
 * @property int $teaching_hour_id
 * @property int $classroom_id
 * @property int $weekday
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Classroom $classroom
 * @property-read Faculty $faculty
 * @property-read Schedule $schedule
 * @property-read Subject $subject
 * @property-read Teacher $teacher
 * @property-read TeachingHour $teaching_hour
 * @method static Builder|ScheduleLine newModelQuery()
 * @method static Builder|ScheduleLine newQuery()
 * @method static Builder|ScheduleLine query()
 * @method static Builder|ScheduleLine whereClassroomId($value)
 * @method static Builder|ScheduleLine whereCreatedAt($value)
 * @method static Builder|ScheduleLine whereFacultyId($value)
 * @method static Builder|ScheduleLine whereId($value)
 * @method static Builder|ScheduleLine whereScheduleId($value)
 * @method static Builder|ScheduleLine whereSubjectId($value)
 * @method static Builder|ScheduleLine whereTeacherId($value)
 * @method static Builder|ScheduleLine whereTeachingHourId($value)
 * @method static Builder|ScheduleLine whereUpdatedAt($value)
 * @method static Builder|ScheduleLine whereWeekDay($value)
 * @mixin Eloquent
 */
class ScheduleLine extends Model
{
    use HasFactory;

    protected $fillable = [
        'schedule_id',
        'faculty_id',
        'subject_id',
        'teacher_id',
        'teaching_hour_id',
        'classroom_id',
        'weekday',
    ];

    /**
     * @return BelongsTo
     */
    public function schedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class);
    }

    /**
     * @return BelongsTo
     */
    public function faculty(): BelongsTo
    {
        return $this->belongsTo(Faculty::class);
    }

    /**
     * @return BelongsTo
     */
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * @return BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * @return BelongsTo
     */
    public function teaching_hour(): BelongsTo
    {
        return $this->belongsTo(TeachingHour::class);
    }

    /**
     * @return BelongsTo
     */
    public function classroom(): BelongsTo
    {
        return $this->belongsTo(Classroom::class);
    }
}
