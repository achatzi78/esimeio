<?php

namespace App\Models;

use App\Concerns\ExposesSchema;
use App\Concerns\Filters\WithTitleFilter;
use App\Concerns\IsOrdered;
use Arr;
use datetime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;



/**
 * App\Models\TeachingHour
 *
 * @property int $id
 * @property int $academic_year_id
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read AcademicYear $academic_year
 * @property-read string $ends_at
 * @property-read string $starts_at
 * @property-read string $title
 * @method static Builder|TeachingHour filter(array $filters = [])
 * @method static Builder|TeachingHour newModelQuery()
 * @method static Builder|TeachingHour newQuery()
 * @method static Builder|TeachingHour query()
 * @method static Builder|TeachingHour sortBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|TeachingHour whereAcademicYearId($value)
 * @method static Builder|TeachingHour whereCreatedAt($value)
 * @method static Builder|TeachingHour whereEndTime($value)
 * @method static Builder|TeachingHour whereId($value)
 * @method static Builder|TeachingHour whereStartTime($value)
 * @method static Builder|TeachingHour whereUpdatedAt($value)
 * @mixin Eloquent
 */
class TeachingHour extends BaseModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'academic_year_id',
        'start_time',
        'end_time',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start_time' => 'datetime:H:i',
        'end_time' => 'datetime:H:i',
    ];

    public function academic_year()
    {
        return $this->belongsTo(AcademicYear::class);
    }

    public function startsAt(): Attribute
    {
        return Attribute::get(fn() => $this->start_time->format('H:i'));
    }

    public function endsAt(): Attribute
    {
        return Attribute::get(fn() => $this->end_time->format('H:i'));
    }

    public function title(): Attribute
    {
        return Attribute::get(fn() => "{$this->starts_at} - {$this->ends_at}");
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->when(Arr::get($filters, 'title'), fn($query) => $query->whereRaw("CONCAT(start_time, ' - ', end_time) LIKE ?", '%' . $filters['title'] . '%'))
            ->when(Arr::get($filters, 'academic_year'), fn($query) => $query->whereAcademicYearId($filters['academic_year']))
            ->when(Arr::get($filters, 'order_by') == 'title', fn($query) => $query->orderBy('start_time', Arr::get($filters, 'sort', 'asc')))
            ->sortBy($filters, 'academic_year_id', 'desc');
    }
}
