<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;


class Schedule extends Model
{
    use HasFactory;

    /**
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(ScheduleLine::class);
    }
}
