<?php

namespace App\Models;

use App\Concerns\ExposesSchema;
use App\Concerns\HasPermissions;
use App\Concerns\IsOrdered;
use Arr;
use Database\Factories\UserFactory;
use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string|null $password
 * @property string|null $timezone
 * @property int $can_login
 * @property array|null $additional_data
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Collection|AcademicYear[] $academic_years
 * @property-read int|null $academic_years_count
 * @property-read string $fullname
 * @property-read string|null $home_phone
 * @property-read bool $is_admin
 * @property-read bool $is_developer
 * @property-read bool $is_parent
 * @property-read bool $is_student
 * @property-read bool $is_teacher
 * @property-read string|null $mobile
 * @property-read string $official_name
 * @property-read string|null $phone
 * @property-read string|null $work_phone
 * @property-read array|null $weekdays
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Role $role
 * @property-read Collection|Subject[] $subjects
 * @property-read int|null $subjects_count
 * @property-read Collection|TeachingHour[] $teaching_hours
 * @property-read int|null $teaching_hours_count
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User admins()
 * @method static Builder|User allowed($user)
 * @method static Builder|User canLogin()
 * @method static Builder|User developers()
 * @method static UserFactory factory(...$parameters)
 * @method static Builder|User filter(array $filters = [])
 * @method static Builder|User search(array $search = [])
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User order_by(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|User parents()
 * @method static Builder|User query()
 * @method static Builder|User students()
 * @method static Builder|User teachers()
 * @method static Builder|User whereCanLogin($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereData($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRoleId($value)
 * @method static Builder|User whereSurname($value)
 * @method static Builder|User whereTimezone($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasPermissions, ExposesSchema, IsOrdered;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'timezone',
        'additional_data',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'additional_data' => 'array',
    ];

    protected $perPage = 9;

    /**
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return BelongsToMany
     */
    public function academic_years(): BelongsToMany
    {
        return $this->belongsToMany(AcademicYear::class, 'academic_years_users', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function subjects(): BelongsToMany
    {
        return $this->belongsToMany(Subject::class, 'users_subjects');
    }

    /**
     * @return BelongsToMany
     */
    public function availabilities(): BelongsToMany
    {
        return $this->belongsToMany(UserAvailability::class, 'users_availabilities');
    }

    public function datum($key, $default = null)
    {
        return Arr::get($this->additional_data, $key, $default) ?? $default;
    }

    public function getFormattedTeachingHoursAttribute()
    {
        //return $this->
    }

    public function getFullnameAttribute(): string
    {
        return "{$this->name} {$this->surname}";
    }

    public function getOfficialNameAttribute(): string
    {
        return "{$this->surname} {$this->name}";
    }

    public function getIsDeveloperAttribute(): bool
    {
        return $this->role_id == ROLE_DEVELOPER;
    }

    public function getIsAdminAttribute(): bool
    {
        return $this->role_id == ROLE_ADMIN;
    }

    public function getIsTeacherAttribute(): bool
    {
        return $this->role_id == ROLE_TEACHER;
    }

    public function getIsStudentAttribute(): bool
    {
        return $this->role_id == ROLE_STUDENT;
    }

    public function getIsParentAttribute(): bool
    {
        return $this->role_id == ROLE_PARENT;
    }

    public function getHomePhoneAttribute(): ?string
    {
        return $this->datum('phones.home');
    }

    public function getWorkPhoneAttribute(): ?string
    {
        return $this->datum('phones.work');
    }

    public function getPhoneAttribute(): ?string
    {
        return $this->datum('phones.home', $this->datum('phones.work'));
    }

    public function getMobileAttribute(): ?string
    {
        return $this->datum('phones.mobile');
    }

    public function getWeekdaysAttribute(): ?string
    {
        return $this->datum('weekdays');
    }

    public function scopeDevelopers(Builder $builder): Builder
    {
        return $builder->where('role_id', ROLE_DEVELOPER);
    }

    public function scopeAdmins(Builder $builder): Builder
    {
        return $builder->where('role_id', ROLE_ADMIN);
    }

    public function scopeTeachers(Builder $builder): Builder
    {
        return $builder->where('role_id', ROLE_TEACHER);
    }

    public function scopeStudents(Builder $builder): Builder
    {
        return $builder->where('role_id', ROLE_STUDENT);
    }

    public function scopeParents(Builder $builder): Builder
    {
        return $builder->where('role_id', ROLE_PARENT);
    }

    public function scopeCanLogin(Builder $builder): Builder
    {
        return $builder->where('can_login', 1);
    }

    public function scopeAllowed(Builder $builder, $user): Builder
    {
        $user ??= auth()->user();

        return $builder->when(!$user->is_developer, function ($query) use ($user) {
            return $query
                ->when(in_array($user->role_id, [ROLE_ADMIN, ROLE_RECEPTION, ROLE_SECRETARY]), function ($qr) use ($user) {
                    return $qr->whereHas('role', function ($q) use ($user) {
                        return $q->where('hierarchy', '>=', $user->role->hierarchy);
                    });
                })
                ->when($user->is_teacher, function ($qr) use ($user) {
                    return $qr->whereHas('role', function ($q) use ($user) {
                        return $q->where('hierarchy', '>=', $user->role->hierarchy);
                    });
                })
                ->when($user->is_student, function ($qr) use ($user) {
                    return $qr->whereId($user->id);
                })
                ;
        });
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder
            ->when(Arr::get($filters, 'email'), function ($query) use ($filters) {
                return $query->where('email', 'like', '%' . $filters['email'] . '%');
            })
            ->when(Arr::get($filters, 'name'), function ($query) use ($filters) {
                return $query->where(function ($q) use ($filters) {
                    return $q
                        ->where('name', 'like', '%' . $filters['name'] . '%')
                        ->orWhere('surname', 'like', '%' . $filters['name'] . '%');
                });
            })
            ->when(Arr::get($filters, 'role'), function ($query) use ($filters) {
                return $query->where('role_id', intval($filters['role']));
            })
            ->when(Arr::get($filters, 'phone'), function ($query) use ($filters) {
                return $query->where(function ($q) use ($filters) {
                    return $q
                        ->whereRaw('JSON_EXTRACT(data, "$.phones.home") LIKE ?', '%' . $filters['phone'] . '%')
                        ->orWhereRaw('JSON_EXTRACT(data, "$.phones.work") LIKE ?', '%' . $filters['phone'] . '%')
                        ->orWhereRaw('JSON_EXTRACT(data, "$.phones.mobile") LIKE ?', '%' . $filters['phone'] . '%');
                });
            })
            ->when(Arr::get($filters, 'login') == 1, function ($query) use ($filters) {
                return $query->whereCanLogin(1);
            })
            ->when(Arr::get($filters, 'login') === '0', function ($query) use ($filters) {
                return $query->whereCanLogin(0);
            })
            ->orderBy('role_id')
            ->orderBy('surname');
    }

    /**
     * @param Builder $builder
     * @param array $search
     *
     * @return Builder
     */
    public function scopeSearch(Builder $builder, array $search = []): Builder
    {
        return $builder
            ->where(function ($query) use ($search) {
                return $query
                    ->when(Arr::get($search, 'email'), function ($query) use ($search) {
                        return $query->orWhere('email', 'like', '%' . $search['email'] . '%');
                    })
                    ->when(Arr::get($search, 'name'), function ($query) use ($search) {
                        return $query
                            ->orWhere('name', 'like', '%' . $search['name'] . '%')
                            ->orWhere('surname', 'like', '%' . $search['name'] . '%');
                    })
                    ->when(Arr::get($search, 'phone'), function ($query) use ($search) {
                        return $query->orWhere(function ($q) use ($search) {
                            return $q
                                ->orWhereRaw('JSON_EXTRACT(data, "$.phones.home") LIKE ?', '%' . $search['phone'] . '%')
                                ->orWhereRaw('JSON_EXTRACT(data, "$.phones.work") LIKE ?', '%' . $search['phone'] . '%')
                                ->orWhereRaw('JSON_EXTRACT(data, "$.phones.mobile") LIKE ?', '%' . $search['phone'] . '%');
                        });
                    });

            })
            ->orderBy('surname');
    }
}
