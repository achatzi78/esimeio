<?php

namespace App\Models;

use App\Concerns\ExposesSchema;
use App\Concerns\IsOrdered;
use Arr;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Subject
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Faculty[] $faculties
 * @property-read int|null $faculties_count
 * @property-read Collection|User[] $teachers
 * @property-read int|null $teachers_count
 * @method static Builder|Subject filter(array $filters = [])
 * @method static Builder|Subject newModelQuery()
 * @method static Builder|Subject newQuery()
 * @method static Builder|Subject order_by(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|Subject query()
 * @method static Builder|Subject whereCreatedAt($value)
 * @method static Builder|Subject whereId($value)
 * @method static Builder|Subject whereTitle($value)
 * @method static Builder|Subject whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Subject extends Model
{
    use HasFactory, ExposesSchema, IsOrdered;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @return BelongsToMany
     */
    public function teachers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_subjects');
    }

    /**
     * @return BelongsToMany
     */
    public function faculties(): BelongsToMany
    {
        return $this->belongsToMany(Faculty::class, 'faculties_subjects');
    }

    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        $filters = array_merge($filters, request()->input('filters', []), ['sort' => request('sort'), 'order_by' => request('order_by')]);

        return $builder
            ->when(Arr::get($filters, 'title'), function ($query) use ($filters) {
                return $query->where('title', 'like', '%' . $filters['title'] . '%');
            })
            ->when(Arr::get($filters, 'teacher'), function ($query) use ($filters) {
                return $query->whereHas('teachers', function ($q) use ($filters) {
                    return $q->whereId($filters['teacher']);
                });
            })
            ->sortBy($filters);
    }
}
