<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Admin
 *
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $email
 * @property string|null $password
 * @property string|null $timezone
 * @property int $can_login
 * @property array|null $data
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read string|null $home_phone
 * @property-read bool $is_admin
 * @property-read bool $is_developer
 * @property-read bool $is_parent
 * @property-read bool $is_student
 * @property-read bool $is_teacher
 * @property-read string|null $mobile
 * @property-read string|null $phone
 * @property-read string|null $work_phone
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \App\Models\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User can_login()
 * @method static Builder|User admins()
 * @method static Builder|User developers()
 * @method static Builder|User filter(array $filters = [])
 * @method static Builder|Admin newModelQuery()
 * @method static Builder|Admin newQuery()
 * @method static Builder|User orderBy(array $parameters, $default_column = null, $default_direction = null)
 * @method static Builder|User parents()
 * @method static Builder|Admin query()
 * @method static Builder|User students()
 * @method static Builder|User teachers()
 * @method static Builder|Admin whereCreatedAt($value)
 * @method static Builder|Admin whereData($value)
 * @method static Builder|Admin whereDeletedAt($value)
 * @method static Builder|Admin whereEmail($value)
 * @method static Builder|Admin whereId($value)
 * @method static Builder|Admin whereICanLogin($value)
 * @method static Builder|Admin whereName($value)
 * @method static Builder|Admin wherePassword($value)
 * @method static Builder|Admin whereRememberToken($value)
 * @method static Builder|Admin whereRoleId($value)
 * @method static Builder|Admin whereTimezone($value)
 * @method static Builder|Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static Builder|User canLogin()
 * @method static Builder|Admin whereCanLogin($value)
 */
class Admin extends User
{
    protected $table = 'users';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('admins', function (Builder $builder) {
            $builder->where('role_id', 2);
        });
    }
}
