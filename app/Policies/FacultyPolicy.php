<?php

namespace App\Policies;

use App\Models\Faculty;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class FacultyPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-faculties');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Faculty $faculty
     *
     * @return Response|bool
     */
    public function view(User $user, Faculty $faculty): Response|bool
    {
        return $user->is_allowed_to('view-faculty');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-faculty');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Faculty $faculty
     *
     * @return Response|bool
     */
    public function update(User $user, Faculty $faculty): Response|bool
    {
        return $user->is_allowed_to('update-faculty');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Faculty $faculty
     *
     * @return Response|bool
     */
    public function delete(User $user, Faculty $faculty): Response|bool
    {
        return $user->is_allowed_to('delete-faculty');
    }
}
