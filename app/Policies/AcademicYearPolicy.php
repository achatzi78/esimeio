<?php

namespace App\Policies;

use App\Models\AcademicYear;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AcademicYearPolicy /*extends BasePolicy*/
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-academic-years');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param AcademicYear $academic_year
     *
     * @return Response|bool
     */
    public function view(User $user, AcademicYear $academic_year): Response|bool
    {
        return $user->is_allowed_to('view-academic-year');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-academic-year');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param AcademicYear $academic_year
     *
     * @return Response|bool
     */
    public function update(User $user, AcademicYear $academic_year): Response|bool
    {
        return $user->is_allowed_to('update-academic-year');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param AcademicYear $academic_year
     *
     * @return Response|bool
     */
    public function delete(User $user, AcademicYear $academic_year): Response|bool
    {
        if ($academic_year->is_current) {
            return Response::deny('Δεν μπορείτε να διαγράψετε το τρέχων ακαδημαϊκό έτος');
        }

        return $user->is_allowed_to('delete-academic-year');
    }
}
