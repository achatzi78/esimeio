<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RolePolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-roles');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-role');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User  $user
     * @param Role $role
     *
     * @return Response|bool
     */
    public function update(User $user, Role $role): Response|bool
    {
        return $user->is_allowed_to('update-role') && $role->hierarchy > $user->role->hierarchy;
    }
}
