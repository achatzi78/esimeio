<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->is_allowed_to('list-users');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function view(User $user, User $model): bool
    {
        if ($user->is_developer || $user->id == $model->id) {
            return true;
        }

        return $user->is_allowed_to('view-user');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->is_allowed_to('create-user');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function update(User $user, User $model): bool
    {
        return $user->is_allowed_to('update-user');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return Response|bool
     */
    public function delete(User $user, User $model): Response|bool
    {
        if ($user->id == $model->id) {
            return Response::deny('Δεν μπορείτε να διαγράψετε τον εαυτό σας!');
        }

        return $user->is_allowed_to('delete-user');
    }
}
