<?php

namespace App\Policies;

use App\Models\Classroom;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ClassroomPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-classrooms');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Classroom $classroom
     *
     * @return Response|bool
     */
    public function view(User $user, Classroom $classroom): Response|bool
    {
        return $user->is_allowed_to('view-classroom');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-classroom');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Classroom $classroom
     *
     * @return Response|bool
     */
    public function update(User $user, Classroom $classroom): Response|bool
    {
        return $user->is_allowed_to('update-classroom');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Classroom $classroom
     *
     * @return Response|bool
     */
    public function delete(User $user, Classroom $classroom): Response|bool
    {
        return $user->is_allowed_to('delete-classroom');
    }
}
