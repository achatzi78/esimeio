<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PermissionPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-permissions');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User  $user
     * @param Permission $permission
     *
     * @return Response|bool
     */
    public function view(User $user, Permission $permission): Response|bool
    {
        return $user->is_allowed_to('view-permission');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-permission');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  User  $user
     * @param Permission $permission
     *
     * @return Response|bool
     */
    public function update(User $user, Permission $permission): Response|bool
    {
        return $user->is_allowed_to('update-permission');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @param Permission  $permission
     *
     * @return Response|bool
     */
    public function delete(User $user, Permission $permission): Response|bool
    {
        return $user->is_allowed_to('delete-permission');
    }
}
