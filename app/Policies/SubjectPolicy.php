<?php

namespace App\Policies;

use App\Models\Subject;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SubjectPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function viewAny(User $user): Response|bool
    {
        return $user->is_allowed_to('list-subjects');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User  $user
     * @param Subject $subject
     *
     * @return Response|bool
     */
    public function view(User $user, Subject $subject): Response|bool
    {
        return $user->is_allowed_to('view-subject');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return Response|bool
     */
    public function create(User $user): Response|bool
    {
        return $user->is_allowed_to('create-subject');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  User  $user
     * @param Subject $subject
     *
     * @return Response|bool
     */
    public function update(User $user, Subject $subject): Response|bool
    {
        return $user->is_allowed_to('update-subject');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @param Subject  $subject
     *
     * @return Response|bool
     */
    public function delete(User $user, Subject $subject): Response|bool
    {
        return $user->is_allowed_to('delete-subject');
    }
}
