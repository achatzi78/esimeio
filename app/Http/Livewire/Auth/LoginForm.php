<?php

namespace App\Http\Livewire\Auth;

use App\Providers\RouteServiceProvider;
use Auth;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;
use Livewire\Component;
use RateLimiter;

class LoginForm extends Component
{
    public ?string $email = '';

    public ?string $password = '';

    public bool $remember_me = false;

    public function mount(?string $email = null, $remember_me = false)
    {
        $this->email = $email;
        $this->remember_me = $remember_me;
    }

    public function render()
    {
        return view('livewire.auth.login-form');
    }

    public function login()
    {
        $this->validate();

        $this->ensureIsNotRateLimited();

        if (!Auth::attempt([
            'can_login' => 1,
            'email' => $this->email,
            'password' => $this->password
        ], $this->remember_me)) {
            RateLimiter::hit($this->throttleKey());

            throw ValidationException::withMessages([
                'email' => trans('auth.failed'),
            ]);
        }

        RateLimiter::clear($this->throttleKey());

        session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    #[ArrayShape(['email' => "string[]", 'password' => "string[]"])]
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email'
            ],
            'password' => [
                'required',
                'string'
            ],
        ];
    }

    /**
     * Ensure the login request is not rate limited.
     *
     * @return void
     *
     * @throws ValidationException
     */
    protected function ensureIsNotRateLimited(): void
    {
        if (!RateLimiter::tooManyAttempts($this->throttleKey(), 5)) {
            return;
        }

        //event(new Lockout($this));

        $seconds = RateLimiter::availableIn($this->throttleKey());

        throw ValidationException::withMessages([
            'email' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    /**
     * Get the rate limiting throttle key for the request.
     *
     * @return string
     */
    protected function throttleKey(): string
    {
        return str($this->email)
            ->lower()
            ->append('|', request()->ip())
            ->toString();
    }
}
