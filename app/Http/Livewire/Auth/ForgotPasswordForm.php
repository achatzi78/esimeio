<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Support\Facades\Password;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use JetBrains\PhpStorm\ArrayShape;
use Livewire\Component;

class ForgotPasswordForm extends Component
{
    use LivewireAlert;

    public ?string $email = '';

    public function render()
    {
        return view('livewire.auth.forgot-password-form');
    }

    public function send(): void
    {
        $this->validate();

        $status = Password::sendResetLink([
            'email' => $this->email
        ]);

        if ($status == Password::RESET_LINK_SENT) {
            $this->alert('success', 'Επιτυχής Αποστολή', [
                'text' => __($status),
                'position' => 'top',
                'timer' => 4000,
                'width' => 600,
            ]);

            $this->reset();
        }
        else {
            $this->addError('email', __($status));
        }
    }

    #[ArrayShape(['email' => "string[]"])]
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email'
            ],
        ];
    }
}
