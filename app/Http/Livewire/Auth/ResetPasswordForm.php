<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password as PasswordRule;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class ResetPasswordForm extends Component
{
    use LivewireAlert;

    public ?string $token = null;

    public ?string $email = null;

    public ?string $password = null;

    public ?string $password_confirmation = null;

    public function mount(?string $token = null, ?string $email = null)
    {
        $this->token = $token;
        $this->email = $email;
    }

    public function render()
    {
        return view('livewire.auth.reset-password-form');
    }

    public function save()
    {
        $this->validate();

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $status = Password::reset([
            'email' => $this->email,
            'password' => $this->password,
            'password_confirmation' => $this->password_confirmation,
            'token' => $this->token,
        ], function($user) {
            $user->forceFill([
                'password' => Hash::make($this->password),
                'remember_token' => Str::random(60),
            ])->save();

            event(new PasswordReset($user));
        });

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($status == Password::PASSWORD_RESET) {
            $this->flash('success', '', [
                'text' => __($status),
                'position' => 'top',
                'timer' => 4000,
            ], route('login'));
        }

        $this->addError('email', __($status));
    }

    public function rules()
    {
        return [
            'email' => [
                'required',
                'email'
            ],
            'password' => [
                'required',
                PasswordRule::defaults(),
                'max:20',
                'confirmed',
            ],
            'password_confirmation' => [
                'required',
            ],
        ];
    }
}
