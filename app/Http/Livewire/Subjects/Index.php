<?php

namespace App\Http\Livewire\Subjects;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\Subject;
use App\Models\User;
use Arr;

class Index extends MasterIndex
{
    public function getFilteredTeacherProperty()
    {
        return User::find(Arr::get($this->filters, 'teacher'));
    }

    protected function builder()
    {
        return $this->model()::with([
            'faculties',
            'teachers',
        ]);
    }

    protected function model()
    {
        return Subject::class;
    }

    protected function view()
    {
        return 'livewire.subjects.index';
    }
}
