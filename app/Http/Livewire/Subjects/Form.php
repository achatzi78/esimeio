<?php

namespace App\Http\Livewire\Subjects;

use App\Models\Faculty;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests;

    /**
     * @var Subject
     */
    public Subject $subject;

    /**
     * @var array
     */
    public array $faculties = [];

    /**
     * @var array
     */
    public array $teachers = [];

    public function mount(?Subject $subject = null)
    {
        $this->subject = $subject ?? new Subject();
        $this->faculties = $this->subject
                ->faculties
                ->pluck('id')
                ->map(function ($id) {
                    return strval($id);
                })
                ->toArray() ?? [];
        $this->teachers = $this->subject
                ->teachers
                ->pluck('id')
                ->map(function ($id) {
                    return strval($id);
                })
                ->toArray() ?? [];
    }

    public function render()
    {
        return view('livewire.subjects.form');
    }

    public function getCreatesProperty()
    {
        return empty($this->subject->id);
    }

    public function getFacultiesListProperty()
    {
        return Faculty::orderBy('title')->get();
    }

    public function getTeachersListProperty()
    {
        return User::teachers()->orderBy('surname')->get();
    }

    public function save()
    {
        if ($this->creates) {
            $this->authorize('create', Subject::class);
            $msg = 'Το μάθημα δημιουργήθηκε με επιτυχία!';
        }
        else {
            $this->authorize('update', $this->subject);
            $msg = 'Το μάθημα ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $this->subject->save();
        $this->subject->faculties()->sync($this->faculties);
        $this->subject->teachers()->sync($this->teachers);

        flash_message('success', $msg);

        return redirect()->route('subjects.show', ['subject' => $this->subject]);
    }

    protected function rules()
    {
        return [
            'subject.title' => [
                'required',
                'string',
                'max:254'
            ],
            'faculties' => [
                'required',
                'array',
            ],
            'teachers' => [
                'required',
                'array',
            ],
        ];
    }
}
