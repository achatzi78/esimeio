<?php

namespace App\Http\Livewire\Roles;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var Role
     */
    public Role $role;

    /**
     * @var Collection
     */
    public Collection $permissions;

    /**
     * @var array
     */
    public array $role_permissions = [];

    public string $sync_users = 'N';

    public function mount(Role $role)
    {
        $this->role = $role;
        $this->role_permissions = $this->role->permissions->pluck('id')->toArray();
        $this->permissions = Permission::all();
    }

    public function render()
    {
        return view('livewire.roles.form');
    }

    public function getPermissionsListProperty()
    {
        return $this->permissions->sortBy('category')->groupBy('category');
    }

    public function getIsNewProperty()
    {
        return empty($this->role->id);
    }

    public function save()
    {
        if ($this->is_new) {
            $this->authorize('create', Role::class);
        }
        else {
            $this->authorize('update', $this->role);
        }

        $this->validate();

        $this->role->save();

        $old_permissions = $this->role->permissions->pluck('id');

        $this->role->permissions()->sync($this->role_permissions);
        $this->role->refresh();

        if ($this->is_new) {
            $this->flash('success', '', [
                'text' => 'Ο ρόλος δημιουργήθηκε με επιτυχία!',
            ], route('roles.edit', ['role' => $this->role]));
        }
        else {
            $new_permissions = $this->role->permissions->pluck('id');

            $removed_permissions = $old_permissions->diff($new_permissions);

            $added_permissions = $new_permissions->diff($old_permissions);

            switch ($this->sync_users) {
                case 'S':
                    $this->role->users->each(fn(User $user) => $user->permissions()->sync($this->role_permissions));
                    break;
                case 'U':
                    $this->role->users->each(function (User $user) use ($added_permissions, $removed_permissions) {
                        $user->permissions()->attach($added_permissions);
                        $user->permissions()->detach($removed_permissions);
                    });
                    break;
            }

            $this->reset('sync_users');

            $this->alert('success', '', [
                'text' => 'Ο ρόλος ενημερώθηκε με επιτυχία!',
            ]);
        }
    }

    protected function rules()
    {
        return [
            'role.title' => [
                'required',
                'string',
                'max:255'
            ],
            'role.hierarchy' => [
                'required',
                'integer',
            ],
            'role_permissions' => [
                'required',
                'array'
            ],
            'sync_users' => [
                'required',
                'string',
                Rule::in('N', 'S', 'U')
            ],
        ];
    }
}
