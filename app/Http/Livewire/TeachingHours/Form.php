<?php

namespace App\Http\Livewire\TeachingHours;

use App\Models\AcademicYear;
use App\Models\TeachingHour;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests;

    /**
     * @var int|null
     */
    public ?int $teaching_hour_id = null;

    /**
     * @var string|null
     */
    public ?string $start_time = null;

    /**
     * @var string|null
     */
    public ?string $end_time = null;

    /**
     * @var int|null
     */
    public ?int $academic_year_id = null;

    /**
     * @var Collection|AcademicYear[]
     */
    public array|Collection $academic_years;


    public function mount(?int $teaching_hour_id = null)
    {
        $this->teaching_hour_id = $teaching_hour_id;
        $this->academic_years = AcademicYear::all();

        $teaching_hour = TeachingHour::find($teaching_hour_id) ?? new TeachingHour();

        $this->start_time = $teaching_hour->starts_at;
        $this->end_time = $teaching_hour->ends_at;
        $this->academic_year_id = $teaching_hour->academic_year_id;
    }

    public function render()
    {
        return view('livewire.teaching-hours.form');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function getCreatesProperty()
    {
        return empty($this->teaching_hour_id);
    }

    public function save()
    {
        if ($this->creates) {
            $teaching_hour = new TeachingHour();

            $this->authorize('create', TeachingHour::class);
            $msg = 'Η διδακτική ώρα δημιουργήθηκε με επιτυχία!';
        }
        else {
            $teaching_hour = TeachingHour::find($this->teaching_hour_id);

            $this->authorize('update', $teaching_hour);
            $msg = 'Η διδακτική ώρα ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $teaching_hour->academic_year_id = $this->academic_year_id;
        $teaching_hour->start_time = $this->start_time;
        $teaching_hour->end_time = $this->end_time;

        $teaching_hour->save();

        flash_message('success', $msg);

        return redirect()->route('teaching-hours.show', ['teaching_hour' => $teaching_hour]);
    }

    protected function rules()
    {
        return [
            'academic_year_id' => [
                'required'
            ],
            'start_time' => [
                'bail',
                'required',
                'date_format:H:i',
                function ($attribute, $value, $fail) {
                    try {
                        $start_time = now()->setTimeFromTimeString($value);
                        $end_time = now()->setTimeFromTimeString($this->end_time ?? '');

                        if ($start_time->gt($end_time)) {
                            $fail(__('validation.before_or_equal', ['attribute' => __('validation.attributes.start_time'), 'date' => $this->end_time]));
                        }
                    }
                    catch (\Exception $e) {

                    }
                }
            ],
            'end_time' => [
                'bail',
                'required',
                'date_format:H:i',
                function ($attribute, $value, $fail) {
                    try {
                        $start_time = now()->setTimeFromTimeString($this->start_time);
                        $end_time = now()->setTimeFromTimeString($value);

                        if ($start_time->gt($end_time)) {
                            $fail(__('validation.after_or_equal', ['attribute' => __('validation.attributes.end_time'), 'date' => $this->start_time]));
                        }
                    }
                    catch (\Exception $e) {

                    }
                }
            ]
        ];
    }
}
