<?php

namespace App\Http\Livewire\TeachingHours;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\AcademicYear;
use App\Models\TeachingHour;
use Illuminate\Database\Eloquent\Collection;

class Index extends MasterIndex
{
    /**
     * @var Collection|AcademicYear[]
     */
    public array|Collection $academic_years;

    public function mount()
    {
        $this->academic_years = AcademicYear::all();
    }

    protected function model()
    {
        return TeachingHour::class;
    }

    protected function builder()
    {
        return $this->model()::with([
            'academic_year',
        ]);
    }

    protected function view()
    {
        return 'livewire.teaching-hours.index';
    }
}
