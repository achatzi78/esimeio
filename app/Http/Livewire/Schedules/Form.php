<?php

namespace App\Http\Livewire\Schedules;

use App\Models\AcademicYear;
use App\Models\Classroom;
use App\Models\Faculty;
use App\Models\Schedule;
use App\Models\ScheduleLine;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\TeachingHour;
use Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;
use Livewire\Component;

class Form extends Component
{
    protected $listeners = [
        'lineAdded' => 'addLine'
    ];

    /**
     * @var Schedule|null
     */
    public ?Schedule $schedule = null;

    /**
     * @var Collection|null
     */
    public ?Collection $schedule_lines = null;

    /**
     * @var Collection|null
     */
    public ?Collection $academic_years = null;

    public function mount(?Schedule $schedule = null)
    {
        $this->schedule = $schedule ?? new Schedule();
        $this->schedule_lines = $schedule->lines ?? collect();
        $this->academic_years = AcademicYear::orderBy('is_current', 'desc')->get();
    }

    public function render()
    {
        return view('livewire.schedules.form');
    }

    public function getLinesProperty()
    {
        return $this->schedule_lines->keyBy(function (ScheduleLine $schedule_line) {
            return "{$schedule_line->weekday}-{$schedule_line->teacher_id}-{$schedule_line->faculty_id}-{$schedule_line->teaching_hour_id}";
        });
    }

    public function addLine($line_data)
    {
        $line = new ScheduleLine($line_data);

        $this->schedule_lines->push($line);

        $this->emitTo('schedules.calendar', 'lineAdded', "{$line->weekday}-{$line->teacher_id}-{$line->faculty_id}-{$line->teaching_hour_id}", $line);
    }

    public function updated($name, $value)
    {
        //check if another schedule exists with the same data
        /*if (
            Schedule::whereAcademicYearId($this->schedule->academic_year_id)
            ->whereFacultyId($this->schedule->faculty_id)
            ->whereSubjectId($this->schedule->subject_id)
            ->whereTeacherId($this->schedule->teacher_id)
            ->whereTeachingHourId($this->schedule->teaching_hour_id)
            //->whereClassroomId($this->schedule->classroom_id)
            ->whereWeekDay($this->schedule->weekday)
            ->when(!empty($this->schedule->id), function($query) {
                return $query->where('id', '<>', $this->schedule->id);
            })
            ->exists()
        ) {
            flash_message('warning', 'Υπάρχει ήδη εγγραφή με αυτά τα στοιχεία');
        }*/
    }

    public function save()
    {
        $this->schedule->save();

        foreach ($this->schedule_lines as $schedule_line) {
            $schedule_line->schedule_id = $this->schedule->id;
            $schedule_line->save();
        }
    }

    public function hydrateScheduleLines($value)
    {
        foreach ($value as $key => $data) {
            $line = new ScheduleLine($data);

            $value->put($key, $line);
        }
    }

    protected function rules(): array
    {
        return [
            'schedule.title' => [
                'required',
                'string'
            ],
            'schedule.academic_year_id' => [
                'required',
            ],
        ];
    }
}
