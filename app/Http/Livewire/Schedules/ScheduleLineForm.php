<?php

namespace App\Http\Livewire\Schedules;

use App\Models\Classroom;
use App\Models\Faculty;
use App\Models\ScheduleLine;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\TeachingHour;
use App\Models\User;
use App\Models\UserAvailability;
use Arr;
use Illuminate\Support\Collection;
use Livewire\Component;

class ScheduleLineForm extends Component
{
    /**
     * @var ScheduleLine|null
     */
    public ?ScheduleLine $schedule_line = null;

    /**
     * @var int|null
     */
    public ?int $academic_year_id = null;

    public ?Collection $faculties = null;

    public ?Collection $classrooms = null;

    public function mount(ScheduleLine $schedule_line = new ScheduleLine, int $academic_year_id = null)
    {
        $this->schedule_line = $schedule_line;
        $this->academic_year_id = $academic_year_id;
        $this->faculties = Faculty::orderBy('title')->get();
        $this->classrooms = Classroom::orderBy('title')->get();
    }

    public function render()
    {
        return view('livewire.schedules.schedule-line-form');
    }

    public function getSubjectsProperty(): Collection
    {
        return Subject::whereHas('faculties', fn($query) => $query->where('faculties.id', $this->schedule_line->faculty_id))
            ->orderBy('title')
            ->get();
    }

    public function getTeachersProperty(): Collection
    {
        return User::teachers()
            ->whereHas('academic_years', fn($query) => $query->whereId($this->academic_year_id))
            ->whereHas('subjects', fn($query) => $query->whereId($this->schedule_line->subject_id))
            ->orderBy('surname')
            ->get();
    }

    public function getWeekdaysProperty(): Collection
    {
        return UserAvailability::whereAcademicYearId($this->academic_year_id)
            ->whereHas('user', fn($query) => $query->whereId($this->schedule_line->teacher_id))
            ->pluck('weekday')
            ->unique()
            ->sort()
            ->flip()
            ->map(fn($key, $day_num) => weekdays($day_num));
    }

    public function getTeachingHoursProperty(): Collection
    {
        return UserAvailability::with('teaching_hour')
            ->whereAcademicYearId($this->academic_year_id)
            ->whereHas('user', fn($query) => $query->whereId($this->schedule_line->teacher_id))
            ->get()
            ->pluck('teaching_hour')
            ->unique()
            ->sortBy('start_time');
    }

    public function updatedScheduleLineFacultyId($value)
    {
        $this->schedule_line->subject_id = null;
        $this->schedule_line->teacher_id = null;
        $this->schedule_line->weekday = null;
        $this->schedule_line->teaching_hour_id = null;
    }

    public function updatedScheduleLineSubjectId($value)
    {
        $this->schedule_line->teacher_id = null;
        $this->schedule_line->weekday = null;
        $this->schedule_line->teaching_hour_id = null;
    }

    public function updatedScheduleLineTeacherId($value)
    {
        $this->schedule_line->weekday = null;
        $this->schedule_line->teaching_hour_id = null;
    }

    public function updatedScheduleLineWeekday($value)
    {
        $this->schedule_line->teaching_hour_id = null;
    }

    public function addLine()
    {
        $this->emitTo('schedules.form', 'lineAdded', $this->schedule_line);
        $this->schedule_line = new ScheduleLine();
    }

    protected function rules(): array
    {
        return [
            'schedule_line.faculty_id' => [
                'required',
            ],
            'schedule_line.subject_id' => [
                'required',
            ],
            'schedule_line.teacher_id' => [
                'required',
            ],
            'schedule_line.weekday' => [
                'required',
            ],
            'schedule_line.teaching_hour_id' => [
                'required',
            ],
            'schedule_line.classroom_id' => [
                'required',
            ],
        ];
    }
}
