<?php

namespace App\Http\Livewire\Schedules;

use App\Models\Faculty;
use App\Models\ScheduleLine;
use App\Models\Teacher;
use App\Models\TeachingHour;
use Illuminate\Support\Collection;
use Livewire\Component;

class Calendar extends Component
{
    protected $listeners = [
        'lineAdded' => 'addLine'
    ];

    /**
     * @var Collection|null
     */
    public ?Collection $teachers = null;

    /**
     * @var Collection|null
     */
    public ?Collection $teaching_hours = null;

    /**
     * @var Collection|null
     */
    public ?Collection $faculties = null;

    /**
     * @var int|null
     */
    public ?int $academic_year_id = null;

    /**
     * @var Collection|null
     */
    public ?Collection $lines = null;

    public function mount(?int $academic_year_id = null, ?Collection $lines = null)
    {
        $this->loadData($academic_year_id);

        $this->lines = $lines ?? collect();
    }

    public function render()
    {
        return view('livewire.schedules.calendar');
    }

    public function loadData($academic_year_id = null)
    {
        if (empty($academic_year_id)) {
            $this->teachers = collect();
            $this->teaching_hours = collect();
            $this->faculties = collect();
            $this->lines = collect();
        }
        else {
            $this->academic_year_id = $academic_year_id;

            $this->teaching_hours = TeachingHour::whereAcademicYearId($this->academic_year_id)->get();
            $this->faculties = Faculty::orderBy('title')->get();
            $this->teachers = Teacher::whereHas('academic_years', function ($query) {
                return $query->whereId($this->academic_year_id);
            })->orderBy('surname')->get();
        }
    }

    public function hydrateLines($value)
    {
        foreach ($value as $key => $data) {
            $line = new ScheduleLine($data);

            $value->put($key, $line);
        }
    }

    public function addLine($key, $line_data)
    {
        $line = new ScheduleLine($line_data);

        $this->lines->put($key, $line);
    }
}
