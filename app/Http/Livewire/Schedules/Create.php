<?php

namespace App\Http\Livewire\Schedules;

use App\Models\Schedule;
use Livewire\Component;

class Create extends Component
{
    /**
     * @var Schedule
     */
    public Schedule $schedule;

    public function mount()
    {
        $this->schedule = new Schedule();
    }

    public function render()
    {
        return view('livewire.schedules.create')
            ->extends('layouts.app')
            ->section('content');
    }
}
