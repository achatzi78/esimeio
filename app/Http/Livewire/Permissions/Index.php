<?php

namespace App\Http\Livewire\Permissions;

use App\Models\Permission;
use App\Http\Livewire\Index as MasterIndex;

class Index extends MasterIndex
{
    public function getCategoriesProperty(): iterable
    {
        return Permission::categories();
    }

    protected function model()
    {
        return Permission::class;
    }

    protected function view()
    {
        return 'livewire.permissions.index';
    }
}
