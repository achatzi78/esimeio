<?php

namespace App\Http\Livewire\Permissions;

use App\Models\Permission;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var Permission
     */
    public Permission $permission;

    public function mount(?Permission $permission = null)
    {
        $this->permission = $permission ?? new Permission();
    }

    public function render()
    {
        return view('livewire.permissions.form');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function getCategoriesProperty(): iterable
    {
        return Permission::categories();
    }

    public function getIsNewProperty()
    {
        return empty($this->permission->id);
    }

    public function save()
    {
        if ($this->is_new) {
            $this->authorize('create', Permission::class);
            $msg = 'Το δικαίωμα δημιουργήθηκε με επιτυχία!';
        }
        else {
            $this->authorize('update', $this->permission);
            $msg = 'Το δικαίωμα ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $this->permission->save();

        $this->flash('success', '', [
            'text' => $msg,
        ], route('permissions.show', ['permission' => $this->permission]));
    }

    protected function rules()
    {
        return [
            'permission.title' => [
                'required',
                'string',
                'max:255'
            ],
            'permission.category' => [
                'required',
                'string',
                'max:255',
            ],
            'permission.description' => [
                'nullable',
                'string'
            ],
            'permission.action' => [
                'required',
                'string',
                'max:255',
                Rule::unique('permissions', 'action')->ignore($this->permission->id)
            ],
        ];
    }
}
