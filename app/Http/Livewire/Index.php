<?php

namespace App\Http\Livewire;

use App\Concerns\Livewire\WithFilters;
use App\Concerns\Livewire\WithSorting;
use Arr;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AuthorizesRequests, LivewireAlert, WithPagination, WithFilters, WithSorting;

    protected string $paginationTheme = 'bootstrap';

    public function render()
    {
        $filters = $this->getFilters();

        return view($this->view(), [
            'rows' => $this->builder()->filter($filters)->paginate()
        ]);
    }

    public function updatedFilters($propertyValue, $propertyName)
    {
        if (is_array($propertyValue)) {
            array_walk_recursive($propertyValue, 'trim');
        }
        else {
            $propertyValue = trim($propertyValue);
        }

        Arr::set($this->filters, $propertyName, $propertyValue);

        if ($propertyValue == '') {
            Arr::forget($this->filters, $propertyName);
        }
    }

    public function clearFilters()
    {
        $this->reset('filters', 'sort_by', 'sort_direction');
        $this->resetPage();
    }

    public function remove($model_id)
    {
        $model = $this->model()::find($model_id);

        $this->authorize('delete', $model);

        $model->delete();

        $this->alert('success', '', [
            'text' => 'Επιτυχής Διαγραφή',
        ]);

        $this->resetPage();
    }

    protected function builder()
    {
        return $this->model()::query();
    }

    protected function model() {}

    protected function view() {}
}
