<?php

namespace App\Http\Livewire\Faculties;

use App\Models\Faculty;
use App\Models\Subject;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var Faculty
     */
    public Faculty $faculty;

    /**
     * @var array
     */
    public array $subjects = [];

    public Collection $subjects_list;

    public function mount(?Faculty $faculty = new Faculty)
    {
        $this->faculty = $faculty;
        $this->subjects = $this->faculty
                ->subjects
                ->pluck('id')
                ->map(function ($id) {
                    return strval($id);
                })
                ->toArray() ?? [];
        $this->subjects_list = Subject::orderBy('title')->get();
    }

    public function render()
    {
        return view('livewire.faculties.form');
    }

    public function getIsNewProperty()
    {
        return empty($this->faculty->id);
    }

    public function save()
    {
        if ($this->is_new) {
            $this->authorize('create', Faculty::class);
            $msg = 'Το τμήμα δημιουργήθηκε με επιτυχία!';
        }
        else {
            $this->authorize('update', $this->faculty);
            $msg = 'Το τμήμα ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $this->faculty->save();
        $this->faculty->subjects()->sync($this->subjects);

        $this->flash('success', '', [
            'text' => $msg,
        ], route('faculties.show', ['faculty' => $this->faculty]));
    }

    protected function rules()
    {
        return [
            'faculty.title' => [
                'required',
                'string',
                'max:254'
            ],
            'subjects' => [
                'required',
                'array',
            ],
        ];
    }
}
