<?php

namespace App\Http\Livewire\Faculties;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\Faculty;

class Index extends MasterIndex
{
    protected function model()
    {
        return Faculty::class;
    }

    protected function view()
    {
        return 'livewire.faculties.index';
    }
}
