<?php

namespace App\Http\Livewire\Classrooms;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\Classroom;

class Index extends MasterIndex
{
    protected function model()
    {
        return Classroom::class;
    }

    protected function view()
    {
        return 'livewire.classrooms.index';
    }
}
