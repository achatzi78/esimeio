<?php

namespace App\Http\Livewire\Classrooms;

use App\Models\Classroom;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var Classroom
     */
    public Classroom $classroom;

    public function mount(?Classroom $classroom = new Classroom)
    {
        $this->classroom = $classroom;
    }

    public function render()
    {
        return view('livewire.classrooms.form');
    }

    public function getIsNewProperty()
    {
        return empty($this->classroom->id);
    }

    public function save()
    {
        if ($this->is_new) {
            $this->authorize('create', Classroom::class);
            $msg = 'Η αίθουσα δημιουργήθηκε με επιτυχία!';
        }
        else {
            $this->authorize('update', $this->classroom);
            $msg = 'Η αίθουσα ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $this->classroom->save();

        $this->flash('success', '', [
            'text' => $msg,
        ], route('classrooms.show', ['classroom' => $this->classroom]));
    }

    protected function rules()
    {
        return [
            'classroom.title' => [
                'required',
                'string',
                'max:254'
            ],
        ];
    }
}
