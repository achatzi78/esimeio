<?php

namespace App\Http\Livewire\AcademicYears;

use App\Models\AcademicYear;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var AcademicYear
     */
    public AcademicYear $academic_year;

    /**
     * @var array
     */
    public array $months = [];

    public bool $current_year_conficts = false;

    public function mount(?AcademicYear $academic_year = new AcademicYear)
    {
        $this->academic_year = $academic_year;
        $this->months = $this->academic_year->months;
    }

    public function render()
    {
        return view('livewire.academic-years.form');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatedAcademicYearStartYear($value)
    {
        $this->resetValidation([
            'academic_year.start_year',
            'academic_year.end_year',
        ]);
        $this->validateOnly('academic_year.start_year');
        $this->validateOnly('academic_year.end_year');
    }

    public function updatedAcademicYearEndYear($value)
    {
        $this->resetValidation([
            'academic_year.start_year',
            'academic_year.end_year',
        ]);
        $this->validateOnly('academic_year.start_year');
        $this->validateOnly('academic_year.end_year');
    }

    public function updatedAcademicYearIsCurrent($value)
    {
        if (empty($value)) {
            $this->reset('current_year_conficts');

            return;
        }

        $this->current_year_conficts = AcademicYear::whereIsCurrent(1)->where('id', '<>', $this->academic_year->id)->exists();
    }

    public function getIsNewProperty()
    {
        return empty($this->academic_year->id);
    }

    public function save()
    {
        if ($this->is_new) {
            $this->authorize('create', AcademicYear::class);
            $msg = 'Το ακαδημαϊκό έτος δημιουργήθηκε με επιτυχία!';
        }
        else {
            $this->authorize('update', $this->academic_year);
            $msg = 'Το ακαδημαϊκό έτος ενημερώθηκε με επιτυχία!';
        }

        $this->validate();

        $this->months = array_map('intval', $this->months);
        sort($this->months);

        $this->academic_year->months = $this->months;

        $this->academic_year->save();

        $this->alert(
            options: [
                'text' => $msg,
            ]
        );

        if ($this->is_new) {
            $this->reset('months', 'current_year_conficts');
            $this->academic_year = new AcademicYear();
        }
    }

    protected function rules()
    {
        return [
            'academic_year.title' => [
                'required',
                'string',
                'max:254'
            ],
            'academic_year.start_year' => [
                'required',
                'integer',
                'min:2020',
                'lte:academic_year.end_year',
            ],
            'academic_year.end_year' => [
                'required',
                'integer',
                'min:2020',
                'gte:academic_year.start_year',
            ],
            'academic_year.is_current' => [
                'required',
                'boolean',
                function ($attribute, $value, $fail) {
                    if (!boolval($value)) {
                        $other_current_year = AcademicYear::whereIsCurrent(1)->where('id', '<>', $this->academic_year->id)->exists();

                        if (!$other_current_year) {
                            $fail('Πρέπει να υπάρχει τουλάχιστον ένα τρέχων ακαδημαϊκό έτος');
                        }
                    }
                }
            ],
            'months' => [
                'required',
                'array',
                'max:12'
            ],
        ];
    }
}
