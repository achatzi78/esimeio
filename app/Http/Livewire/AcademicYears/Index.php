<?php

namespace App\Http\Livewire\AcademicYears;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\AcademicYear;

class Index extends MasterIndex
{
    protected function model()
    {
        return AcademicYear::class;
    }

    protected function view()
    {
        return 'livewire.academic-years.index';
    }
}
