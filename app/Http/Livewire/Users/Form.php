<?php

namespace App\Http\Livewire\Users;

use App\Models\AcademicYear;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Subject;
use App\Models\TeachingHour;
use App\Models\User;
use App\Models\UserAvailability;
use Hash;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Str;

class Form extends Component
{
    use AuthorizesRequests, LivewireAlert;

    /**
     * @var int|null
     */
    public ?int $user_id = null;

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var string|null
     */
    public ?string $surname = null;

    /**
     * @var int|null
     */
    public ?int $role_id = null;

    /**
     * @var string|null
     */
    public ?string $email = null;

    /**
     * @var string|null
     */
    public ?string $password = null;

    /**
     * @var bool|int|null
     */
    public bool|int|null $can_login = null;

    /**
     * @var array
     */
    public array $permissions = [];

    /**
     * @var array
     */
    public array $academic_years = [];

    /**
     * @var array
     */
    public array $availabilities = [];

    /**
     * @var array
     */
    public array $subjects = [];

    /**
     * @var array
     */
    public array $additional_data = [];

    /**
     * @var ?string
     */
    public ?string $password_confirmation = null;

    public function mount(?int $user_id = null)
    {
        $this->user_id = $user_id;

        $this->role_id = $this->user->role_id;
        $this->name = $this->user->name;
        $this->surname = $this->user->surname;
        $this->email = $this->user->email;
        $this->can_login = $this->user->can_login;

        if (!$this->creates) {
            $this->permissions = $this->user
                    ->permissions
                    ->pluck('id')
                    ->map(function ($id) {
                        return strval($id);
                    })
                    ->toArray() ?? [];

            $this->academic_years = $this->user
                    ->academic_years
                    ->pluck('id')
                    ->map(function ($id) {
                        return strval($id);
                    })
                    ->toArray() ?? [];

            $this->availabilities = $this->user->availabilities
                    ->pluck('id')
                    ->map(function ($id) {
                        return strval($id);
                    })
                    ->toArray() ?? [];

            $this->subjects = $this->user
                    ->subjects
                    ->pluck('id')
                    ->map(function ($id) {
                        return strval($id);
                    })
                    ->toArray() ?? [];

            $this->additional_data = $this->user->additional_data;
        }
    }

    public function render()
    {
        return view('livewire.users.form');
    }

    public function save()
    {
        if (empty($this->user_id)) {
            $user = new User();

            $this->authorize('create', User::class);
        }
        else {
            $user = User::find($this->user_id);

            $this->authorize('update', $user);
        }

        try {
            $this->validate();
        }
        catch (ValidationException $e) {
            $this->alert(
                type: 'error',
                message: 'Σφάλμα Καταχώρησης',
                options: [
                    'text' => 'Παρακαλώ ελέγξτε τα στοιχεία που καταχωρήσατε στην φόρμα',
                    'timer' => 4000
                ]
            );

            throw $e;
        }

        $user->id = $this->user_id;
        $user->role_id = $this->role_id;
        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->email = $this->email;
        $user->can_login = $this->can_login;
        $user->additional_data = $this->additional_data;

        if (!empty($this->password)) {
            $user->password = Hash::make($this->password);
        }

        $user->save();

        if (!$user->is_developer && !$user->is_admin) {
            $user->academic_years()->sync($this->academic_years);
            $user->availabilities()->sync($this->availabilities);
        }

        $user->permissions()->sync($this->permissions);
        $user->subjects()->sync($this->subjects);

        $msg = 'Ο χρήστης ενημερώθηκε με επιτυχία!';

        if ($this->creates) {
            $msg = 'Ο χρήστης δημιουργήθηκε με επιτυχία!';
        }

        flash_message('success', $msg);

        return redirect()->route('users.show', ['user' => $user]);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatedRoleId($value)
    {
        $current_user_role_id = User::whereId($this->user->id)->value('role_id');

        if ($value == $current_user_role_id) {
            $this->permissions = $this->user->permissions->pluck('id')->toArray();
        }
        else {
            $this->permissions = Permission::whereHas('roles', function ($query) use ($value) {
                return $query->whereId($value);
            })->pluck('id')->toArray();
        }
    }

    public function getCreatesProperty()
    {
        return empty($this->user_id);
    }

    public function getIsDeveloperProperty()
    {
        return $this->role_id == ROLE_DEVELOPER;
    }

    public function getIsAdminProperty()
    {
        return $this->role_id == ROLE_ADMIN;
    }

    public function getIsTeacherProperty()
    {
        return $this->role_id == ROLE_TEACHER;
    }

    public function getIsStudentProperty()
    {
        return $this->role_id == ROLE_STUDENT;
    }

    public function getUserProperty()
    {
        $user = User::find($this->user_id) ?? new User();

        $user->loadMissing([
            'role.permissions',
            'permissions',
            'academic_years',
            'subjects',
        ]);

        return $user;
    }

    public function getRolesListProperty()
    {
        return Role::lower_equal()->get();
    }

    public function getPermissionsListProperty()
    {
        return Permission::all();
    }

    public function getAcademicYearsListProperty()
    {
        return AcademicYear::with('teaching_hours')
            ->orderBy('is_current', 'desc')
            ->get();
    }

    public function getSubjectsListProperty()
    {
        return Subject::orderBy('title')->get();
    }

    public function getAvailabilitiesListProperty()
    {
        return UserAvailability::with([
                'academic_year',
                'teaching_hour',
            ])
            ->orderBy('academic_year_id')
            ->orderBy('teaching_hour_id')
            ->orderBy('weekday')
            ->get()
            ->groupBy(['academic_year_id', function ($user_availability) {
                return $user_availability->teaching_hour_id;
            }], true);
    }

    protected function rules()
    {
        $rules = [
            'name' => [
                'required',
                'string',
                'max:255'
            ],
            'surname' => [
                'required',
                'string',
                'max:255'
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users,email'
            ],
            'password' => [
                'nullable',
                Rule::requiredIf($this->creates),
                Password::min(6)
                    ->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
                    ->uncompromised(),
                'confirmed'
            ],
            'can_login' => [
                'required',
                'boolean'
            ],
            'role_id' => [
                'required'
            ],
            'additional_data.phones.home' => [
                'nullable',
                'phone:GR'
            ],
            'additional_data.phones.work' => [
                'nullable',
                'phone:GR'
            ],
            'additional_data.phones.mobile' => [
                'nullable',
                'phone:GR'
            ],
            'permissions' => [
                'required',
                'array',
            ],
            'academic_years' => [
                'nullable',
                'array',
                Rule::requiredIf($this->is_teacher || $this->is_student),
            ],
            'subjects' => [
                'nullable',
                'array',
                Rule::requiredIf($this->is_teacher),
            ],
            'availabilities' => [
                'nullable',
                'array',
            ],
        ];

        if (!$this->creates) {
            $rules['email'][4] = Rule::unique('users', 'email')->ignore($this->user_id);
        }

        return $rules;
    }
}
