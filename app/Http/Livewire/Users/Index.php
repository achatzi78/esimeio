<?php

namespace App\Http\Livewire\Users;

use App\Http\Livewire\Index as MasterIndex;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class Index extends MasterIndex
{
    /**
     * @var Collection|Role[]
     */
    public array|Collection $roles;

    public function mount()
    {
        $this->roles = Role::all();
    }

    protected function model()
    {
        return User::class;
    }

    protected function builder()
    {
        return $this->model()::with([
            'role',
            'academic_years',
            'subjects',
        ]);
    }

    protected function view()
    {
        return 'livewire.users.index';
    }
}
