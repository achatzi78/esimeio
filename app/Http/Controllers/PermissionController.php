<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permissions\CreatePermissionRequest;
use App\Http\Requests\Permissions\UpdatePermissionRequest;
use App\Jobs\Permissions\CreatePermission;
use App\Jobs\Permissions\UpdatePermission;
use App\Models\Permission;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', Permission::class);

        $rows = Permission::filter()->paginate();

        return view('permissions.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Permission::class);

        return view('permissions.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Permission $permission
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(Permission $permission): View
    {
        $this->authorize('view', $permission);

        return view('permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Permission $permission
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Permission $permission): View
    {
        $this->authorize('view', $permission);

        return view('permissions.edit', compact('permission'));
    }
}
