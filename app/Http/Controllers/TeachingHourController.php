<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeachingHours\CreateTeachingHourRequest;
use App\Http\Requests\TeachingHours\UpdateTeachingHourRequest;
use App\Jobs\TeachingHours\CreateTeachingHour;
use App\Jobs\TeachingHours\UpdateTeachingHour;
use App\Models\AcademicYear;
use App\Models\TeachingHour;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TeachingHourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', TeachingHour::class);

        return view('teaching-hours.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', TeachingHour::class);

        return view('teaching-hours.create');
    }

    /**
     * Display the specified resource.
     *
     * @param TeachingHour $teaching_hour
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(TeachingHour $teaching_hour): View
    {
        $this->authorize('view', $teaching_hour);

        return view('teaching-hours.show', compact('teaching_hour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TeachingHour $teaching_hour
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(TeachingHour $teaching_hour): View
    {
        $this->authorize('update', $teaching_hour);

        return view('teaching-hours.edit', compact('teaching_hour'));
    }
}
