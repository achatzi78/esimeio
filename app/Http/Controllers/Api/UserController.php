<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Users\UserResource;
use App\Http\Resources\Users\UserResourceCollection;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return UserResourceCollection
     * @throws AuthorizationException
     */
    public function index(): UserResourceCollection
    {
        $this->authorize('viewAny', User::class);

        return new UserResourceCollection(
            User::filter(request('filters', []))
            ->search(request('search', []))
            ->paginate()
        );
    }
}
