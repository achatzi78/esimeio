<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\View\View;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', Subject::class);

        return view('subjects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Subject::class);

        return view('subjects.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Subject $subject
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(Subject $subject): View
    {
        $this->authorize('view', $subject);

        return view('subjects.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Subject $subject
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Subject $subject): View
    {
        $this->authorize('update', $subject);

        return view('subjects.edit', compact('subject'));
    }
}
