<?php

namespace App\Http\Controllers;

use App\Models\TeachingHour;
use App\Models\User;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
