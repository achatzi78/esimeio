<?php

namespace App\Http\Controllers;

use App\Http\Requests\AcademicYears\CreateAcademicYearRequest;
use App\Http\Requests\AcademicYears\UpdateAcademicYearRequest;
use App\Jobs\AcademicYears\CreateAcademicYear;
use App\Jobs\AcademicYears\UpdateAcademicYear;
use App\Models\AcademicYear;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', AcademicYear::class);

        return view('academic-years.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', AcademicYear::class);

        return view('academic-years.create');
    }

    /**
     * Display the specified resource.
     *
     * @param AcademicYear $academic_year
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(AcademicYear $academic_year): View
    {
        $this->authorize('view', $academic_year);

        return view('academic-years.show', compact('academic_year'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AcademicYear $academic_year
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(AcademicYear $academic_year): View
    {
        $this->authorize('update', $academic_year);

        return view('academic-years.edit', compact('academic_year'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AcademicYear $academic_year
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(AcademicYear $academic_year): RedirectResponse
    {
        $this->authorize('delete', $academic_year);

        $academic_year->delete();

        flash_message('success', "Το ακαδημαϊκό έτος {$academic_year->title} διαγράφηκε με επιτυχία!");

        return redirect()->route('academic-years.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AcademicYear $academic_year
     *
     * @return RedirectResponse
     * @throws Exception
     */
    public function change(AcademicYear $academic_year): RedirectResponse
    {
        cache()->forever('users:' . auth()->id() . ':academic-year', $academic_year);

        flash_message('success', "Το ακαδημαϊκό έτος άλλαξε στο {$academic_year->title}");

        return back();
    }
}
