<?php

namespace App\Http\Controllers;

use App\Http\Requests\Classrooms\CreateClassroomRequest;
use App\Http\Requests\Classrooms\UpdateClassroomRequest;
use App\Jobs\Classrooms\CreateClassroom;
use App\Jobs\Classrooms\UpdateClassroom;
use App\Models\Classroom;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', Classroom::class);

        return view('classrooms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Classroom::class);

        return view('classrooms.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Classroom $classroom
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(Classroom $classroom): View
    {
        $this->authorize('view', $classroom);

        return view('classrooms.show', compact('classroom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Classroom $classroom
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Classroom $classroom): View
    {
        $this->authorize('update', $classroom);

        return view('classrooms.edit', compact('classroom'));
    }
}
