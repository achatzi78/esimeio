<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\CreateRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Jobs\Roles\CreateRole;
use App\Jobs\Roles\UpdateRole;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', Role::class);

        $roles = Role::orderBy('hierarchy')->get();
        $selected_role = new Role();

        return view('roles.index', compact('roles', 'selected_role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Role $role): View
    {
        $this->authorize('update', $role);

        $roles = Role::orderBy('hierarchy')->get();
        $selected_role = $role;

        return view('roles.edit', compact('roles', 'selected_role'));
    }
}
