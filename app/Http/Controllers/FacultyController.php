<?php

namespace App\Http\Controllers;

use App\Http\Requests\Faculties\CreateFacultyRequest;
use App\Http\Requests\Faculties\UpdateFacultyRequest;
use App\Jobs\Faculties\CreateFaculty;
use App\Jobs\Faculties\UpdateFaculty;
use App\Models\Faculty;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('viewAny', Faculty::class);

        return view('faculties.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Faculty::class);

        return view('faculties.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Faculty $faculty
     *
     * @return View
     * @throws AuthorizationException
     */
    public function show(Faculty $faculty): View
    {
        $this->authorize('view', $faculty);

        return view('faculties.show', compact('faculty'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Faculty $faculty
     *
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Faculty $faculty): View
    {
        $this->authorize('update', $faculty);

        return view('faculties.edit', compact('faculty'));
    }
}
