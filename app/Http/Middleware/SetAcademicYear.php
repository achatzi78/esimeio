<?php

namespace App\Http\Middleware;

use App\Models\AcademicYear;
use Closure;
use Illuminate\Http\Request;

class SetAcademicYear
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            cache()->rememberForever('users:' . auth()->id() . ':academic-year', function () {
                return AcademicYear::whereIsCurrent(1)->first();
            });
        }

        return $next($request);
    }
}
