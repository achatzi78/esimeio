<?php

namespace App\Concerns;

use Illuminate\Database\Eloquent\Builder;
use Arr;

trait IsOrdered
{
    public function scopeSortBy(Builder $builder, array $parameters, $default_column = null, $default_direction = null)
    {
        $default_column ??= $this->getKeyName();
        $default_direction ??= 'asc';
        $column = Arr::get($parameters, 'order_by') ?? $default_column;
        $direction = Arr::get($parameters, 'sort') ?? $default_direction;

        return $builder->when($column && $this->columns()->contains($column), function ($q) use ($column, $direction) {
            return $q->orderBy($column, $direction);
        }, function ($q) use ($default_column, $default_direction) {
            return $q->orderBy($default_column, $default_direction);
        });
    }
}
