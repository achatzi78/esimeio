<?php

namespace App\Concerns;

use Illuminate\Support\Collection;

trait WithDefaultCasting
{
    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if (!is_null($value)) {
            return parent::castAttribute($key, $value);
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return 0;
            case 'real':
            case 'float':
            case 'double':
                return (float) 0;
            case 'string':
                return '';
            case 'bool':
            case 'boolean':
                return false;
            case 'object':
            case 'array':
            case 'json':

                return [];
            case 'collection':
                return new Collection();
            /*case 'date':
                return $this->asDate('0000-00-00');
            case 'datetime':
                return $this->asDateTime('0000-00-00');
            case 'timestamp':
                return $this->asTimestamp('0000-00-00');*/
            default:
                return $value;
        }
    }
}
