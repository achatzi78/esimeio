<?php

namespace App\Concerns;

use App\Models\Permission;
use Str;

trait HasPermissions
{
    public function permissions()
    {
        return $this->morphToMany(Permission::class, 'permissionable');
    }

    /**
     * Determine if the user has a permission
     *
     * @param string $action
     *
     * @return bool
     */
    public function is_allowed_to(string $action): bool
    {
        if ($this->is_developer) {
            return true;
        }

        return $this->permissions->contains('action', $action);
    }
}
