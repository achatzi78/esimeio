<?php

namespace App\Concerns\Enums;

trait Arrayable
{
    public static function toArray(): array
    {
        $array = [];

        foreach (array_column(self::cases(), 'value') as $value) {
            $enum = self::from($value);
            $description = $enum->name;

            if (method_exists($enum, 'description')) {
                $description = $enum->description();
            }

            $array[$enum->value] = $description;
        }

        return $array;
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
