<?php

namespace App\Concerns\Livewire;

use Livewire\Exceptions\PropertyNotFoundException;

trait WithFilters
{
    /**
     * @var array
     */
    public array $filters = [];

    protected $queryStringWithFilters = [
        'filters' => ['except' => []]
    ];

    protected function getFilters(): array
    {
        try {
            $order_by = $this->sort_by;
        }
        catch (PropertyNotFoundException $e) {
            $order_by = null;
        }

        try {
            $sort = $this->sort_direction;
        }
        catch (PropertyNotFoundException $e) {
            $sort = null;
        }

        return array_merge($this->filters, ['order_by' => $order_by, 'sort' => $sort]);
    }

    public function updatingFilters()
    {
        $this->resetPage();
    }
}
