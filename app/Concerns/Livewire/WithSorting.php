<?php

namespace App\Concerns\Livewire;

trait WithSorting
{
    /**
     * @var string
     */
    public string $sort_by = '';

    /**
     * @var string
     */
    public string $sort_direction = '';

    protected $queryStringWithSorting = [
        'sort_by' => ['except' => ''],
        'sort_direction' => ['except' => ''],
    ];

    public function sortBy($field)
    {
        $this->sort_direction = ($this->sort_by === $field) ? $this->reverseSort() : 'asc';

        $this->sort_by = $field;
    }

    public function reverseSort(): string
    {
        return $this->sort_direction === 'asc' ? 'desc' : 'asc';
    }
}
