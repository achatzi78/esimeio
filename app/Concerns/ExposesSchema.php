<?php

namespace App\Concerns;

use Illuminate\Support\Collection;

trait ExposesSchema
{
    protected function columns(): Collection
    {
        return cache()->remember('schemas.' . $this->getTable(), 60 * 60 * 24, function () {
            return collect(
                app('db')
                ->connection($this->getConnectionName())
                ->getSchemaBuilder()
                ->getColumnListing($this->getTable())
            );
        });
    }
}