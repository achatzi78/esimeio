<?php

namespace App\Concerns\Filters;

use Arr;
use Illuminate\Database\Eloquent\Builder;

trait WithTitleFilter
{
    /**
     * @param Builder $builder
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeTitleFilter(Builder $builder, array $filters = []): Builder
    {
        return $builder->when(Arr::get($filters, 'title'), fn($query) => $query->where('title', 'like', '%' . $filters['title'] . '%'));
    }
}
