<?php

namespace App\Concerns\Filters;

use Arr;
use Illuminate\Database\Eloquent\Builder;

trait WithBooleanFilter
{
    /**
     * @param Builder $builder
     * @param string $column
     * @param string $filter_key
     * @param array $filters
     *
     * @return Builder
     */
    public function scopeBooleanFilter(Builder $builder, string $column, string $filter_key, array $filters = []): Builder
    {
        return $builder->when(strval(Arr::get($filters, $filter_key)) != '', fn($query) => $query->where($column, intval(Arr::get($filters, $filter_key))));
    }
}
