<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class LaravelCollectiveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::macro('boolean_select', function($name, $selected = null, $selectAttributes = array(), $optionsAttributes = array(), $optgroupsAttributes = array()) {
            $list = [
                1 => 'Ναι',
                0 => 'Όχι',
            ];

            return Form::select($name, $list, $selected, $selectAttributes, $optionsAttributes, $optgroupsAttributes);
        });
    }
}
