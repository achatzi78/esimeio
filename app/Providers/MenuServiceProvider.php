<?php

namespace App\Providers;

use App\Models\AcademicYear;
use App\Models\Admin;
use App\Models\Classroom;
use App\Models\Developer;
use App\Models\Faculty;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\TeachingHour;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Link;
use Menu;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Menu::macro('navbar', function () {
            $primary_data_menu = Menu::new()
                ->setWrapperTag('ul')
                ->addClass('dropdown-menu')
                ->addParentClass('dropdown')
                ->addIfCan(['viewAny', Role::class], Link::toRoute('roles.index', 'Ρόλοι')->addClass('dropdown-item'))
                ->addIfCan(['viewAny', Permission::class], Link::toRoute('permissions.index', 'Δικαιώματα')->addClass('dropdown-item'))
                ->add(Html::raw('<hr class="dropdown-divider">'))
                ->addIfCan(['viewAny', AcademicYear::class], Link::toRoute('academic-years.index', 'Ακαδημαϊκά Έτη')->addClass('dropdown-item'))
                ->addIfCan(['viewAny', Classroom::class], Link::toRoute('classrooms.index', 'Αίθουσες')->addClass('dropdown-item'))
                ->addIfCan(['viewAny', Faculty::class], Link::toRoute('faculties.index', 'Τμήματα')->addClass('dropdown-item'))
                ->addIfCan(['viewAny', TeachingHour::class], Link::toRoute('teaching-hours.index', 'Διδακτικές Ώρες')->addClass('dropdown-item'))
                ->addIfCan(['viewAny', Subject::class], Link::toRoute('subjects.index', 'Μαθήματα')->addClass('dropdown-item'))
            ;


            return Menu::new()
                ->addClass('navbar-nav me-auto')
                ->addItemParentClass('nav-item')
                ->add(Link::toRoute('home', '<i class="fas fa-home"></i> Αρχική')->addClass('nav-link'))
                ->submenuIf(
                    $primary_data_menu->count() > 0,
                    Link::to('#', '<i class="fas fa-database"></i> Πρωτογενή Δεδομένα <span class="caret"></span>')
                        ->addClass('nav-link dropdown-toggle')
                        ->setAttributes([
                            'data-bs-toggle' => 'dropdown',
                            'role' => 'button',
                            'aria-haspopup' => 'true',
                            'aria-expanded' => 'false'
                        ]),
                    $primary_data_menu
                )
                ->addIfCan(['viewAny', User::class], Link::toRoute('users.index', '<i class="fas fa-users"></i> Χρήστες')->addClass('nav-link'))
                ->setActiveClassOnLink()
                ->setActiveFromRequest();
        });
    }
}
