<?php

namespace App\Providers;

use App\Models\AcademicYear;
use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('partials.navbar', function ($view) {
            $current_academic_year = cache()->rememberForever('users:' . auth()->id() . ':academic-year', function () {
                return AcademicYear::whereIsCurrent(1)->first();
            });

            $academic_years = AcademicYear::orderBy('is_current', 'desc')
                ->where('id', '<>', $current_academic_year->id)
                ->orderBy('start_year', 'desc')
                ->limit(3)
                ->get();

            $view->with([
                'navbar_academic_years' => $academic_years,
                'navbar_current_academic_year' => $current_academic_year,
            ]);
        });
    }
}
