<?php

namespace App\Providers;

use App\Models\AcademicYear;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Password::defaults(function () {
            $rule = Password::min(6);

            if ($this->app->isProduction()) {
                $rule->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols();
            }

            return $rule;
        });

        Carbon::setLocale(app()->getLocale());

        cache()->rememberForever('current_academic-year', function () {
            return AcademicYear::whereIsCurrent(1)->first();
        });
    }
}
