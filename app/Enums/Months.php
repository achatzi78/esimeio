<?php

namespace App\Enums;

use App\Concerns\Enums\Arrayable;

enum Months: int
{
    use Arrayable;

    case January = 1;
    case February = 2;
    case March = 3;
    case April = 4;
    case May = 5;
    case June = 6;
    case July = 7;
    case August = 8;
    case September = 9;
    case October = 10;
    case November = 11;
    case December = 12;

    public function description()
    {
        return match ($this) {
            self::January => 'Ιανουάριος',
            self::February => 'Φεβρουάριος',
            self::March => 'Μάρτιος',
            self::April => 'Απρίλιος',
            self::May => 'Μάϊος',
            self::June => 'Ιούνιος',
            self::July => 'Ιούλιος',
            self::August => 'Αύγουστος',
            self::September => 'Σεπτέμβριος',
            self::October => 'Οκτώβριος',
            self::November => 'Νοέμβριος',
            self::December => 'Δεκέμβριος',
        };
    }
}
