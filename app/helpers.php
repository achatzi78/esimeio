<?php

use Illuminate\Support\Carbon;

const ROLE_DEVELOPER = 1;
const ROLE_ADMIN = 2;
const ROLE_SECRETARY = 3;
const ROLE_RECEPTION = 4;
const ROLE_TEACHER = 5;
const ROLE_STUDENT = 6;
const ROLE_PARENT = 7;

if (!function_exists('file_from_cloud')) {
    function file_from_cloud(string $path)
    {
        return cache()->remember('files:' . Str::slug($path), 60 * 60 * 24 * 365, function () use ($path) {
            return Storage::cloud()->get($path);
        });
    }
}

if (!function_exists('base64_file_from_cloud')) {
    function base64_file_from_cloud(string $path)
    {
        return cache()->remember('files:base64:' . Str::slug($path), 60 * 60 * 24 * 365, function () use ($path) {
            return base64_encode(Storage::cloud()->get($path));
        });
    }
}

if (!function_exists('flash_message')) {
    function flash_message(string $type, string $msg)
    {
        $messages = session('messages', []);
        $messages[$type][] = $msg;

        session()->flash('messages', $messages);
    }
}

if (!function_exists('flash_notification')) {
    function flash_notification(string $type, string $msg)
    {
        $notifications = session('notifications', []);
        $notifications[$type][] = $msg;

        session()->flash('notifications', $notifications);
    }
}

if (!function_exists('app_version')) {
    function app_version(): string
    {
        $version_file_path = base_path() . '/version';

        if (file_exists($version_file_path)) {
            return trim(file_get_contents($version_file_path));
        }

        return 'v' . config('app.version', '0.0.0');
    }
}

if (!function_exists('translate')) {
    function translate($key = null, $replace = [], $locale = null): string
    {
        return addslashes(trans($key, $replace, $locale));
    }
}

if (!function_exists('weekdays')) {
    function weekdays($key = null, $default = null): array|string|null
    {
        $weekdays = Carbon::getDays();

        Arr::forget($weekdays, 0);

        foreach ($weekdays as $day_num => $day_name) {
            $weekdays[$day_num] = Carbon::create($day_name)->dayName;
        }

        if (!is_null($key)) {
            return Arr::get($weekdays, $key, $default) ?? $default;
        }

        return $weekdays;
    }
}

if (!function_exists('teaching_hours_times')) {
    function teaching_hours_times(): array
    {
        $times = [];
        $start = now()->startOfDay()->addHours(13);
        $end = now()->endOfDay();

        while ($start->lte($end)) {
            $times[$start->format('H:i')] = $start->format('H:i');
            $start->addMinutes(5);
        }

        return $times;
    }
}
