<?php

namespace App\Jobs\Users;

use App\Models\User;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SyncPermissionsToUser
{
    use Dispatchable, InteractsWithQueue, SerializesModels;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @var Collection
     */
    protected $permissions;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param iterable $permissions
     */
    public function __construct(User $user, iterable $permissions)
    {
        $this->user = $user;

        if (is_array($permissions)) {
            $this->permissions = collect($permissions);
        }
        else {
            $this->permissions = $permissions;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (
            $this->user->permissions->pluck('id')->diff($this->permissions)->count() > 0 ||
            $this->permissions->diff($this->user->permissions->pluck('id'))->count() > 0
        ) {
            $this->user->permissions()->sync($this->permissions);
        }
    }
}


