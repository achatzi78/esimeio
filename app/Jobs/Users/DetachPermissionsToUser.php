<?php

namespace App\Jobs\Users;

class DetachPermissionsToUser extends SyncPermissionsToUser
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->permissions()->detach($this->permissions);
    }
}
