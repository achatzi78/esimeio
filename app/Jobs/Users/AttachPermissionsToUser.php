<?php

namespace App\Jobs\Users;

class AttachPermissionsToUser extends SyncPermissionsToUser
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->permissions()->attach($this->permissions);
    }
}
