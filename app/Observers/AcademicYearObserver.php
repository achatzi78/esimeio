<?php

namespace App\Observers;

use App\Models\AcademicYear;
use App\Models\User;

class AcademicYearObserver
{
    /**
     * Handle the AcademicYear "created" event.
     *
     * @param AcademicYear $academic_year
     * @return void
     */
    public function created(AcademicYear $academic_year)
    {
        User::developers()->get()->each(function ($user) use ($academic_year) {
            $user->academic_years()->attach($academic_year->id);
        });

        User::admins()->get()->each(function ($user) use ($academic_year) {
            $user->academic_years()->attach($academic_year->id);
        });
    }

    public function saved(AcademicYear $academic_year)
    {
        if ($academic_year->is_current) {
            AcademicYear::whereIsCurrent(1)
                ->where('id', '<>', $academic_year->id)
                ->update([
                    'is_current' => 0
                ]);

            cache()->forever('current_academic_year', $academic_year);
        }
    }
}
