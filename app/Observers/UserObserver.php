<?php

namespace App\Observers;

use App\Models\AcademicYear;
use App\Models\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  User $user
     * @return void
     */
    public function created(User $user)
    {
        if ($user->is_developer || $user->is_admin) {
            $user->academic_years()->sync(AcademicYear::all());
        }
    }
}
