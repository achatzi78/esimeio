<?php

namespace App\Observers;

use App\Models\TeachingHour;
use App\Models\UserAvailability;

class TeachingHourObserver
{
    /**
     * Handle the TeachingHour "created" event.
     *
     * @param TeachingHour $teaching_hour
     * @return void
     */
    public function created(TeachingHour $teaching_hour)
    {
        foreach (weekdays() as $day_num => $day_name) {
            UserAvailability::create([
                'academic_year_id' => $teaching_hour->academic_year_id,
                'teaching_hour_id' => $teaching_hour->id,
                'weekday' => $day_num,
            ]);
        }
    }
}
