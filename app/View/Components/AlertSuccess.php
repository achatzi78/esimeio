<?php

namespace App\View\Components;

class AlertSuccess extends Alert
{
    /**
     * Create a new component instance.
     *
     * @param string|null $message
     * @param bool $dismiss
     */
    public function __construct(string $message = null, $dismiss = true)
    {
        parent::__construct($message, 'success', 'fa-check-circle', $dismiss);
    }
}
