<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AlertLight extends Alert
{
    /**
     * Create a new component instance.
     *
     * @param string|null $message
     * @param null $icon
     * @param bool $dismiss
     */
    public function __construct(string $message = null, $icon = null, $dismiss = true)
    {
        parent::__construct($message, 'light', $icon, $dismiss);
    }
}
