<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AlertWarning extends Alert
{
    /**
     * Create a new component instance.
     *
     * @param string|null $message
     * @param bool $dismiss
     */
    public function __construct(string $message = null, $dismiss = true)
    {
        parent::__construct($message, 'warning', 'fa-exclamation-triangle', $dismiss);
    }
}

