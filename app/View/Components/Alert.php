<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * @var string|null
     */
    public ?string $type;

    /**
     * @var string|null
     */
    public ?string $icon;

    /**
     * @var string|null
     */
    public ?string $message;

    /**
     * @var bool
     */
    public bool $dismiss;

    /**
     * Create a new component instance.
     *
     * @param string|null $message
     * @param string|null $type
     * @param string|null $icon
     * @param bool $dismiss
     */
    public function __construct(string $message = null, string $type = null, string $icon = null, bool $dismiss = true)
    {
        $this->type = $type ?? 'primary';
        $this->icon = $icon ?? 'fa-info-circle';
        $this->dismiss = $dismiss;
        $this->message = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render(): View|Closure|string
    {
        return view('components.alert');
    }
}
