<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class TableHead extends Component
{
    /**
     * @var string|null
     */
    public ?string $sortBy = null;

    /**
     * @var string|null
     */
    public ?string $currentSort = null;

    /**
     * @var string|null
     */
    public ?string $currentDirection = null;

    /**
     * Create a new component instance.
     *
     * @param null $sortBy
     * @param null $currentSort
     * @param null $currentDirection
     *
     * @return void
     */
    public function __construct($sortBy = null, $currentSort = null, $currentDirection = null)
    {
        $this->sortBy = $sortBy;
        $this->currentSort = $currentSort;
        $this->currentDirection = $currentDirection;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render(): View|Closure|string
    {
        return view('components.table-head');
    }
}
