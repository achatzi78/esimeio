<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AlertDark extends Alert
{
    /**
     * Create a new component instance.
     *
     * @param string|null $message
     * @param string|null $icon
     * @param bool $dismiss
     */
    public function __construct(string $message = null, string $icon = null, bool $dismiss = true)
    {
        parent::__construct($message, 'dark', $icon, $dismiss);
    }
}
